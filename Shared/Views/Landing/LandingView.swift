//
//  LandingView.swift
//  iOS
//
//  Created by Leudy on 30/06/2020.
//

import SwiftUI

struct LandingView: View {
    var body: some View {
        VStack(alignment: .center) {
            TitleView()
        }
    }
}

struct TitleView: View {
    
    @State var showingLogin = false
    @State var showingRegister = false

    var body: some View {
            VStack {
                ScrollView(.vertical, showsIndicators: false){
                    
                    Image("landing")
                        .resizable()
                        .frame(width: 300, height: 300)
                        .padding(.top, 20)
                    
                    Text("Bienvenidx a Vetec")
                    .multilineTextAlignment(.center)
                    .font(.title)
                    .font(Font.body.bold())
                    
                    Text("Consultas veterinarias online las 24/7 con tu centro veterio de confianza.")
                    .multilineTextAlignment(.center)
                    .padding(20)
                    .lineLimit(3)
                    

                    Button(action: {
                        self.showingLogin.toggle()
                                }) {
                            Text("Iniciar sesión")
                            .font(.system(size: 18))
                                    .frame(maxWidth: .infinity)
                                    .padding()
                                    .foregroundColor(.white)
                                    .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                    .cornerRadius(10)
                                    .padding(.horizontal, 30) .padding(10)
                                }.sheet(isPresented: $showingLogin) {
                                    Sociallogin()
                                }
                    
                    Button(action: {
                        self.showingRegister.toggle()
                                }) {
                            Text("Regístrarme")
                            .font(.system(size: 18))
                                    .frame(maxWidth: .infinity)
                                    .padding()
                                    .foregroundColor(.white)
                                    .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                    .cornerRadius(10)
                                    .padding(.horizontal, 30)
                                    .padding(10)
                    }.sheet(isPresented: $showingRegister) {
                        RegiterView()
                    }
                }
            }
            
        }
    }







