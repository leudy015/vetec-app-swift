//
//  AddMascota.swift
//  vetecapp
//
//  Created by Leudy on 16/07/2020.
//

import SwiftUI
import Foundation
import Apollo

struct AddMascota: View {
    
    @State private var name: String = ""
    @State private var raza: String = ""
    @State private var genero: String = ""
    @State private var avatar: String = ""
    @State private var especie: String = ""
    @State private var peso: String = ""
    @State private var microchip: String = ""
    @State private var vacuna: String = ""
    @State private var alerjias: String = ""
    @State private var castrado: Bool = false
    @State private var dates: Date = Date()
    @State private var showingAlert = false
    
    @State var imagePicker = false
    @State var imgData : Data = Data(count: 0)
    @State var value : CGFloat = 0
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical,  showsIndicators: false){
                VStack(alignment: .leading){
                    
                    VStack(alignment: .leading){
                        Button(action: {
                            
                            imagePicker.toggle()
                            
                        }) {
                            
                            if self.imgData.count != 0 {
                                
                                Image(uiImage: UIImage(data: self.imgData)!).resizable().renderingMode(.original).frame(width: 120, height: 120).clipShape(Circle())
                                
                            } else {
                                
                                Image(systemName: "plus")
                                .resizable()
                                .frame(width: 25, height: 25)
                                .foregroundColor(Color("text"))
                                .padding(38)
                                
                            }
                        }.background(Color("Category").opacity(0.5))
                        .clipShape(Circle())
                        .padding(.leading, 15)
                        .padding(.trailing, 15)
                        .padding(.bottom, 30)
                        .padding(.top, 20)
                        .frame(alignment: .leading)
                        
                        
                        
                    }
                    
                    HStack {
                        Image(systemName: "person")
                        .foregroundColor(.secondary)
                        
                        TextField("Nombre", text: $name)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    HStack {
                        Image(systemName: "staroflife")
                        .foregroundColor(.secondary)
                        
                        TextField("Raza", text: $raza)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    
                    HStack {
                        Image(systemName: "figure.wave")
                        .foregroundColor(.secondary)
                        
                        TextField("Genéro", text: $genero)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    
                    HStack {
                        Image(systemName: "circles.hexagongrid")
                        .foregroundColor(.secondary)
                        
                        TextField("Especie", text: $especie)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    HStack {
                        Image(systemName: "scalemass")
                        .foregroundColor(.secondary)
                        
                        TextField("Peso", text: $peso).keyboardType(.numberPad)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    HStack {
                        Image(systemName: "pills")
                        .foregroundColor(.secondary)
                        
                        TextField("Vacunas", text: $vacuna)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    HStack {
                        Image(systemName: "bolt.heart")
                        .foregroundColor(.secondary)
                        
                        TextField("Alergias", text: $alerjias)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    HStack {
                        Image(systemName: "qrcode.viewfinder")
                        .foregroundColor(.secondary)
                        
                        TextField("MicroChip No. (Opcional)", text: $microchip)
                        }
                        .padding()
                        .background(Color("white"))
                        .frame(height: 55)
                        .textFieldStyle(PlainTextFieldStyle())
                        .padding([.leading, .trailing], 4)
                        .cornerRadius(7)
                        .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 45)
                    
                    
                        Toggle(isOn: $castrado) {
                                    Text("¿Está tu mascota esterilizada?")
                                }.padding()
                    
                    
                    
                }
                
                DatePicker("Nacimiento de \(name)", selection: $dates, displayedComponents: .date).padding()
                
            }
            
            .navigationBarTitle(Text("Añadir mascota"))
            .navigationBarItems( trailing:
                                    Button(action: {
                                        if self.imgData.count != 0 {
                                            
                                            let defaults = UserDefaults.standard
                                            let id = defaults.value(forKey: "id")
                                          
                                            
                                            let input = Mascotainput(name: name, age: "\(dates)", avatar: avatar, usuario: id as? GraphQLID, especie: especie, raza: raza, genero: genero, peso: peso, microchip: microchip, vacunas: vacuna, alergias: alerjias, castrado: castrado)
                                            
                                            Network.shared.apollo.perform(mutation: CrearMascotaMutation(input: input))
                                                { result in
                                                    
                                                    guard (try? result.get().data) != nil else { return }
                                                    
                                                 switch result {
                                                 case .success(let graphQLResult):
                                                    
                                                    if graphQLResult.data?.crearMascota?.success == false {
                                                        
                                                        let message = graphQLResult.data?.crearMascota?.message
                                                        print(message ?? "Message")
                                                       
                                                    } else {
                                                        
                                                        print("Mascota agregado con exito: \(graphQLResult)")
                                                        
                                                        self.name = ""
                                                        self.raza = ""
                                                        self.genero = ""
                                                        self.avatar = ""
                                                        self.especie = ""
                                                        self.peso = ""
                                                        self.microchip = ""
                                                        self.vacuna = ""
                                                        self.alerjias = ""
                                                        self.castrado = false
                                                        self.imgData.count = 0
                                                        self.dates = Date()
                                                        
                                                    }
                                                   if let errors = graphQLResult.errors {
                                                     print("Errors from server: \(errors)")
                                                   }
                                                 case .failure(let error):
                                                   print("Error: \(error)")
                                                 }
                                                }
                                            
        
                                        } else {
                                            
                                            self.showingAlert = true
                                            
                                        }
                                        
                                    }, label: {
                                        
                                        Text("Guardar")
                                
                                    }).alert(isPresented: $showingAlert) {
                                        Alert(title: Text("Foto del perfil necesaria"), message: Text("Para añadir una mascota debes seleccionar una foto de perfil"), dismissButton: .default(Text("Seleccionar foto!"),  action: { imagePicker.toggle() }))
                                    }
                                  
                                )
            
        }// Full Screen Image Picker...
        .fullScreenCover(isPresented: self.$imagePicker, onDismiss: {
            
            if self.imgData.count != 0 {
            
                let imgs = imgData.base64EncodedString()
            
                let imgBlob = "data:image/jpeg;base64,\(imgs)"
                
            Network.shared.apollo.perform(mutation: SingleUploadMutation(imgBlob: imgBlob))
                { result in
                
                print(imgs)
                
                 switch result {
                 case .success(let graphQLResult):
                    
                    print("foto subida con exito: \(graphQLResult.data?.singleUpload?.filename ?? "Nada")")
                    
                    self.avatar = graphQLResult.data?.singleUpload?.filename ?? "Nada"
                    
                    if let errors = graphQLResult.errors {
                     print("Errors from server: \(errors)")
                   }
                 case .failure(let error):
                   print("Error: \(error)")
                 }
                }
                
            }
                    
        }) {
            
            AvatarPiker(imagePicker: self.$imagePicker, imgData: self.$imgData)
        }
    }
}


struct AvatarPiker : UIViewControllerRepresentable {
    
    
    func makeCoordinator() -> Coordinator {
        
        return AvatarPiker.Coordinator(parent1: self)
    }

    @Binding var imagePicker : Bool
    @Binding var imgData : Data
    
    func makeUIViewController(context: Context) -> UIImagePickerController{
        
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        
    }
    
    class Coordinator : NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
        
        var parent : AvatarPiker
        
        init(parent1 : AvatarPiker) {
            
            parent = parent1
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            
            parent.imagePicker.toggle()
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            let image = info[.originalImage] as! UIImage
            parent.imgData = image.jpegData(compressionQuality: 0.5)!
            parent.imagePicker.toggle()
        }
    }
}
