//
//  DetailsMascota.swift
//  vetecapp
//
//  Created by Leudy on 13/07/2020.
//

import SwiftUI
import URLImage

struct DetailsMascota: View {
    
    var data: Mascota
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
        VStack {
            HStack{
                URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.avatar)")!){
                        $0.image.resizable().renderingMode(.original)
                        }
                     .frame(minWidth: 100.0, maxWidth: 100.0, minHeight: 100.0, maxHeight: 100.0)
                     .clipped()
                     .clipShape(Circle())
                     .padding(.bottom, -30)
                
                VStack {
                    Text(data.name)
                        .font(.system(size: 24, weight: .bold))
                        .lineLimit(1)
                        .padding(.top, 15)
                        .frame(maxWidth: 250, alignment: .leading)
                    
                    Text(data.especie)
                        .font(.system(size: 18))
                        .foregroundColor(.gray)
                        .lineLimit(1)
                        .frame(maxWidth: 250, alignment: .leading)
                }
                
            }
            
            HStack {
                Text(data.age)
                    .font(.system(size: 14))
                            .frame(maxWidth: .infinity)
                            .lineLimit(1)
                            .padding()
                            .foregroundColor(Color.white)
                            .background(Color.orange)
                            .cornerRadius(10).padding(5)
                
                
                Text("Salud excelente")
                    .font(.system(size: 14))
                            .frame(maxWidth: .infinity)
                            .padding()
                            .foregroundColor(Color.white)
                            .background(Color.green)
                            .cornerRadius(10).padding(5)
            }.padding(.top, 40)
        }.padding(.top, 50)
        .padding(.bottom, 35)
            
            VStack(alignment: .leading){
                
                Text("Caracteriticas")
                    .font(.system(size: 20, weight: .bold))
                    .padding(.top,5)
                    .padding(.leading, 10)
                
                
                ItemsMascota(image: "circles.hexagongrid", title: data.especie)
                ItemsMascota(image: "staroflife", title: data.raza)
                ItemsMascota(image: "figure.wave", title: data.genero)
                ItemsMascota(image: "scalemass", title: "Peso: \(data.peso) KG")
                ItemsMascota(image: "pills", title: "Vacunas: \(data.vacunas)")
                ItemsMascota(image: "bolt.heart", title: "Alergias: \(data.alergias)")
                ItemsMascota(image: "qrcode.viewfinder", title: data.microchip)
                
                Text("Historial de consultas")
                    .font(.system(size: 20, weight: .bold))
                    .padding(.top, 15)
                    .padding(.bottom, 25)
                    .padding(.leading, 10)
                    .frame(alignment: .leading)
                
                DiagnosticoView(id: data.id).padding(.horizontal, 15)
            }
        }
    }

}


struct ItemsMascota: View {
    
    var image: String = ""
    var title = ""
    
    var body: some View{
     
        HStack(spacing: 15){
                Image(systemName: image)
                    .font(.system(size: 28))
                    .clipped()
                    .padding(.leading, 20)
                
                VStack(alignment: .leading) {
                    Text(title)
                        .font(.system(size: 22))
                        .foregroundColor(Color("text"))
                     }
            
            Spacer()
            
            Image(systemName: "chevron.right")
                .font(.system(size: 16))
                .clipped()
                .padding(.trailing, 20)
            
                }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.vertical, 20)
        .background(Color("Category").opacity(0.5))
                            
            }
        }
