//
//  MascotasView.swift
//  vetecapp
//
//  Created by Leudy on 06/07/2020.
//

import SwiftUI
import URLImage

struct MascotasView: View {
    
    @ObservedObject private var dataMascota: setMascota = setMascota()
    
    var body: some View {
        
        VStack{
        if dataMascota.mascota.isEmpty {
            
            ProgressView()
            
        } else {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack{
                    AddMacota()
                
                ForEach(dataMascota.mascota) { datos in
                    MacotaComponent(data: datos)
                }
            }
           }
        }
    }
  }
}

struct AddMacota: View {
    
    @State var showingAdd = false
    
var body: some View{
        
    VStack(alignment: .leading){
        
        Button(action: {
            self.showingAdd.toggle()
        }) {
            
            Image(systemName: "plus")
            .resizable()
            .frame(width: 25, height: 25)
            .foregroundColor(Color("text"))
            .padding(38)
            
            
        }.background(Color("Category").opacity(0.5))
        .clipShape(Circle())
        .padding(.leading, 15)
        .padding(.trailing, 15)
        .padding(.top, 20)
        .frame(alignment: .leading)
        .sheet(isPresented: $showingAdd) {
            AddMascota()
        }
            
        }
        
    }
    
}

struct MacotaComponent: View {
    
    @State var showingDetailsMascota = false
    
    var data: Mascota
    
    var body: some View {
               VStack{
                    Button(action: {
                        self.showingDetailsMascota.toggle()
                       }) {
                        VStack {
                            URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.avatar)")!){
                                $0.image.resizable().renderingMode(.original)
                                }
                             .frame(minWidth: 100.0, maxWidth: 100.0, minHeight: 100.0, maxHeight: 100.0)
                             .clipped()
                             .clipShape(Circle())
                             .padding(.bottom, -30)
                            .contentShape(Circle())
                            .padding(.all, 0)
                            .contextMenu {
                             Button(action: {
                                 self.showingDetailsMascota.toggle()
                             }) {
                                 
                                 HStack{
                                     Image(systemName: "newspaper")
                                     Text("Ver Detalles")
                                 }
                             }
                             
                             Button(action: {
                                eliminarMacota(id: data.id)
                             }) {
                                 
                                 HStack{
                                     Image(systemName: "trash.fill")
                                     Text("Eliminar mascota")
                                 }
                             }
                             
                         }
                        
                            VStack{
                                Text("\(data.name)")
                                    .foregroundColor(Color.white)
                                    .lineLimit(1)
                                    .font(.system(size: 14))
                                    .frame(width: 90, height: 8, alignment: .center)
                                            .padding()
                                            .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                            .cornerRadius(50)

                                
                            }
                        }
                           
                       }
                        .padding(.top, 20)
                        .sheet(isPresented: $showingDetailsMascota) {
                            DetailsMascota(data: data)
                    }
                   }
               }
           }


func eliminarMacota(id: String) {
    
    Network.shared.apollo.perform(mutation: EliminarMascotaMutation(id: id))
    { result in
     switch result {
     case .success(let graphQLResult):
        
        print("Mascota eliminada con exito: \(graphQLResult)")
        
       if let errors = graphQLResult.errors {
         print("Errors from server: \(errors)")
       }
     case .failure(let error):
       print("Error: \(error)")
     }
    }
    
}


struct Mascota: Identifiable{
    var id: String
    var name: String
    var age: String
    var usuario: String
    var avatar: String
    var especie: String
    var raza: String
    var genero: String
    var peso: String
    var microchip: String
    var vacunas: String
    var alergias: String
    var castrado:  Bool

   
    init(
        id: String,
        name: String,
        age: String,
        usuario: String,
        avatar: String,
        especie: String,
        raza: String,
        genero: String,
        peso: String,
        microchip: String,
        vacunas: String,
        alergias: String,
        castrado: Bool
    ){
        self.id = id
        self.name = name
        self.age = age
        self.usuario = usuario
        self.avatar = avatar
        self.especie = especie
        self.raza = raza
        self.genero = genero
        self.peso = peso
        self.microchip = microchip
        self.vacunas = vacunas
        self.alergias = alergias
        self.castrado = castrado
    }

}


class setMascota: ObservableObject {
    @Published var mascota: [Mascota]
    
    init() {
        let defaults = UserDefaults.standard
        let id = defaults.value(forKey: "id")
        print("running loadData")
        self.mascota = []
        loadData(id: id as! String)
    }
    
    func loadData(id: String) {
        Network.shared.apollo.fetch(query: GetMascotaQuery(usuarios: id)) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getMascota?.list {
                                        for datas in items {
                                            if let value = datas {
                                            //Do whatever here with your Graphql Result
                                                self.mascota.append(Mascota(id: value.id!, name: value.name!, age: value.age!, usuario: value.usuario!, avatar: value.avatar!, especie: value.especie!, raza: value.raza!, genero: value.genero!, peso: value.peso!, microchip: value.microchip!, vacunas: value.vacunas!, alergias: value.alergias!, castrado: value.castrado!))
                                            }
                                        }
                                    }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
    
           
    }

    
