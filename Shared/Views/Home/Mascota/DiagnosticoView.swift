
//
//  DiagnosticoView.swift
//  vetecapp
//
//  Created by Leudy on 15/07/2020.
//

import SwiftUI
import URLImage

struct DiagnosticoView: View {
    @ObservedObject private var dataDiagnostico: setDiadnostico = setDiadnostico()
    
    var id = ""
   
    var body: some View {
        return ForEach(dataDiagnostico.diagnostico) { dat in
            VStack{
                HStack{
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(dat.profesional.avatar)")!){
                        $0.image.resizable()
                           }
                            .clipShape(Circle())
                            .frame(width: 40, height: 40)
                    VStack(alignment: .leading){
                        Text("\(dat.profesional.nombre)").lineLimit(1)
                            .font(.system(size: 14))
                        
                        Text(dat.profesional.profesion).lineLimit(1)
                            .font(.system(size: 10)).foregroundColor(.gray)
                    }
                    
                    Spacer()
                    
                    VStack(alignment: .trailing){
                        HStack{
                            Image(systemName: "checkmark.seal").font(.system(size: 14)).foregroundColor(Color.green)
                            Text("Verificaco por Vetec").lineLimit(1)
                                .font(.system(size: 14)).foregroundColor(.green)
                        }
                        
                        Text("\(DateFormatter().date(from: dat.created_at) ?? Date())")
                            .lineLimit(1)
                            .font(.system(size: 10)).foregroundColor(.gray)
                            .frame(width: 150)
                    }
                }
                
                Text(dat.diagnostico).foregroundColor(.gray).font(.system(size: 12)).padding(.leading, 45).padding(.top, 5)
                
                VStack(alignment: .leading){
                    Button(action: {
                        print(id)
                    }, label: {
                        HStack{
                            Image(systemName: "translate").font(.system(size: 12)).foregroundColor(Color("main")).frame(alignment: .leading)
                            Text("Traducir").lineLimit(1).frame(alignment: .leading)
                                .font(.system(size: 12)).foregroundColor(Color("main"))
                        }
                    })
                }
               
                
            }.padding(.top, 20)
        }
    }
}


struct Pfsn : Identifiable {
    
    var id: String
    var nombre : String
    var apellidos : String
    var profesion : String
    var avatar : String
    

    init(
        id: String,
        nombre : String,
        apellidos : String,
        profesion : String,
        avatar : String
    ) {
        self.id = id
        self.nombre = nombre
        self.apellidos = apellidos
        self.profesion = profesion
        self.avatar = avatar
    }
}


struct Diagnostico : Identifiable {
    var id : String
    var diagnostico : String
    var mascota : String
    var created_at : String
    var profesional : Pfsn

    init(
        id : String,
        diagnostico : String,
        mascota : String,
        created_at : String,
        profesional: Pfsn
    ) {
        self.id = id
        self.diagnostico = diagnostico
        self.mascota = mascota
        self.created_at = created_at
        self.profesional = profesional
        }
}


class setDiadnostico: ObservableObject {
    @Published var diagnostico: [Diagnostico]

    init() {
        self.diagnostico = []
        loadData(id: "5eee0c22608a47116788e9e7")
    }
    
    func loadData(id: String) {
    
        Network.shared.apollo.fetch(query: GetDiagnosticoQuery(id: id)) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getDiagnostico?.list {
                    for values in items {
                        if let val = values {
                            self.diagnostico.append(Diagnostico(
                                                        id: val._id!,
                                                        diagnostico: val.diagnostico!,
                                                        mascota: val.mascota!,
                                                        created_at: val.createdAt!,
                                                        profesional:
                                                            Pfsn(
                                                                id: val.profesional?.nombre! ?? "",
                                                                nombre: val.profesional?.nombre! ?? "",
                                                                apellidos: val.profesional?.apellidos! ?? "",
                                                                profesion: val.profesional?.profesion! ?? "",
                                                                avatar: val.profesional?.avatar! ?? "")))
                        }
                    }
                }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
      }

