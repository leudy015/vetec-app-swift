//
//  ProfesionalCategory.swift
//  vetecapp
//
//  Created by Leudy on 19/07/2020.
//

import SwiftUI
import Apollo
import URLImage

struct ProfesionalCategory: View {
    
    var id = ""
    
    @ObservedObject private var dataCategoryPro: setCategoryProfesional = setCategoryProfesional()
    @State var showingDetail = false
    
    var body: some View {
        HStack{
        if dataCategoryPro.profesional.isEmpty {
            
            VStack{
                ProgressView()
            }.padding(.leading, 195)
            
        } else {
            ForEach(dataCategoryPro.profesional){ datas in
                Button(action: {
                    self.showingDetail.toggle()
                }, label: {
                    ZStack(alignment: .top) {
                        Rectangle()
                            .fill(Color("Background"))
                            .cornerRadius(10)
                            .padding(.horizontal, 10)
                            .padding(.vertical, 6)
                            .frame(width: 180, height: 180)
                            .overlay(RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color("border"), lineWidth: 0.5)
                                        .padding(.horizontal, 10)
                                        .padding(.vertical, 6)
                            )
                        
                        VStack{
                            
                            VStack(spacing: 15) {
                                URLImage(URL(string: "https://server.vetec.es/assets/images/\(datas.avatar)")!){
                                    $0.image.resizable().renderingMode(.original)
                                    }
                                    .clipShape(Circle())
                                    .frame(width: 70, height: 70)
                                    .padding(.top, -30)
                            }
                            
                            VStack(alignment: .leading) {
                                
                                HStack{
                                    Text("\(datas.nombre) \(datas.apellidos)")
                                    .foregroundColor(Color("text"))
                                    .lineLimit(1)
                                    .frame(maxWidth: 120, alignment: .leading)
                                    .font(.system(size: 16))
                                    .padding(.top, 10)
                                        .padding(.bottom, datas.isverifield ? 0 : 5)
                                    
                                    
                                    if datas.isverifield {
                                        Image(systemName: "checkmark.seal.fill")
                                            .foregroundColor(Color("main"))
                                            .padding(.leading, -10)
                                            .font(.system(size: 12))
                                            .padding(.top, 10)
                                    }
                                }
                                
                                 Text("\(datas.profesion)")
                                    .foregroundColor(Color(.secondaryLabel))
                                .lineLimit(1)
                                .frame(maxWidth: 100, alignment: .leading)
                                .font(.system(size: 12))
                                
                                Text("\(datas.experiencia) de experiencia")
                                    .foregroundColor(Color(.secondaryLabel))
                                .lineLimit(1)
                                .frame(maxWidth: 100, alignment: .leading)
                                .font(.system(size: 12))
                                
                                HStack{
                                    Image(systemName: "star.fill")
                                        .foregroundColor(Color(.orange))
                                        .frame(alignment: .leading)
                                        .font(.system(size: 14))
                                    
                                    Text("\(datas.ratin)")
                                        .foregroundColor(Color(.secondaryLabel))
                                    .lineLimit(1)
                                    .frame(alignment: .leading)
                                    .font(.system(size: 14))
                                        .padding(.trailing, 60)
                                
                                    
                                    Button(action: {
                                        if datas.anadidoFavorito {
                                            eliminarFavorito(profesionalID: datas.id)
                                        } else {
                                            
                                            let defaults = UserDefaults.standard
                                            let userID = defaults.value(forKey: "id")
                                            
                                            añadiaFavorito(userID: userID as! String, profesionalID: datas.id)
                                        }
                                        
                                    }, label: {
                                        ZStack{
                                            Circle().frame(width: 40, height: 40, alignment: .center)
                                             .foregroundColor(Color("Category"))
                                            Image(systemName: datas.anadidoFavorito ? "heart.fill" : "heart")
                                                    .font(.system(size: 20))
                                                    .foregroundColor(Color.red)
                                        }
                                    })
                                    
                                }
                                
                            }
                            
                        }
                    }
                }).sheet(isPresented: $showingDetail) {
                    DetailsProfesional(data: datas)
                    }
                
                .shadow(color: Color("shadow"), radius: 10)
                
              }
        }
      }
    }
}

class setCategoryProfesional: ObservableObject {
    @Published var profesional: [Profesional]
 
    init() {
        self.profesional = []
        print("running loadData")
        loadDataCategoryProfesional()
    }
    
    func loadDataCategoryProfesional() {
        Network.shared.apollo.fetch(query: GetCategoryQuery(id: "5ea8a50a43a7ca27aa0e739e")) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getCategory?.data?.profesionales {
                                        for datas in items {
                                            if let pofesionales = datas {
                                                
                                                self.profesional.append(Profesional(id: pofesionales.id!, nombre: pofesionales.nombre!, apellidos:pofesionales.apellidos!, ciudad: pofesionales.ciudad!, experiencia: pofesionales.experiencia!, avatar: pofesionales.avatar!, consultas: pofesionales.consultas!.count, profesion: pofesionales.profesion!, anadidoFavorito: pofesionales.anadidoFavorito!, Precio: pofesionales.precio!, votos: pofesionales.professionalRatinglist!.count, descripcion: pofesionales.descricion!, visit: pofesionales.visitas!, ratin: pofesionales.professionalRatinglist?.count ?? 0, isverifield: pofesionales.isVerified!))
                                                
                                            }
                                        }
                                    }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
     }
