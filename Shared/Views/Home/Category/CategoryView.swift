//
//  CategoryView.swift
//  vetecapp
//
//  Created by Leudy on 06/07/2020.
//

import SwiftUI

struct CategoryView: View {
    
    @ObservedObject private var dataCategory: setCategory = setCategory()
    
    @State var selected = "Perros y Gatos"
    @State var ids = "5ea8a50a43a7ca27aa0e739e"
    
    var body: some View{
        VStack(alignment: .leading) {
            Text("Especialidades").font(.title3)
                .padding(.horizontal, 20)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                ForEach(dataCategory.category){ i in
                    VStack(spacing: 10){
                        Button(action: {
                            
                            self.selected = i.title
                            self.ids = i.id
                            
                        }, label: {

                                Text(i.title)
                                    .foregroundColor(Color(self.selected == i.title ? "Items" : "text"))
                                    .lineLimit(1)
                                    .padding()
                                    .background(LinearGradient(gradient: Gradient(colors: [Color(self.selected == i.title ? "main" : "Category"), Color(self.selected == i.title ? "secundary" : "Category")]), startPoint: .leading, endPoint: .trailing).opacity(self.selected == i.title ? 100 : 0.5)).cornerRadius(25)

                        })
                        
                    }
                }
            }.padding(.horizontal, 15)
         }
        
            VStack {
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack{
                        ProfesionalCategory(id: ids)
                    }.padding(.top, 50).padding(.bottom, 20)
                }
            }
      }
   }
}


struct Category: Identifiable{
    var id: String
    var title: String
    
    init(
        id: String,
        title: String
    ){
        self.id = id
        self.title = title
    }

}


class setCategory: ObservableObject {
    @Published var category: [Category]
    
    init() {
        print("running loadData")
        self.category = []
        loadDataCategory()
    }
    
    func loadDataCategory() {
        Network.shared.apollo.fetch(query: GetCategoriesQuery()) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getCategories {
                                        for datas in items {
                                            if let value = datas {
                                                
                                                self.category.append(Category(id: value.id!, title: value.title ?? ""))
                                                
                                            }
                                        }
                                    }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
     }


