//
//  CardProfesional.swift
//  vetecapp
//
//  Created by Leudy on 06/07/2020.
//

import SwiftUI
import Apollo
import URLImage


extension String: Identifiable {
    public var id: String { self }
}

struct ListwihtData: View {
    @ObservedObject private var dataProfesional: setProfesionales = setProfesionales()
    var body: some View {
        VStack{
            
            if dataProfesional.profesional.isEmpty {
                
                ProgressView()
                
            } else {
                
           ForEach(dataProfesional.profesional) { dat in
                
                CardProfesional(data: dat)
                
            }
        }
    }
  }
}


struct CardProfesional: View {
    
    @State var showingDetail = false
    
    var data: Profesional
    
    var body: some View {
        VStack{
                    
            ZStack(alignment: .topLeading) {
                Rectangle()
                    .fill(Color("Background"))
                    .cornerRadius(15)
                    .padding(.horizontal, 10)
                    .padding(.vertical, 6)
                    .frame(height: 290)
                    .overlay(RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("border"), lineWidth: 0.5)
                                .padding(.horizontal, 10)
                                .padding(.vertical, 6)
                    )
                
                VStack{
                HStack {
                VStack(spacing: 15) {
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.avatar)")!){
                        $0.image.resizable().renderingMode(.original)
                        }
                        .clipShape(Circle())
                        .frame(width: 90, height: 90)
                        .padding(.leading, 25)
                        .padding(.top, 25)
                        
                        
                        
                    
                    ZStack{
                        Circle().frame(width: 30, height: 30, alignment: .center)
                            .foregroundColor(Color.green)
                            .padding(.top, -30)
                        
                        Text("90%")
                            .foregroundColor(Color.white)
                            .padding(.top, -20)
                            .lineLimit(1)
                            .font(.system(size: 10))
                    }
                }
                    
                    HStack(alignment: .top){
                        
                        VStack(alignment: .leading) {
                            
                            HStack{
                                Text("\(data.nombre) \(data.apellidos)")
                                .foregroundColor(Color("text"))
                                .lineLimit(1)
                                .frame(maxWidth: 170, alignment: .leading)
                                .font(.system(size: 18))
                                .padding(.leading, 15)
                                
                                
                                if data.isverifield {
                                    Image(systemName: "checkmark.seal.fill")
                                        .foregroundColor(Color("main"))
                                        .padding(.leading, -5)
                                        .font(.system(size: 18))
                                }
                            }.padding(.top, 20)
                            
                            
                            Text("Disponible hoy")
                                .foregroundColor(Color.green)
                                .lineLimit(1)
                                .frame(width: 170, alignment: .leading)
                                .font(.system(size: 12))
                                .padding(.leading, 15)
                            
                            
                            ZStack(alignment: .leading) {
                                VStack(alignment: .leading){
                                    Text(data.profesion)
                                        .foregroundColor(Color.gray)
                                        .lineLimit(1)
                                        .font(.system(size: 12))
                                        .padding(.leading, 20)
                                    
                                    Text("\(data.experiencia) de experiencia")
                                        .lineLimit(1)
                                        .font(.system(size: 12))
                                        .padding(.leading, 20)
                                }
                            
                            }.frame(width: 200, height: 50, alignment: .leading)
                        
                        }
                        
                        Spacer()
                        
                        VStack{
                            
                            Button(action: {
                                if data.anadidoFavorito {
                                    eliminarFavorito(profesionalID: data.id)
                                } else {
                                    
                                    let defaults = UserDefaults.standard
                                    let userID = defaults.value(forKey: "id")
                                    
                                    añadiaFavorito(userID: userID as! String, profesionalID: data.id)
                                }
                                
                            }, label: {
                                ZStack{
                                    Circle().frame(width: 40, height: 40, alignment: .center)
                                     .foregroundColor(Color("Category"))
                                    Image(systemName: data.anadidoFavorito ? "heart.fill" : "heart")
                                            .font(.system(size: 20))
                                            .foregroundColor(Color.red)
                                }
                            })
                        }.padding(.trailing, 20).padding(.top, 10)
                        
              }
            }
                    
                    HStack(alignment: .center, spacing: 30) {
                        
                        VStack(alignment: .center, spacing: 5){
                            
                            Text("\(data.votos) Votos")
                                .foregroundColor(Color.gray)
                                .lineLimit(1)
                                .font(.system(size: 12))
                                .padding(.leading, 15)
                            
                            Text("\(data.consultas) Feedback")
                                .foregroundColor(Color("text"))
                                .lineLimit(1)
                                .font(.system(size: 14))
                                .padding(.leading, 15)
                        }
                        
                        
                        VStack{
                            Image(systemName: "flag")
                                    .font(.system(size: 16))
                                .foregroundColor(Color("bg"))
                            
                            Text(data.ciudad)
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }.padding(.leading, 20)
                        
                        VStack{
                            Image(systemName: "eurosign.circle")
                                    .font(.system(size: 16))
                                    .foregroundColor(Color("main"))
                            
                            Text("\(data.Precio)€/Consulta")
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }
                        
                        
                        VStack{
                            Image(systemName: "star")
                                    .font(.system(size: 16))
                                .foregroundColor(Color.orange)
                            
                            Text("\(data.ratin)")
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }
                        
                        
                        
                        
                    }
                    
             
                
            
                    HStack{
                        Button(action: {
                            }) {
                                Text("Hacer consulta")
                                .font(.system(size: 14))
                                        .frame(maxWidth: .infinity)
                                        .padding()
                                        .foregroundColor(.white)
                                        .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                        .cornerRadius(10)
                                        .padding(.horizontal, 5) .padding(5)
                                   
                            
                        }
                        
                        Button(action: {
                            self.showingDetail.toggle()
                            }) {
                            Text("Detalles")
                                .font(.system(size: 14))
                                        .frame(maxWidth: .infinity)
                                        .padding()
                                        .foregroundColor(Color("text"))
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                            .stroke(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing), lineWidth: 2)
                                                )
                                        .cornerRadius(10).padding(5)
                            
                        }.sheet(isPresented: $showingDetail) {
                            DetailsProfesional(data: data)
                        }
                        
                        }.padding(.top, 30)
                    .padding(.horizontal, 15)
          }
            }
            .cornerRadius(15)
            .contextMenu {
                Button(action: {
                    self.showingDetail.toggle()
                }) {
                    
                    HStack{
                        Image(systemName: "person")
                        Text("Ver Detalles")
                    }
                }
                
                Button(action: {
                    
                }) {
                    
                    HStack{
                        Image(systemName: "calendar")
                        Text("Hacer Consulta")
                    }
                }
                
                
                Button(action: {
                    
                    if data.anadidoFavorito {
                        
                        eliminarFavorito(profesionalID: data.id)
                        
                    } else {
                        
                        let defaults = UserDefaults.standard
                        let userID = defaults.value(forKey: "id")
                        
                        añadiaFavorito(userID: userID as! String, profesionalID: data.id)
                    }
                }) {
                    
                    HStack{
                        Image(systemName: data.anadidoFavorito ? "heart.fill" : "heart")
                        Text(data.anadidoFavorito ? "Añadido en favorito" : "Añadir a favorito")
                    }
                }
                    
            }.shadow(color: Color("shadow"), radius: 30)
      }
   }
}

func eliminarFavorito(profesionalID: String) {
    
    Network.shared.apollo.perform(mutation: EliminarUsuarioFavoritoProMutation(id: profesionalID))
    { result in
        
        guard (try? result.get().data) != nil else { return }
        
     switch result {
     case .success(let graphQLResult):
        
        if graphQLResult.data?.eliminarUsuarioFavoritoPro?.success == false {
            
            let message = graphQLResult.data?.eliminarUsuarioFavoritoPro?.message
            print(message ?? "Message")
           
        } else {
            
            print("Usuario agregado con exito: \(graphQLResult)")
            
        }
       if let errors = graphQLResult.errors {
         print("Errors from server: \(errors)")
       }
     case .failure(let error):
       print("Error: \(error)")
     }
    }
    
}

func añadiaFavorito(userID: String, profesionalID: String) {
            
    Network.shared.apollo.perform(mutation: CrearUsuarioFavoritoProMutation(profesionalId: profesionalID, usuarioId: userID))
        { result in
            
            guard (try? result.get().data) != nil else { return }
            
         switch result {
         case .success(let graphQLResult):
            
            if graphQLResult.data?.crearUsuarioFavoritoPro?.success == false {
                
                let message = graphQLResult.data?.crearUsuarioFavoritoPro?.message
                print(message ?? "Message")
               
            } else {
                
                print("Usuario agregado con exito: \(graphQLResult)")
                
            }
           if let errors = graphQLResult.errors {
             print("Errors from server: \(errors)")
           }
         case .failure(let error):
           print("Error: \(error)")
         }
        }
        
    }

struct Profesional: Identifiable{
    var id: String
    var nombre: String
    var apellidos: String
    var ciudad: String
    var experiencia: String
    var avatar: String
    var consultas: Int
    var profesion: String
    var anadidoFavorito: Bool
    var Precio: String
    var votos: Int
    var descripcion: String
    var visit:  Int
    var ratin: Int
    var isverifield: Bool
   
    init(
        id: String,
        nombre: String,
        apellidos: String,
        ciudad: String,
        experiencia: String,
        avatar: String,
        consultas: Int,
        profesion: String,
        anadidoFavorito: Bool,
        Precio: String,
        votos: Int,
        descripcion: String,
        visit: Int,
        ratin: Int,
        isverifield: Bool
    ){
        self.id = id
        self.nombre = nombre
        self.apellidos = apellidos
        self.ciudad = ciudad
        self.experiencia = experiencia
        self.avatar = avatar
        self.consultas = consultas
        self.profesion = profesion
        self.anadidoFavorito = anadidoFavorito
        self.Precio = Precio
        self.votos = votos
        self.descripcion = descripcion
        self.visit = visit
        self.ratin = ratin
        self.isverifield = isverifield
    }

}


class setProfesionales: ObservableObject {
    @Published var profesional: [Profesional]

    
    init() {
        print("running loadData")
        self.profesional = []
        loadData()
    }
    
    func loadData() {
        Network.shared.apollo.fetch(query: GetProfesionalallQuery()) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getProfesionalall {
                                        for prof in items {
                                            if let pofesionales = prof {
                                            //Do whatever here with your Graphql Result
                                                self.profesional.append(Profesional(id: pofesionales.id!, nombre: pofesionales.nombre!, apellidos:pofesionales.apellidos!, ciudad: pofesionales.ciudad!, experiencia: pofesionales.experiencia!, avatar: pofesionales.avatar!, consultas: pofesionales.consultas!.count, profesion: pofesionales.profesion!, anadidoFavorito: pofesionales.anadidoFavorito!, Precio: pofesionales.precio!, votos: pofesionales.professionalRatinglist!.count, descripcion: pofesionales.descricion!, visit: pofesionales.visitas!, ratin: pofesionales.professionalRatinglist?.count ?? 0, isverifield: pofesionales.isVerified!))
                                            }
                                        }
                                    }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
    
           
    }

    




