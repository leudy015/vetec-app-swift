//
//  ValoracionesView.swift
//  vetecapp
//
//  Created by Leudy on 09/07/2020.
//

import SwiftUI
import URLImage

struct ValoracionesView: View {
    
    var id = ""
    
    @ObservedObject private var dataValoraciones: setValoraciones = setValoraciones()
    
    var body: some View {
    return ForEach(dataValoraciones.valoraciones) { dat in
        VStack{
            HStack{
                URLImage(URL(string: "https://server.vetec.es/assets/images/\(dat.customer.avatar)")!){
                    $0.image.resizable()
                       }
                        .clipShape(Circle())
                        .frame(width: 40, height: 40)
                VStack(alignment: .leading){
                    Text("\(dat.customer.nombre) \(dat.customer.apellidos)").lineLimit(1)
                        .font(.system(size: 14))
                    
                    Text(dat.customer.ciudad).lineLimit(1)
                        .font(.system(size: 10)).foregroundColor(.gray)
                }
                
                Spacer()
                
                VStack(alignment: .trailing){
                    HStack{
                        Image(systemName: "star.fill").font(.system(size: 14)).foregroundColor(Color.orange)
                        Text("\(dat.rate)").lineLimit(1)
                            .font(.system(size: 14)).foregroundColor(.orange)
                    }
                    
                    Text("\(DateFormatter().date(from: dat.updated_at) ?? Date())")
                        .lineLimit(1)
                        .font(.system(size: 10)).foregroundColor(.gray)
                        .frame(width: 150)
                }
            }
            
            Text(dat.coment).foregroundColor(.gray).font(.system(size: 12)).padding(.leading, 42).padding(.top, 5)
            
            VStack(alignment: .leading){
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    HStack{
                        Image(systemName: "translate").font(.system(size: 12)).foregroundColor(Color("main")).frame(alignment: .leading)
                        Text("Traducir").lineLimit(1).frame(alignment: .leading)
                            .font(.system(size: 12)).foregroundColor(Color("main"))
                    }
                })
            }
           
            
        }.padding(.top, 20)
    }
  }
    
}


struct Customers : Identifiable {
    
    var id: String
    var nombre : String
    var apellidos : String
    var ciudad : String
    var avatar : String

    init(
        id: String,
        nombre : String,
        apellidos : String,
        ciudad : String,
        avatar : String
    ) {
        self.id = id
        self.nombre = nombre
        self.apellidos = apellidos
        self.ciudad = ciudad
        self.avatar = avatar
    }
}


struct ProfessionalRatinglist : Identifiable {
    var id : String
    var rate : Int
    var updated_at : String
    var coment : String
    var averages: Double
    var customer : Customers

    init(
        id : String,
        rate : Int,
        updated_at : String,
        coment : String,
        averages: Double,
        customer: Customers
    ) {
        self.id = id
        self.rate = rate
        self.coment = coment
        self.updated_at = updated_at
        self.averages = averages
        self.customer = customer
        }
}


class setValoraciones: ObservableObject {
    @Published var valoraciones: [ProfessionalRatinglist]
    init() {
        self.valoraciones = []
        loadData()
    }
    
    func loadData() {
    
        Network.shared.apollo.fetch(query: GetProfessionalRatingQuery(id: "5eac1a0a17814373f32c2a3c")) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getProfessionalRating?.list {
                    for values in items {
                        if let val = values {
                            
                            let array = [val.rate!]
                            let average = array.reduce(0) {
                                return $0 + ($1)/(array.count)
                            }
                            
                            self.valoraciones.append(ProfessionalRatinglist(id: val.id!, rate: val.rate!, updated_at: val.updatedAt!, coment: val.coment!, averages: Double(average),
                                                                            customer: Customers(id: val.customer?.id! ?? "", nombre: val.customer?.nombre! ?? "", apellidos: val.customer?.apellidos! ?? "", ciudad: val.customer?.ciudad! ?? "", avatar: val.customer?.avatar! ?? "")
                            
                            
                            ))
                            
                        }
                    }
                }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
      }
