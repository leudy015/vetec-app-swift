//
//  DetailsProfesional.swift
//  iOS
//
//  Created by Leudy on 07/07/2020.
//

import SwiftUI
import URLImage

struct DetailsProfesional : View {
    @State var Addfavourites  = false
    
    var data:Profesional
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false, content: {
            
            GeometryReader{reader in
                
                // Type 2 Parollax....
                
                if reader.frame(in: .global).minY > -480 {
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.avatar)")!){
                        $0.image.resizable()
                        }
                        .aspectRatio(contentMode: .fill)
                        // moving View Up....
                        .offset(y: -reader.frame(in: .global).minY)
                    // going to add parallax effect....
                        .frame(width: UIScreen.main.bounds.width, height:  reader.frame(in: .global).minY > 0 ? reader.frame(in: .global).minY + 480 : 480)
                }
  
                // Type 1 Parallax....
                
//                Image("main")
//                    .resizable()
//                    .aspectRatio(contentMode: .fill)
//                    // moving View Up....
//                    .offset(y: -reader.frame(in: .global).minY)
//                // going to add parallax effect....
//                    .frame(width: UIScreen.main.bounds.width, height:  reader.frame(in: .global).minY + 480)
                
                
            }
            // default frame...
            .frame(height: 480)
            
            VStack(alignment: .leading,spacing: 15){
                
                HStack{
                    
                    VStack(alignment: .leading) {
                        HStack{
                            Text("\(data.nombre) \(data.apellidos)")
                                .font(.system(size: 35, weight: .bold))
                                .lineLimit(1)
                                .frame(maxWidth: 250, alignment: .leading)
                            
                            if data.isverifield {
                                Image(systemName: "checkmark.seal.fill")
                                    .foregroundColor(Color("main"))
                                    .padding(.leading, -5)
                            }
                        }
                        
                        Text(data.profesion).foregroundColor(.gray)
                        
                        Text("\(data.experiencia) de experiencia").foregroundColor(Color("main"))
                    }
                    
                    Spacer()
                    
                    VStack(alignment: .trailing){
                        
                        Text("\(data.Precio) €")
                            .font(.system(size: 20, weight: .bold))
                            .lineLimit(1)
                            .frame(width: 50, alignment: .trailing)
                            .padding(.top, -30)
                        
                        Text("/consulta")
                            .font(.system(size: 12))
                            .lineLimit(2)
                            .frame(width: 70, alignment: .trailing)
                            .padding(.top, -10)
                        
                    }
                }
                    
                        

                HStack(spacing: 5){
                    
                    ForEach(1...5, id: \.self){ i in
                        
                        Image(systemName: "star.fill")
                            .foregroundColor(.orange)
                    }
                }
                
                HStack{
                    ZStack{
                        Rectangle()
                            .fill(Color("Category").opacity(0.5))
                            .cornerRadius(15)
                            .padding(10)
                            .frame(width: 100, height: 100, alignment: .center)
                        
                        VStack{
                            Image(systemName: "bandage")
                                .foregroundColor(Color("Color"))
                                .font(.system(size: 20))
                            Text("+\(data.consultas)")
                                .font(.system(size: 12))
                                .lineLimit(2)
                                .frame(width: 80, alignment: .center)
                            Text("Consultas")
                                .font(.system(size: 12))
                                .lineLimit(2)
                                .frame(width: 80, alignment: .center)
                        }
                        
                    }
                    
                    ZStack{
                        Rectangle()
                            .fill(Color("Category").opacity(0.5))
                            .cornerRadius(15)
                            .padding(10)
                            .frame(width: 100, height: 100, alignment: .center)
                        
                        VStack{
                            Image(systemName: "eye")
                                .foregroundColor(.orange)
                                .font(.system(size: 18))
                            Text("+\(data.visit)")
                                .font(.system(size: 12))
                                .lineLimit(2)
                                .frame(width: 80, alignment: .center)
                            Text("Visitas")
                                .font(.system(size: 12))
                                .lineLimit(2)
                                .frame(width: 80, alignment: .center)
                        }
                        
                    }
                    
                    ZStack{
                        Rectangle()
                            .fill(Color("Category").opacity(0.5))
                            .cornerRadius(15)
                            .padding(10)
                            .frame(width: 100, height: 100, alignment: .center)
                        
                        VStack{
                            Button(action: {
                                if data.anadidoFavorito {
                                    eliminarFavorito(profesionalID: data.id)
                                } else {
                                    
                                    let defaults = UserDefaults.standard
                                    let userID = defaults.value(forKey: "id")
                                    
                                    añadiaFavorito(userID: userID as! String, profesionalID: data.id)
                                }
                            }, label: {
                                Image(systemName: data.anadidoFavorito ? "heart.fill" : "heart")
                                    .foregroundColor(.red)
                                    .font(.system(size: 20))
                            })
                            Text(data.anadidoFavorito ? "Añadido en " : "Añadir a")
                                .font(.system(size: 12))
                                .lineLimit(2)
                                .frame(width: 80, alignment: .center)
                            Text("favoritos")
                                .font(.system(size: 12))
                                .lineLimit(2)
                                .frame(width: 80, alignment: .center)
                        }
                        
                    }
                }
                
                VStack{
                    Button(action: {
                        }) {
                            Text("Hacer consulta")
                            .font(.system(size: 14))
                                    .frame(maxWidth: .infinity)
                                    .padding()
                                    .foregroundColor(.white)
                                    .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                    .cornerRadius(10)
                                    .padding(.horizontal, 5) .padding(5)
                        
                                }
                }
                
                Text("Descripción")
                    .font(.system(size: 20, weight: .bold))
                    .padding(.top,5)
                
                Text(data.descripcion).foregroundColor(.gray)
                
                Text("Valoraciones")
                    .font(.system(size: 20, weight: .bold))
                    .padding(.top,5)
                
                ZStack(alignment: .leading){
                    Rectangle()
                        .fill(Color("Category").opacity(0.5))
                        .cornerRadius(15)
                        .frame(height: 100, alignment: .leading)
                    
                    HStack{
                        Text("\(data.ratin)")
                            .font(.system(size: 64))
                            .lineLimit(2)
                            .padding(.leading, 20)
                            .frame(width: 80, alignment: .leading)
                        
                        VStack(alignment: .leading){
                            HStack{
                                ForEach(1...5, id: \.self){ i in
                                    
                                    Image(systemName: "star.fill")
                                        .foregroundColor(.orange)
                                }
                            }
                           
                            Text("Opiniones")
                                .font(.system(size: 18))
                                .lineLimit(1)
                                .frame(width: 100, alignment: .leading)
                        }
                    }
                   
                    
                }
                
                VStack{
                    if data.ratin == 0 {
                        NoContent(content: "\(data.nombre) \(data.apellidos) aún no tiene opiniones de pacientes")
                    } else {
                        ValoracionesView(id: data.id)
                    }
                }
            }
            .padding(.top, 25)
            .padding(.horizontal)
            .background(Color("back"))
            .cornerRadius(20)
            .offset(y: -35)
        })
        .edgesIgnoringSafeArea(.all)
        .background(Color("back").edgesIgnoringSafeArea(.all))
        .background(Color("back"))
    }
}

