//
//  DetailsConsulta.swift
//  iOS
//
//  Created by Leudy on 13/07/2020.
//

import SwiftUI
import URLImage

struct DetailsConsulta: View {
    
    @State var showingDetailsMascota = false
    var data: Consulta
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false, content: {
            
            GeometryReader{reader in
                
                // Type 2 Parollax....
                
                if reader.frame(in: .global).minY > -280 {
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.profesional.avatar)")!){
                        $0.image.resizable()
                        }
                        .aspectRatio(contentMode: .fill)
                        // moving View Up....
                        .offset(y: -reader.frame(in: .global).minY)
                    // going to add parallax effect....
                        .frame(width: UIScreen.main.bounds.width, height:  reader.frame(in: .global).minY > 0 ? reader.frame(in: .global).minY + 280 : 280)
                }

                // Type 1 Parallax....
                
       //                Image("main")
       //                    .resizable()
       //                    .aspectRatio(contentMode: .fill)
       //                    // moving View Up....
       //                    .offset(y: -reader.frame(in: .global).minY)
       //                // going to add parallax effect....
       //                    .frame(width: UIScreen.main.bounds.width, height:  reader.frame(in: .global).minY + 480)
                
                
            }
            // default frame...
            .frame(height: 280)
            
            VStack(alignment: .leading,spacing: 15){
                
                HStack{
                    
                    VStack(alignment: .leading) {
                        
                        HStack{
                            Text("\(data.profesional.nombre) \(data.profesional.apellidos)")
                                .font(.system(size: 22))
                                .lineLimit(1)
                                .frame(alignment: .leading)
                            
                            if data.profesional.isverifield {
                                Image(systemName: "checkmark.seal.fill")
                                    .foregroundColor(Color("main"))
                                    .padding(.leading, -5)
                            }
                        }
                        
                        Text(data.profesional.profesion).foregroundColor(Color("main"))
                        
                        Text("\(data.profesional.experiencia) de experiencia").foregroundColor(.gray).padding(.top, 5)
                    }
                    
                    Spacer()
                    
                    VStack(alignment: .trailing) {
                        Button(action: {}, label: {
                            HStack{
                                Text("Ayuda").foregroundColor(Color.gray)
                                    .font(.system(size: 14))
                                Image(systemName: "questionmark.circle").foregroundColor(Color("main"))
                            }
                                .font(.system(size: 16))
                                .frame(maxWidth: 90)
                                .padding()
                                .foregroundColor(.white)
                                .background(Color("Category").opacity(0.5))
                                .cornerRadius(50)
                                .padding(.horizontal, 5)
                                
                        })
                        
                    }.padding(.top, -40)
                }
                
                Text("Detalles del pedido")
                    .font(.system(size: 20, weight: .bold))
                    .padding(.top,5)
                
                VStack{
                    HStack{
                        Text("Total")
                            .font(.system(size: 18))
                            .padding(.top, 10)
                        
                        
                        Spacer()
                        
                        Text("\(data.profesional.Precio)€")
                            .font(.system(size: 18))
                            .padding(.top,5)
                            .lineLimit(1)
                            .frame(alignment: .trailing)
                            .foregroundColor(.gray)
                            .padding(.top, 10)
                    }
                    
                    HStack{
                        Text("ID consulta")
                            .font(.system(size: 18))
                            .padding(.top, 10)
                        
                        Spacer()
                        
                        Text("\(data.id)")
                            .font(.system(size: 18))
                            .padding(.top, 10)
                            .frame(width: 160)
                            .lineLimit(1)
                            .frame(alignment: .trailing)
                            .foregroundColor(.gray)
                    }
                    
                    HStack{
                        Text("Estado")
                            .font(.system(size: 18))
                            .padding(.top, 10)
                        
                        Spacer()
                        
                        Text("\(data.estado)")
                            .font(.system(size: 18))
                            .padding(.top, 10)
                            .lineLimit(1)
                            .frame(alignment: .trailing)
                            .foregroundColor(data.estado == "Pendiente de pago" ? Color.red : Color.green)
                    }
                    
                    HStack{
                        Text("Fecha")
                            .font(.system(size: 18))
                            .padding(.top, 10)
                        
                        Spacer()
                        
                        Text("\(DateFormatter().date(from: data.created_at) ?? Date())")
                            .font(.system(size: 18))
                            .padding(.top, 10)
                            .frame(alignment: .trailing)
                            .lineLimit(1)
                            .frame(width: 160)
                            .foregroundColor(.gray)
                    }
                }
                      
            
                Text("Mascota")
                    .font(.system(size: 20, weight: .bold))
                    .padding(.top,5)
                
                HStack {
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.mascota.avatar)")!){
                        $0.image.resizable().renderingMode(.original)
                        }
                        .clipShape(Circle())
                        .frame(width: 70, height: 70)
                    .padding(.all, 10)
                    
                    VStack{
                        Text("\(data.mascota.name)")
                            .font(.system(size: 24, weight: .bold))
                            .lineLimit(1)
                            .frame(maxWidth: .infinity, alignment: .leading)
                        
                        Text("\(data.mascota.especie)")
                            .lineLimit(1)
                            .frame(maxWidth: .infinity, alignment: .leading)
                    }
                    
                    Spacer()
                    
                    VStack(alignment: .trailing){
                        
                        Button(action: {
                            self.showingDetailsMascota.toggle()
                        }, label: {
                            Text("Detalles")
                                .font(.system(size: 14))
                                .frame(maxWidth: 90)
                                .padding()
                                .foregroundColor(.white)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(7)
                                .padding(.horizontal, 5) .padding(5)
                                
                        }).sheet(isPresented: $showingDetailsMascota) {
                            DetailsMascota(data: data.mascota)
                            }
                    }
                }.background(Color("Category").opacity(0.5))
                .cornerRadius(10)
                
                Text("Los sintomas de \(data.mascota.name)")
                    .font(.system(size: 20, weight: .bold))
                    .padding(.top,5)
                
                Text(data.nota).foregroundColor(.gray)
            }
            .padding(.top, 25)
            .padding(.horizontal)
            .background(Color("back"))
            .cornerRadius(20)
            .offset(y: -35)
        })
        .edgesIgnoringSafeArea(.all)
        .background(Color("back").edgesIgnoringSafeArea(.all))
        .background(Color("back"))
    }
}
