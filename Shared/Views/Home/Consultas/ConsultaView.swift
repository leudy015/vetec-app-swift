//
//  ConsultaView.swift
//  iOS
//
//  Created by Leudy on 09/07/2020.
//

import SwiftUI
import URLImage

struct ConsultaView: View {
    
    @ObservedObject private var dataConsulta: setConsulta = setConsulta()
    
    var body: some View {
        ForEach(dataConsulta.consulta){ datos in
            if dataConsulta.consulta.count == 0 {
                NoContent(content: "Aún no has realizado ninguna consulta")
            }else{
                CompConsulta(data: datos)
            }
                
        }
    }
}

struct CompConsulta: View {
    
    @State var showingDetail = false
    
    var data: Consulta
    
    var body: some View {
        VStack{
            ZStack(alignment: .topLeading){
                Rectangle()
                    .fill(Color("Background"))
                    .cornerRadius(15)
                    .padding(.horizontal, 10)
                    .padding(.vertical, 6)
                    .frame(height: 300)
                    .overlay(RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("border"), lineWidth: 0.5)
                                .padding(.horizontal, 10)
                                .padding(.vertical, 6)
                                .shadow(radius: 10)
                    )
                
                VStack{
                HStack {
                VStack(spacing: 15) {
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.profesional.avatar)")!){
                        $0.image.resizable().renderingMode(.original)
                        }
                        .clipShape(Circle())
                        .frame(width: 70, height: 70)
                        .padding(.top, 25)
                    
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.mascota.avatar)")!){
                        $0.image.resizable().renderingMode(.original)
                        }
                        .clipShape(Circle())
                        .frame(width: 70, height: 70)
                        .padding(.top, -45)
                
                        }
                    
                    HStack(alignment: .top) {
                        
                        VStack(alignment: .leading) {
                            
                        HStack{
                            Text("\(data.profesional.nombre) \(data.profesional.apellidos)")
                            .foregroundColor(Color("text"))
                            .lineLimit(1)
                            .frame(maxWidth: 170, alignment: .leading)
                            .font(.system(size: 18))
                            .padding(.leading, 15)
                                .padding(.top, 25)
                            
                            if data.profesional.isverifield {
                                Image(systemName: "checkmark.seal.fill")
                                    .foregroundColor(Color("main"))
                                    .padding(.leading, -5)
                                    .font(.system(size: 16))
                                    .padding(.top, 25)
                            }
                        }
                            
                            Text("\(data.estado)")
                                .foregroundColor(data.estado == "Pendiente de pago" ? Color.red : Color.green)
                                .lineLimit(1)
                                .frame(width: 170, alignment: .leading)
                                .font(.system(size: 12))
                                .padding(.leading, 15)
                            
                            
                            ZStack(alignment: .leading) {
                                VStack(alignment: .leading){
                                    Text(data.profesional.profesion)
                                        .foregroundColor(Color.gray)
                                        .lineLimit(1)
                                        .font(.system(size: 12))
                                        .padding(.leading, 20)
                                    
                                    Text("\(data.profesional.experiencia) de experiencia")
                                        .lineLimit(1)
                                        .font(.system(size: 12))
                                        .padding(.leading, 20)
                                     }
                            
                            }.frame(width: 270, height: 60, alignment: .leading)
                        
                        }
                        
                    }
                }
                    
                    HStack(alignment: .center, spacing: 45){
                        
                        VStack(alignment: .center, spacing: 5){
                            
                            Text(data.mascota.name)
                                .foregroundColor(Color.gray)
                                .lineLimit(1)
                                .font(.system(size: 12))
                                .padding(.leading, 15)
                            
                            Text(data.mascota.especie)
                                .foregroundColor(Color("text"))
                                .lineLimit(1)
                                .font(.system(size: 14))
                                .padding(.leading, 15)
                        }
                        
                        
                        VStack{
                            Image(systemName: "flag")
                                    .font(.system(size: 16))
                                .foregroundColor(Color("bg"))
                            
                            Text(data.profesional.ciudad)
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }.padding(.leading, 20)
                        
                        VStack{
                            Image(systemName: "eurosign.circle")
                                    .font(.system(size: 16))
                                    .foregroundColor(Color("main"))
                            
                            Text("\(data.profesional.Precio)€/Consulta")
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }
                        
                        
                        VStack{
                            Image(systemName: "star")
                                    .font(.system(size: 16))
                                .foregroundColor(Color.orange)
                            
                            Text("\(data.profesional.ratin)")
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }
                        }.padding(.top, 5)
                    
                    HStack{
                        if data.estado == "Pendiente de pago" {
                            Button(action: {
                                }) {
                                    Text("Completar pago")
                                    .font(.system(size: 14))
                                            .frame(maxWidth: .infinity)
                                            .padding()
                                            .foregroundColor(.white)
                                            .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                            .cornerRadius(10)
                                            .padding(.horizontal, 5) .padding(5)
                                    }
                        } else if data.estado == "Nueva"{
                            
                            Text("En espera de aceptación del profesional").padding(10).foregroundColor(.orange).font(.system(size: 12))
                            
                        } else if data.estado == "Aceptada" {
                            
                            Button(action: {
                                }) {
                                    Text("Iniciar Chat")
                                    .font(.system(size: 14))
                                            .frame(maxWidth: .infinity)
                                            .padding()
                                            .foregroundColor(.white)
                                            .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                            .cornerRadius(10)
                                            .padding(.horizontal, 5) .padding(5)
                                    }
                            
                        } else if data.estado == "Finalizada" {
                            
                            Button(action: {
                                }) {
                                    Text("Valorar")
                                    .font(.system(size: 14))
                                            .frame(maxWidth: .infinity)
                                            .padding()
                                            .foregroundColor(.white)
                                            .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                            .cornerRadius(10)
                                            .padding(.horizontal, 5) .padding(5)
                                    }
                            
                        } else if data.estado == "Valorada" {
                            
                            Button(action: {
                                }) {
                                    Text("Repetir")
                                    .font(.system(size: 14))
                                            .frame(maxWidth: .infinity)
                                            .padding()
                                            .foregroundColor(.white)
                                            .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                            .cornerRadius(10)
                                            .padding(.horizontal, 5) .padding(5)
                                    }
                            
                        }
                        
                        Button(action: {
                            self.showingDetail.toggle()
                            }) {
                            Text("Detalles")
                                .font(.system(size: 14))
                                        .frame(maxWidth: .infinity)
                                        .padding()
                                        .foregroundColor(Color("text"))
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                            .stroke(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing), lineWidth: 2)
                                                )
                                        .cornerRadius(10).padding(5)
                            
                        }.sheet(isPresented: $showingDetail) {
                                DetailsConsulta(data: data)
                        }
                        
                        }.padding(.top, 30)
                    .padding(.horizontal, 15)
          }
        }
      }.cornerRadius(15)
        .shadow(color: Color("shadow"), radius: 30)
        .contextMenu {
            Button(action: {
                self.showingDetail.toggle()
            }) {
                
                HStack{
                    Image(systemName: "person")
                    Text("Ver Detalles")
                }
            }
            
            Button(action: {
                
            }) {
                
                HStack{
                    Image(systemName: "calendar")
                    Text("Repetir Consulta")
                }
            }
            
        }
   }
}

struct ProfesionalConsulta: Identifiable{
    var id: String
    var nombre: String
    var apellidos: String
    var ciudad: String
    var experiencia: String
    var avatar: String
    var profesion: String
    var anadidoFavorito: Bool
    var Precio: String
    var isverifield: Bool
    var ratin: Int
   
    init(
        id: String,
        nombre: String,
        apellidos: String,
        ciudad: String,
        experiencia: String,
        avatar: String,
        profesion: String,
        anadidoFavorito: Bool,
        Precio: String,
        isverifield: Bool,
        ratin: Int
    ){
        self.id = id
        self.nombre = nombre
        self.apellidos = apellidos
        self.ciudad = ciudad
        self.experiencia = experiencia
        self.avatar = avatar
        self.profesion = profesion
        self.anadidoFavorito = anadidoFavorito
        self.Precio = Precio
        self.isverifield = isverifield
        self.ratin = ratin
    }

}


struct ClienteConsulta: Identifiable{
        var id: String
        var nombre: String
        var apellidos: String
        var ciudad: String
        var avatar: String

   
    init(
        id: String,
        nombre: String,
        apellidos: String,
        ciudad: String,
        avatar: String
    ){
        self.id = id
        self.nombre = nombre
        self.apellidos = apellidos
        self.ciudad = ciudad
        self.avatar = avatar
    }

}

struct Consulta: Identifiable{
        var id: String
        var estado: String
        var nota: String
        var created_at: String
        var mascota: Mascota
        var usuario: ClienteConsulta
        var profesional: ProfesionalConsulta
    
      

   
    init(
        id: String,
        estado: String,
        nota: String,
        created_at: String,
        mascota: Mascota,
        usuario: ClienteConsulta,
        profesional: ProfesionalConsulta
     
    ){
        self.id = id
        self.estado = estado
        self.nota = nota
        self.created_at = created_at
        self.mascota = mascota
        self.usuario = usuario
        self.profesional = profesional
      
    }

}


class setConsulta: ObservableObject {
    @Published var consulta: [Consulta]
    
    init() {
        let defaults = UserDefaults.standard
        let id = defaults.value(forKey: "id")
        print("running loadData")
        self.consulta = []
        loadData(id: id as! String)
    }
    
    func loadData(id: String) {
        
        
        
        Network.shared.apollo.fetch(query: GetConsultaByUsuarioQuery(usuarios: id)) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getConsultaByUsuario?.list {
                                        for datas in items {
                                            if let value = datas {
                                            //Do whatever here with your Graphql Result
                                                self.consulta.append(Consulta(id: value.id!, estado: value.estado!, nota: value.nota!, created_at: value.createdAt!, mascota: Mascota(
                                                                                id: value.mascota?.id! ?? "",
                                                                                name: value.mascota?.name! ?? "",
                                                                                age: value.mascota?.age! ?? "",
                                                                                usuario: value.mascota?.usuario! ?? "",
                                                                                avatar: value.mascota?.avatar! ?? "",
                                                                                especie: value.mascota?.especie! ?? "",
                                                                                raza: value.mascota?.raza! ?? "",
                                                                                genero: value.mascota?.genero! ?? "",
                                                                                peso: value.mascota?.peso! ?? "",
                                                                                microchip: value.mascota?.microchip! ?? "",
                                                                                vacunas: value.mascota?.vacunas! ?? "",
                                                                                alergias: value.mascota?.alergias! ?? "",
                                                                                castrado: value.mascota?.castrado! ?? false),
                                                                                usuario: ClienteConsulta(
                                                                                    id: value.usuario?.id! ?? "",
                                                                                    nombre: value.usuario?.nombre! ?? "",
                                                                                    apellidos: value.usuario?.apellidos! ?? "",
                                                                                    ciudad: value.usuario?.ciudad! ?? "",
                                                                                    avatar: value.usuario?.avatar! ?? ""),
                                                                                profesional: ProfesionalConsulta(
                                                                                    id: value.profesional?.id! ?? "",
                                                                                    nombre: value.profesional?.nombre! ?? "",
                                                                                    apellidos: value.profesional?.apellidos! ?? "",
                                                                                    ciudad: value.profesional?.ciudad! ?? "",
                                                                                    experiencia: value.profesional?.experiencia! ?? "",
                                                                                    avatar: value.profesional?.avatar! ?? "",
                                                                                    profesion: value.profesional?.profesion! ?? "",
                                                                                    anadidoFavorito: value.profesional?.anadidoFavorito! ?? false,
                                                                                    Precio: value.profesional?.precio! ?? "",
                                                                                    isverifield: value.profesional?.isVerified! ?? false,
                                                                                    ratin: value.profesional?.professionalRatinglist?.count ?? 0
                                                                                    )))
                                            }
                                        }
                                    }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
    }

    



