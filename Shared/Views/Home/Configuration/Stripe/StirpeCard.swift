//
//  StirpeCard.swift
//  vetecapp
//
//  Created by Leudy on 03/07/2020.
//

import SwiftUI
import Stripe

struct StirpeCard: View {
    
    @ObservedObject var paymentContextDelegate = PaymentContextDelegate()

    var body: some View {
        ShoppingView(paymentContextDelegate: paymentContextDelegate)
    }
}

