//
//  ChatListView.swift
//  iOS
//
//  Created by Leudy on 03/07/2020.
//

import SwiftUI

struct ChatListView: View {
    
    @State var showingChat = false
    @State private var searchText : String = ""
    
    var body: some View {
        
        VStack{
            ScrollView(.vertical, showsIndicators: false){
                SearchBar(text: $searchText)
                Button(action: {
                    self.showingChat.toggle()
                }) {
                    Text("Chats")
                }.sheet(isPresented: $showingChat) {
                      ChatView()
                }
            }
    }
}

}
