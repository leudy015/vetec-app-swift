//
//  ContentView.swift
//  Shared
//
//  Created by Leudy on 29/06/2020.
//

import SwiftUI
import StoreKit
import URLImage


struct ContentView: View {
    
    @State var showingNotification = false
    @State var showingChat = false
    @State private var showingSheet = false
    @State  var showingFilter = false
    @State private var searchText : String = ""
        
    var body: some View {
        
        TabView {
            NavigationView {
                
               HomeView()
                
            .navigationBarTitle(Text("Veterinarios"))
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.showingNotification.toggle()
                                    }) {
                                        
                                        Image(systemName: "bell").imageScale(.large)
                                        
                                    }.sheet(isPresented: $showingNotification) {
                                        NotificationView()
                                    },
                                trailing:
                                    
                                    NavigationLink(destination: SearchView()
                                        .navigationBarTitle("Buscar")
                                        .navigationBarItems(trailing:
                                                                Button(action: {
                                                                    self.showingFilter.toggle()
                                                                }, label: {
                                                                    
                                                                    Image(systemName: "slider.horizontal.3")
                                                            
                                                                }).sheet(isPresented: $showingFilter) {
                                                                    Text("Filtros")
                                                                }
                                                    
                                                            )
                                    
                                    ){
                                        Image(systemName: "magnifyingglass").imageScale(.large)
                                    }
                                )
                        }
             .tabItem {
                Image(systemName: "square.grid.2x2.fill").font(.system(size: 22))
                Text("Explorar")
              }
            
            //Screen Dos
            
            NavigationView {
                ScrollView(.vertical, showsIndicators: false, content: {
                    ConsultaView()
                })
            .navigationBarTitle(Text("Consultas"))
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.showingNotification.toggle()
                                    }) {
                                        
                                        Image(systemName: "bell").imageScale(.large)
                                        
                                    }.sheet(isPresented: $showingNotification) {
                                        NotificationView()
                                    },
                                trailing:
                                    NavigationLink(destination: SearchView()
                                                    .navigationBarTitle("Buscar")
                                                    .navigationBarItems(trailing:
                                                                            Button(action: {
                                                                                self.showingFilter.toggle()
                                                                            }, label: {
                                                                                
                                                                                Image(systemName: "slider.horizontal.3")
                                                                        
                                                                            }).sheet(isPresented: $showingFilter) {
                                                                                Text("Filtros")
                                                                            }
                                                                
                                                                        )
                                    
                                    ){
                                        Image(systemName: "magnifyingglass").imageScale(.large)
                                    }
                )
                
            }
             .tabItem {
                Image(systemName: "calendar").font(.system(size: 22))
                Text("Consultas")
              }
            
            //Screen Tres
            NavigationView {
            
                ChatsLista()
                    
            .navigationBarTitle(Text("Chats"))
            .navigationBarItems(leading:
                         Button(action: {
                             self.showingNotification.toggle()
                                }) {
                    
                                  Image(systemName: "bell").imageScale(.large)
                    
                                }.sheet(isPresented: $showingNotification) {
                                      NotificationView()
                                 },
                                trailing:
                                    NavigationLink(destination: SearchView()
                                                    .navigationBarTitle("Buscar")
                                                    .navigationBarItems(trailing:
                                                                            Button(action: {
                                                                                self.showingFilter.toggle()
                                                                            }, label: {
                                                                                
                                                                                Image(systemName: "slider.horizontal.3")
                                                                        
                                                                            }).sheet(isPresented: $showingFilter) {
                                                                                Text("Filtros")
                                                                            }
                                                                
                                                                        )
                                    ){
                                        Image(systemName: "magnifyingglass").imageScale(.large)
                                    }
                                )
                
                            }
            
            .tabItem {
               Image(systemName: "bubble.left.and.bubble.right")
                .font(.system(size: 22))
               Text("Chats")
            }
                        
            //Screen Cuatro
            
            NavigationView {
            
                ScrollView(.vertical, showsIndicators: false, content: {
                    ListwihtDataFavoritos()
                })
            .navigationBarTitle(Text("Favoritos"))
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.showingNotification.toggle()
                                    }) {
                                        
                                        Image(systemName: "bell").imageScale(.large)
                                        
                                    }.sheet(isPresented: $showingNotification) {
                                        NotificationView()
                                    },
                                trailing:
                                    NavigationLink(destination: SearchView()
                                                    .navigationBarTitle("Buscar")
                                                    .navigationBarItems(trailing:
                                                                            Button(action: {
                                                                                self.showingFilter.toggle()
                                                                            }, label: {
                                                                                
                                                                                Image(systemName: "slider.horizontal.3")
                                                                        
                                                                            }).sheet(isPresented: $showingFilter) {
                                                                                Text("Filtros")
                                                                            }
                                                                
                                                                        )
                                    ){
                                        Image(systemName: "magnifyingglass").imageScale(.large)
                                    }
                )
                
            }
             .tabItem {
                Image(systemName: "heart")
                    .font(.system(size: 22))
                Text("Favorito")
              }
            
            //Screen Cinco
            
            NavigationView {
                
                List {
                    
                    Section {
                        NavigationLink(destination: DetalProfile()
                                        .navigationBarTitle(Text("Editar datos"), displayMode: .inline)
                                        .navigationBarItems(trailing:
                                                                Button(action: {
                                                                    
                                                                }, label: {
                                                                    
                                                                    Text("Guardar")
                                                            
                                                                })
                                                    
                                                            )
                                                        )
                        {
                            ProfileViwe(image: "https://server.vetec.es/assets/images/1589211302167-file.jpeg", name: "Leudy Martes")
                        }
                        
                }
                    
                    Section {
                        NavigationLink(destination: Text("Paiment")
                                        .navigationBarTitle(Text("Metódo de pago"), displayMode: .inline)
                                        .navigationBarItems(trailing:
                                                                Button(action: {
                                                                    
                                                                }, label: {
                                                                    
                                                                    Text("Editar")
                                                            
                                                                })
                                                    
                                                            )
                        
                        ){
                            BtnView(image: "credit", name: "Metódos de pago")
                                
                        }
                        NavigationLink(destination: Text("plus")
                                        .navigationBarTitle(Text("Vetec PLUS"), displayMode: .inline)
                                        .navigationBarItems(trailing:
                                                                Button(action: {
                                                                    
                                                                }, label: {

                                                                    Image(systemName: "info.circle")
                                                                
                                                                })
                                                    
                                                            )
                        ){
                            BtnView(image: "plus", name: "Vetec Plus")
                               
                        }
                        NavigationLink(destination: Text("contacto")
                                        .navigationBarTitle(Text("Contacto"), displayMode: .inline)
                        ){
                            BtnView(image: "contact", name: "Contacto")
                                
                        }
                        NavigationLink(destination: Text("cupones")
                                        .navigationBarTitle(Text("Cupones"), displayMode: .inline)
                                        .navigationBarItems(trailing:
                                                                Button(action: {
                                                                    
                                                                }, label: {
                                                                    
                                                                    Text("Copiar")
                                                            
                                                                })
                                                    
                                                            )
                        ){
                            BtnView(image: "gift", name: "Cupones")
                                
                        }
                    }
                    
                    Section {
                        Button(action: {
                            UIApplication.shared.open(URL(string: "mailto:info@vetec.es")!)
                        }) {
                            
                            BtnView(image: "help", name: "Ayuda")
                        }
                        
                        NavigationLink(destination: Text("notoficaciones")
                                .navigationBarTitle(Text("Notificaciones"), displayMode: .inline)
                        ){
                            BtnView(image: "notification", name: "Notificaciones")
                               
                        }
                        
                        NavigationLink(destination: Text("amigo")
                                .navigationBarTitle(Text("Compratir con un amigo"), displayMode: .inline)
                        ){
                            BtnView(image: "compartir", name: "Invita un amigo")
                                
                        }
                        
                        Button(action: {
                            SKStoreReviewController.requestReview()
                        }) {
                            
                            BtnView(image: "apple", name: "Valorar App")
                        }
                    
                    }
                    
                    Section {
                        Button(action: {
                            self.showingSheet = true
                        }) {
                            
                            BtnView(image: "shares", name: "Compartir")
                        }.sheet(isPresented: $showingSheet,
                                content: {
                                 ActivityView(activityItems: [NSURL(string: "https://vetec.es")!] as [Any], applicationActivities: nil) })

                        
                    }
                    
                    Section {
                        Logout(image: "salir", name: "Cerrar sesión")
                    }
                }
                    
                .navigationBarTitle(Text("Ajustes"))
                .listStyle(GroupedListStyle())
                .navigationBarItems(leading:
                        Button(action: {
                            self.showingNotification.toggle()
                        }) {
                            
                            Image(systemName: "bell").imageScale(.large)
                            
                        }.sheet(isPresented: $showingNotification) {
                            NotificationView()
                        },
                    trailing:
                        NavigationLink(destination: SearchView()
                                        .navigationBarTitle("Buscar")
                                        .navigationBarItems(trailing:
                                                                Button(action: {
                                                                    self.showingFilter.toggle()
                                                                }, label: {
                                                                    
                                                                    Image(systemName: "slider.horizontal.3")
                                                            
                                                                }).sheet(isPresented: $showingFilter) {
                                                                    Text("Filtros")
                                                                }
                                                    
                                                            )
                        
                        ){
                            Image(systemName: "magnifyingglass").imageScale(.large)
                        }
                    )
            }.tabItem {
                Image(systemName: "gear")
                    .font(.system(size: 22))
                Text("Ajustes")
                
              }
        }
        
    }


struct ActivityView: UIViewControllerRepresentable {

    let activityItems: [Any]
    let applicationActivities: [UIActivity]?

    func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityView>) -> UIActivityViewController {
        return UIActivityViewController(activityItems: activityItems,
                                        applicationActivities: applicationActivities)
    }

    func updateUIViewController(_ uiViewController: UIActivityViewController,
                                context: UIViewControllerRepresentableContext<ActivityView>) {

    }
}


struct BtnView: View {
    
    var image = ""
    var name = ""
    
    var body: some View{
     
            HStack(spacing: 15){
                Image(image).renderingMode(.original).resizable().frame(width: 30, height: 30)
                    .cornerRadius(7)
                 
                Text(name)
                    .font(.system(size: 18))
                    .foregroundColor(Color("text"))
                
                Spacer(minLength: 10)
                
            }
            .padding(5)
            .foregroundColor(Color.black.opacity(0.5))
    }
}

    
    struct ProfileViwe: View {
        
        var image = ""
        var name = ""
    
        
        var body: some View{
         
                HStack(spacing: 15){
                    URLImage(URL(string: image)!){
                        $0.image.resizable().renderingMode(.original)
                        }
                        .frame(minWidth: 60.0, maxWidth: 60.0, minHeight: 60.0, maxHeight: 60.0)
                        .clipped()
                        .clipShape(Circle())
                        
                    VStack(alignment: .leading) {
                        Text(name)
                            .font(.system(size: 22))
                            .foregroundColor(Color("text"))
                        
                        Text("Editar datos, Núm. móvil, email")
                            .font(.system(size: 14))
                            .foregroundColor(Color("text"))
                    }
                    
                    Spacer(minLength: 10)
                    
                }
                .padding(5)
                .foregroundColor(Color.black.opacity(0.5))
        }
    }
    
struct Logout: View {
    
    var image = ""
    var name = ""
    let defaults = UserDefaults.standard
    @State private var showingAlert = false
    
    var body: some View{
        Button(action: {
            self.showingAlert = true
        }) {
            
            HStack(spacing: 15){
                Image(image).renderingMode(.original).resizable().frame(width: 30, height: 30)
                    .cornerRadius(7)
                 
                Text(name)
                    .font(.system(size: 18))
                    .foregroundColor(Color("text"))
                
                Spacer(minLength: 10)
                
                Image(systemName: "chevron.right")
                    .foregroundColor(.gray)
            }
            .padding(5)
            .foregroundColor(Color.black.opacity(0.5))
        }.alert(isPresented: $showingAlert) {
            Alert(title: Text("Cerrar sesión"), message: Text("¿Seguro que quieres cerrar sesión?"),  primaryButton: Alert.Button.default(Text("Si"), action: {
                defaults.removeObject(forKey: "token")
                defaults.removeObject(forKey: "id")
                defaults.synchronize()
            }), secondaryButton: .default(Text("Cancelar")))
                }
            }
        }
    }
