//
//  FavouritesViewCard.swift
//  iOS
//
//  Created by Leudy on 09/07/2020.
//

import SwiftUI
import Apollo
import URLImage

struct ListwihtDataFavoritos: View {
    @ObservedObject private var dataProfesionalfavorite: setProfesionalfav = setProfesionalfav()
        
    var body: some View {
        return ForEach(dataProfesionalfavorite.profesional) { dat in
            
            FavouritesViewCard(data: dat)
            
        }
    }
}

struct FavouritesViewCard: View {
    @State var Addfavourites  = false
    @State var showingDetail = false
    
    var data: Profesional
    
    var body: some View {
        VStack{
                    
            ZStack(alignment: .topLeading){
                Rectangle()
                    .fill(Color("Background"))
                    .cornerRadius(15)
                    .padding(.horizontal, 10)
                    .padding(.vertical, 6)
                    .frame(height: 290)
                    .overlay(RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("border"), lineWidth: 0.5)
                                .padding(.horizontal, 10)
                                .padding(.vertical, 6)
                    )
                
                VStack{
                HStack {
                VStack(spacing: 15) {
                    URLImage(URL(string: "https://server.vetec.es/assets/images/\(data.avatar)")!){
                        $0.image.resizable().renderingMode(.original)
                        }
                        .clipShape(Circle())
                        .frame(width: 90, height: 90)
                        .padding(.leading, 25)
                        .padding(.top, 25)
                        
                        
                        
                    
                    ZStack{
                        Circle().frame(width: 30, height: 30, alignment: .center)
                            .foregroundColor(Color.green)
                            .padding(.top, -30)
                        
                        Text("90%")
                            .foregroundColor(Color.white)
                            .padding(.top, -20)
                            .lineLimit(1)
                            .font(.system(size: 10))
                    }
                }
                    
                    HStack(alignment: .top){
                        
                        VStack(alignment: .leading) {
                            
                            HStack{
                                Text("\(data.nombre) \(data.apellidos)")
                                .foregroundColor(Color("text"))
                                .lineLimit(1)
                                .frame(maxWidth: 170, alignment: .leading)
                                .font(.system(size: 18))
                                .padding(.leading, 15)
                                
                                
                                if data.isverifield {
                                    Image(systemName: "checkmark.seal.fill")
                                        .foregroundColor(Color("main"))
                                        .padding(.leading, -5)
                                        .font(.system(size: 18))
                                }
                            }.padding(.top, 20)
                            
                            
                            Text("Disponible hoy")
                                .foregroundColor(Color.green)
                                .lineLimit(1)
                                .frame(width: 170, alignment: .leading)
                                .font(.system(size: 12))
                                .padding(.leading, 15)
                            
                            
                            ZStack(alignment: .leading) {
                                VStack(alignment: .leading){
                                    Text(data.profesion)
                                        .foregroundColor(Color.gray)
                                        .lineLimit(1)
                                        .font(.system(size: 12))
                                        .padding(.leading, 20)
                                    
                                    Text("\(data.experiencia) de experiencia")
                                        .lineLimit(1)
                                        .font(.system(size: 12))
                                        .padding(.leading, 20)
                                }
                            
                            }.frame(width: 200, height: 50, alignment: .leading)
                        
                        }
                        
                        Spacer()
                        
                        VStack{
                            
                            Button(action: {
                                if data.anadidoFavorito {
                                    eliminarFavorito(profesionalID: data.id)
                                } else {
                                    
                                    let defaults = UserDefaults.standard
                                    let userID = defaults.value(forKey: "id")
                                    
                                    añadiaFavorito(userID: userID as! String, profesionalID: data.id)
                                }
                                
                            }, label: {
                                ZStack{
                                    Circle().frame(width: 40, height: 40, alignment: .center)
                                     .foregroundColor(Color("Category"))
                                    Image(systemName: data.anadidoFavorito ? "heart.fill" : "heart")
                                            .font(.system(size: 20))
                                            .foregroundColor(Color.red)
                                }
                            })
                        }.padding(.trailing, 20).padding(.top, 10)
                        
              }
            }
                    
                    HStack(alignment: .center, spacing: 30) {
                        
                        VStack(alignment: .center, spacing: 5){
                            
                            Text("\(data.votos) Votos")
                                .foregroundColor(Color.gray)
                                .lineLimit(1)
                                .font(.system(size: 12))
                                .padding(.leading, 15)
                            
                            Text("\(data.consultas) Feedback")
                                .foregroundColor(Color("text"))
                                .lineLimit(1)
                                .font(.system(size: 14))
                                .padding(.leading, 15)
                        }
                        
                        
                        VStack{
                            Image(systemName: "flag")
                                    .font(.system(size: 16))
                                .foregroundColor(Color("bg"))
                            
                            Text(data.ciudad)
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }.padding(.leading, 20)
                        
                        VStack{
                            Image(systemName: "eurosign.circle")
                                    .font(.system(size: 16))
                                    .foregroundColor(Color("main"))
                            
                            Text("\(data.Precio)€/Consulta")
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }
                        
                        
                        VStack{
                            Image(systemName: "star")
                                    .font(.system(size: 16))
                                .foregroundColor(Color.orange)
                            
                            Text("\(data.ratin)")
                                .lineLimit(1)
                                .font(.system(size: 14))
                        }
                        
                    }
                    
                    HStack{
                        Button(action: {
                            }) {
                                Text("Hacer consulta")
                                .font(.system(size: 14))
                                        .frame(maxWidth: .infinity)
                                        .padding()
                                        .foregroundColor(.white)
                                        .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                        .cornerRadius(10)
                                        .padding(.horizontal, 5) .padding(5)
                                   
                            
                        }
                        
                        Button(action: {
                            self.showingDetail.toggle()
                            }) {
                            Text("Detalles")
                                .font(.system(size: 14))
                                        .frame(maxWidth: .infinity)
                                        .padding()
                                        .foregroundColor(Color("text"))
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                            .stroke(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing), lineWidth: 2)
                                                )
                                        .cornerRadius(10).padding(5)
                            
                        }.sheet(isPresented: $showingDetail) {
                            DetailsProfesional(data: data)
                        }
                        
                        }.padding(.top, 30)
                    .padding(.horizontal, 15)
          }
            }
            .cornerRadius(15)
            .contextMenu {
                Button(action: {
                    self.showingDetail.toggle()
                }) {
                    
                    HStack{
                        Image(systemName: "person")
                        Text("Ver Detalles")
                    }
                }
                
                Button(action: {
                    
                }) {
                    
                    HStack{
                        Image(systemName: "calendar")
                        Text("Hacer Consulta")
                    }
                }
                
                
                Button(action: {
                    
                    if data.anadidoFavorito {
                        eliminarFavorito(profesionalID: data.id)
                    } else {
                        
                        let defaults = UserDefaults.standard
                        let userID = defaults.value(forKey: "id")
                        
                        añadiaFavorito(userID: userID as! String, profesionalID: data.id)
                    }
                }) {
                    
                    HStack{
                        Image(systemName: data.anadidoFavorito ? "heart.fill" : "heart")
                        Text(data.anadidoFavorito ? "Añadido en favorito" : "Añadir a favorito")
                    }
                }
                    
            }.shadow(color: Color("shadow"), radius: 30)
      }
   }
}

class setProfesionalfav: ObservableObject {
    @Published var profesional: [Profesional]

    
    init() {
        let defaults = UserDefaults.standard
        let id = defaults.value(forKey: "id")
        print("running loadData")
        self.profesional = []
        loadData(id: id as! String)
    }
    
    func loadData(id: String) {
        Network.shared.apollo.fetch(query: GetUsuarioFavoritoProQuery(id: id)) { result in
             switch result {
             case .success(let graphQLResult):
                if let items = graphQLResult.data?.getUsuarioFavoritoPro?.list {
                                        for prof in items {
                                            if let pofesionales = prof {
                                            //Do whatever here with your Graphql Result
                                                print(pofesionales.profesional?.anadidoFavorito ?? "")
                                                
                                                self.profesional.append(Profesional(
                                                                            id: pofesionales.profesional?.id! ?? "",
                                                                            
                                                                            nombre: pofesionales.profesional?.nombre! ?? "",
                                                                            
                                                                            apellidos:pofesionales.profesional?.apellidos! ?? "",
                                                                            
                                                                            ciudad: pofesionales.profesional?.ciudad! ?? "",
                                                                            
                                                                            experiencia: pofesionales.profesional?.experiencia! ?? "",
                                                                            
                                                                            avatar: pofesionales.profesional?.avatar! ?? "",
                                                                            
                                                                            consultas: pofesionales.profesional?.consultas!.count ?? 0,
                                                                            
                                                                            profesion: pofesionales.profesional?.profesion! ?? "",
                                                                            
                                                                            anadidoFavorito: pofesionales.profesional?.anadidoFavorito! ?? false,
                                                                            
                                                                            Precio: pofesionales.profesional?.precio! ?? "",
                                                                            
                                                                            votos: pofesionales.profesional?.professionalRatinglist!.count ?? 0,
                                                                            
                                                                            descripcion: pofesionales.profesional?.descricion! ?? "",
                                                                            
                                                                            visit: pofesionales.profesional?.visitas! ?? 0,
                                                                            
                                                                            ratin: pofesionales.profesional?.professionalRatinglist?.count ?? 0,
                                                                            
                                                                            isverifield: pofesionales.profesional?.isVerified ?? false
                                                ))
                                            }
                                        }
                                    }
            
             case .failure(let error):
               print("Failure! Error: \(error)")
             }
           }
        }
    
           
    }

