//
//  HomeView.swift
//  vetecapp
//
//  Created by Leudy on 03/07/2020.
//

import SwiftUI

struct HomeView: View {

    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack{
                MascotasView()
                
                Spacer().frame(height: 30)
                
                CategoryView()
                
                Spacer().frame(height: 10)
                
                ListwihtData()
           }
       }
    }
}


/*
 

 
 */

