//
//  SearchView.swift
//  iOS
//
//  Created by Leudy on 03/07/2020.
//

import SwiftUI

struct SearchView: View {
    
    @State private var searchText : String = ""

    
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack(){
               
                SearchBar(text: $searchText)
                
                NoContent()
            }
        }
    }
}
