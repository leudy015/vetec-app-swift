//
//  NoContent.swift
//  iOS
//
//  Created by Leudy on 03/07/2020.
//

import SwiftUI

struct NoContent: View {
    
   var content = ""
    var body: some View {
        VStack(alignment: .center){
            Image("vacio").renderingMode(.original).resizable().frame(width: 300, height: 200)
            Text(content).frame(alignment: .center).lineLimit(2).padding(.horizontal, 20).multilineTextAlignment(.center)
        }
    }
}
