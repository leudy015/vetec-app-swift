//
//  FacebookLogin.swift
//  iOS
//
//  Created by Leudy on 29/06/2020.
//

import SwiftUI
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Alamofire

struct FacebookLogin: View {
    
    var body: some View {
        Button(action: {
                fbLogin()
                    }) {
                        HStack {
                            Image("facebook")
                                .resizable()
                                .renderingMode(.template)
                                .frame(width: 30, height: 30)
                                .foregroundColor(.white)
                                
                            Text("Continúa con Facebook")
                                .font(.system(size: 24))
                        }
                        .frame(maxWidth: .infinity)
                        .padding()
                        .foregroundColor(.white)
                        .background(LinearGradient(gradient: Gradient(colors: [Color("AccentColor"), Color("AccentColor")]), startPoint: .leading, endPoint: .trailing))
                        .cornerRadius(6)
                        .padding(.horizontal, 10)
                        .padding(.bottom, 15)
                    }
    }
}

//MARK:- Facebook Login
    
    func fbLogin() {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: ["public_profile", "email"] ) { loginResult in
            
            switch loginResult {
            
            case .failed(let error):
                //HUD.hide()
                print(error)
            
            case .cancelled:
                //HUD.hide()
                print("User cancelled login process.")
            
            case .success( _, _, _):
                print("Logged in!")
                getFBUserData()
            }
        }
    }
    
    func getFBUserData() {
        //which if my function to get facebook user details
        if((AccessToken.current) != nil){
            
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    let dict = result as! [String : AnyObject]
                
                    print(dict)
                
                    AF.request(URL.init(string: "https://server.vetec.es/api/v1/auth/social/mobile")!, method: .post, parameters: dict, encoding: JSONEncoding.default).responseString { (response) in
                    
                        print(response)
                
                            switch response.result {
                            case .success(_):
                                if let json = response.value {
                                    
                                    print(json)
                                    
                                    Network.shared.apollo.perform(mutation: AutenticarUsuarioMutation(email: "leudy@vetec.es", password: "Leudy")) { result in
                                     switch result {
                                     case .success(let graphQLResult):
                                        if let token = graphQLResult.data?.autenticarUsuario?.data?.token,
                                           let id = graphQLResult.data?.autenticarUsuario?.data?.id {
                                        
                                            let defaults = UserDefaults.standard
                                                defaults.set(token, forKey: "token")
                                                defaults.set(id, forKey: "id")
                                                defaults.synchronize()
                                                }

                                       if let errors = graphQLResult.errors {
                                         print("Errors from server: \(errors)")
                                       }
                                     case .failure(let error):
                                       print("Error: \(error)")
                                        }
                                    }
                                }
                                break
                            case .failure(let error):
                                print(error)
                                break
                            }
                        }
                    
                    let picutreDic = dict as NSDictionary
                    let tmpURL1 = picutreDic.object(forKey: "picture") as! NSDictionary
                    let tmpURL2 = tmpURL1.object(forKey: "data") as! NSDictionary
                    _ = tmpURL2.object(forKey: "url") as! String
                    
                    let nameOfUser = picutreDic.object(forKey: "name") as! String
            
                    var tmpEmailAdd = ""
                    if let emailAddress = picutreDic.object(forKey: "email") {
                        tmpEmailAdd = emailAddress as! String
                    }
                    else {
                        var usrName = nameOfUser
                        usrName = usrName.replacingOccurrences(of: " ", with: "")
                        tmpEmailAdd = usrName+"@facebook.com"
                    }
                   
                }
                
                print(error?.localizedDescription as Any)
            })
        }
    }



struct FacebookLogin_Previews: PreviewProvider {
    static var previews: some View {
        FacebookLogin()
    }
}
