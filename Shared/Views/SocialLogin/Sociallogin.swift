//
//  Sociallogin.swift
//  iOS
//
//  Created by Leudy on 29/06/2020.
//

import SwiftUI


struct Sociallogin: View {
    var body: some View {
        VStack(spacing: 10) {
            GoogleSining()
            AppleLoging()
            FacebookLogin()
            NormalLogin()
        }
       
    }
}
