//
//  AppleLoging.swift
//  iOS
//
//  Created by Leudy on 29/06/2020.
//

import SwiftUI
import Alamofire

struct AppleLoging: View {
    
    private var currentNonce: String?

    var body: some View {
        SignInWithAppleButton(
            onRequest: { request in
                print(request)
            },
            onCompletion: { result in
                print(result)
                
                ///// Send data to server
                AF.request(URL.init(string: "https://server.vetec.es/api/v1/auth/social/mobile")!, method: .post, encoding: JSONEncoding.default).responseString { (response) in

                    print(response)

                        switch response.result {
                        case .success(_):
                            if let json = response.value {
                                
                                print(json)
                                
                                //////// Mutacion Login
                                
                                Network.shared.apollo.perform(mutation: AutenticarUsuarioMutation(email: "leudy@vetec.es", password: "Leudy")) { result in
                                 switch result {
                                 case .success(let graphQLResult):
                                    if let token = graphQLResult.data?.autenticarUsuario?.data?.token,
                                       let id = graphQLResult.data?.autenticarUsuario?.data?.id {
                                    
                                        let defaults = UserDefaults.standard
                                            defaults.set(token, forKey: "token")
                                            defaults.set(id, forKey: "id")
                                            defaults.synchronize()
                                            }

                                   if let errors = graphQLResult.errors {
                                     print("Errors from server: \(errors)")
                                   }
                                 case .failure(let error):
                                   print("Error: \(error)")
                                    }
                                }
                            }
                            break
                        case .failure(let error):
                            print(error)
                            break
                        }
                    }
                print(result)
            }
        )
        .frame(height: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .padding(10)
    }
}
