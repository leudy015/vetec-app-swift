//
//  GoogleSining.swift
//  iOS
//
//  Created by Leudy on 29/06/2020.
//

import SwiftUI
import GoogleSignIn

struct GoogleSining: View {
    var body: some View {
        Button(action: {
            Googleloginsocial.attemptLoginGoogle()
                    }) {
                        HStack {
                            Image("google")
                                .resizable()
                                .frame(width: 20, height: 20)
                                
                            Text("Continúa con Google")
                                .font(.system(size: 24))
                        }
                        .frame(maxWidth: .infinity)
                        .padding()
                        .foregroundColor(.gray)
                        .overlay(
                            RoundedRectangle(cornerRadius: 6)
                            .stroke(Color.gray, lineWidth: 1)
                                )
                        .padding(.horizontal, 10)
                        .cornerRadius(6)
                        
                    }
    }

}



struct Googleloginsocial: UIViewRepresentable {

    func makeUIView(context: UIViewRepresentableContext<Googleloginsocial>) -> UIView {
        return UIView()
    }

    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<Googleloginsocial>) {
    }

    static func attemptLoginGoogle(){
        GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
        GIDSignIn.sharedInstance()?.signIn()
    }
}

