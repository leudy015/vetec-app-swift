//
//  NormalLogin.swift
//  iOS
//
//  Created by Leudy on 30/06/2020.
//

import SwiftUI

struct NormalLogin: View {
    @State var showingRegister = false

    var body: some View {
            Button(action: {
                self.showingRegister.toggle()
                        }) {
                            HStack {
                                Image(systemName: "envelope")
                                    .foregroundColor(.white)
                                    
                                Text("Continúa con tu Email")
                                    .font(.system(size: 24))
                            }
                            .frame(maxWidth: .infinity)
                            .padding()
                            .foregroundColor(.white)
                            .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                            .cornerRadius(6)
                            .padding(.horizontal, 10)
                        } .sheet(isPresented: $showingRegister) {
                        LoginView()
                    }
    }
}
