//
//  RegiterView.swift
//  iOS
//
//  Created by Leudy on 30/06/2020.
//

import SwiftUI

struct RegiterView: View {
    var body: some View {
        NavigationView {
            FormViewl()
        .navigationBarTitle(Text("Crear cuenta"))
        }
    }
}

struct FormViewl: View {
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false){
                RegisteForm()
            }
            
        }
        
        
    }
}




struct RegisteForm: View {
    
    @State private var emailString  : String = ""
    @State private var nombre  : String = ""
    @State private var apellidos  : String = ""
    @State private var email    : String = ""
    @State private var isEmailValid : Bool   = true
    @State private var password     : String = ""
    @State var showingLogin = false
    @State private var showingAlert = false
    @State private var showingAlertNoemail = false
    @State private var showPassword = false
    @State var showingRegister = false

    var body: some View {
            VStack() {
                Spacer().frame(height: 40.0)
                HStack {
                    Image(systemName: "person")
                    .foregroundColor(.secondary)
                    
                    TextField("Nombre", text: $nombre)
                    }
                    .padding()
                    .background(Color("white"))
                    .frame(height: 55)
                    .textFieldStyle(PlainTextFieldStyle())
                    .padding([.leading, .trailing], 4)
                    .cornerRadius(7)
                    .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                    .padding([.leading, .trailing], 24)
                    .padding(.bottom, 45)
                
                HStack {
                    Image(systemName: "person.2")
                    .foregroundColor(.secondary)
                    
                    TextField("Apellidos", text: $apellidos)
                    }
                    .padding()
                    .background(Color("white"))
                    .frame(height: 55)
                    .textFieldStyle(PlainTextFieldStyle())
                    .padding([.leading, .trailing], 4)
                    .cornerRadius(7)
                    .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                    .padding([.leading, .trailing], 24)
                    .padding(.bottom, 45)
                
            
                HStack {
                    Image(systemName: "envelope")
                    .foregroundColor(.secondary)
                TextField("Correo electrónico", text: $email, onEditingChanged: { (isChanged) in
                    if !isChanged {
                        if self.textFieldValidatorEmail(self.email) {
                            self.isEmailValid = true
                        } else {
                            self.isEmailValid = false
                            self.email = ""
                        }
                    }
                })}
                    .padding()
                    .background(Color("white"))
                    .frame(height: 55)
                    .textFieldStyle(PlainTextFieldStyle())
                    .padding([.leading, .trailing], 4)
                    .cornerRadius(7)
                    .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                    .padding([.leading, .trailing], 24)
                    .padding(.bottom, 10)

                if !self.isEmailValid {
                    Text("Correo electrónico no valido")
                        .foregroundColor(Color.red)
                }
                
                Spacer().frame(height: 20.0)
                
                HStack {
                    Image(systemName: "lock")
                        .foregroundColor(.secondary)
                    if showPassword {
                        TextField("Contraseña",
                        text: $password)
                    } else {
                        SecureField("Contraseña",
                        text: $password)
                    }
                    Button(action: { self.showPassword.toggle()}) {
                        Image(systemName: showPassword ? "eye.slash" : "eye")
                        .foregroundColor(.secondary)
                                  }
                              }
                            .padding()
                            .background(Color("white"))
                            .frame(height: 55)
                            .textFieldStyle(PlainTextFieldStyle())
                            .padding([.leading, .trailing], 4)
                            .cornerRadius(7)
                            .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                            .padding([.leading, .trailing], 24)
                            .padding(.bottom, 10)
        
                Button(action: {
                    if email == "" && password == "" && nombre == "" && apellidos == ""{
                        self.showingAlert = true
                    }else {
                        let input = UsuarioInput(nombre: nombre, apellidos: apellidos, email: email, password: password )
                        Network.shared.apollo.perform(mutation: CrearUsuarioMutation(input: input))
                        { result in
                         switch result {
                         case .success(let graphQLResult):
                            
                            if graphQLResult.data?.crearUsuario?.success == false {
                                
                                let message = graphQLResult.data?.crearUsuario?.message
                                print(message ?? "Message")
                               
                            } else {
                                
                                self.showingRegister.toggle()
                                self.email = ""
                                self.password = ""
                                self.nombre = ""
                                self.apellidos = ""
                                print("Usuario agregado con exito: \(graphQLResult)")
                            }
                           if let errors = graphQLResult.errors {
                             print("Errors from server: \(errors)")
                           }
                         case .failure(let error):
                           print("Error: \(error)")
                         }
                        }

                        }
                    }) {
                        Text("Regístrarme")
                        .font(.system(size: 18))
                                .frame(maxWidth: .infinity)
                                .padding()
                                .foregroundColor(.white)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(10)
                                .padding(.horizontal, 30) .padding(10)
                    
                            }.alert(isPresented: $showingAlert) {
                                Alert(title: Text("Campos necesarios"), message: Text("Para registrarte en Vetec debes rellenar todos los campos"), dismissButton: .default(Text("Vale!")))
                            }
                
                
                Text("¿Tienes una cuenta?")
                    .padding(.vertical, 30)
                
                
                Button(action: {
                    self.showingRegister.toggle()
                }) {
                    Text("Iniciar sesión")
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 10)
                }.sheet(isPresented: $showingRegister) {
                    LoginView()
                }
            }
        }

        func textFieldValidatorEmail(_ string: String) -> Bool {
            if string.count > 100 {
                return false
            }
            let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
            let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
            return emailPredicate.evaluate(with: string)
        }}


