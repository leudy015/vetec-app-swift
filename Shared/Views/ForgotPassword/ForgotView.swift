//
//  ForgotView.swift
//  iOS
//
//  Created by Leudy on 30/06/2020.
//

import SwiftUI
import Alamofire

struct ForgotView: View {
    var body: some View {
        NavigationView {
            Forog()
        .navigationBarTitle(Text("Recuperar contraseña"))
        }
        
    }
}

struct Forog: View {
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false){
                ViewForgot()
            }
            
        }
        
        
    }
}


struct ViewForgot: View {
    
    @State private var emailString  : String = ""
    @State private var textEmail    : String = ""
    @State private var isEmailValid : Bool   = true
    @State private var showingAlertNoemail = false
    @State private var showPassword = false
    @State var showingRegister = false
    @State var showingLogin = false
    @State private var showingAlert = false

    
    var body: some View {
            VStack() {
                Spacer().frame(height: 50.0)

                HStack {
                    Image(systemName: "envelope")
                    .foregroundColor(.secondary)
                TextField("Correo electrónico", text: $textEmail, onEditingChanged: { (isChanged) in
                    if !isChanged {
                        if self.textFieldValidatorEmail(self.textEmail) {
                            self.isEmailValid = true
                        } else {
                            self.isEmailValid = false
                            self.textEmail = ""
                        }
                    }
                })}
                    .padding()
                    .background(Color("white"))
                    .frame(height: 55)
                    .textFieldStyle(PlainTextFieldStyle())
                    .padding([.leading, .trailing], 4)
                    .cornerRadius(7)
                    .overlay(RoundedRectangle(cornerRadius: 7).stroke(Color.gray))
                    .padding([.leading, .trailing], 24)
                    .padding(.bottom, 10)

                if !self.isEmailValid {
                    Text("Correo electrónico no valido")
                        .foregroundColor(Color.red)
                }
                
                Spacer().frame(height: 20.0)
    
                Button(action: {
                    if textEmail == ""{
                        self.showingAlert = true
                    }else {
                        AF.request("https://server.vetec.es/forgotpassword?email=\(textEmail)")
                            .validate(statusCode: 200..<300)
                            .validate(contentType: ["application/json"])
                            .responseData { response in
                                switch response.result {
                                case .success:
                                    self.textEmail = ""
                                    self.showingLogin.toggle()
                                case let .failure(error):
                                    print(error)
                                }
                            }
                        
                        }
                    }) {
                        Text("Enviar enlace")
                        .font(.system(size: 18))
                                .frame(maxWidth: .infinity)
                                .padding()
                                .foregroundColor(.white)
                                .background(LinearGradient(gradient: Gradient(colors: [Color("main"), Color("secundary")]), startPoint: .leading, endPoint: .trailing))
                                .cornerRadius(10)
                                .padding(.horizontal, 30) .padding(10)
                    
                            }.alert(isPresented: $showingAlert) {
                                Alert(title: Text("Campo necesario"), message: Text("Para recuperar tu contraseña debe añadir un correo electrónico"), dismissButton: .default(Text("Vale!")))
                            }.sheet(isPresented: $showingLogin) {
                                LoginView()
                            }
                
                
                Text("¿Aún no tienes una cuenta?")
                    .padding(.vertical, 30)
                
                
                
                Button(action: {
                    self.showingRegister.toggle()
                }) {
                    Text("Regístrarme")
                        .padding([.leading, .trailing], 24)
                        .padding(.bottom, 10)
                }.sheet(isPresented: $showingRegister) {
                    RegiterView()
                }
            }
        }

        func textFieldValidatorEmail(_ string: String) -> Bool {
            if string.count > 100 {
                return false
            }
            let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
            let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
            return emailPredicate.evaluate(with: string)
        }}

