//
//  ChatsLista.swift
//  iOS
//
//  Created by Leudy on 06/07/2020.
//

import SwiftUI

  struct ChatsLista: View {
      var body: some View {
        ChatsListaView()
      }
  }
    
  struct ChatsListaView : View {
      
      @State var messages : [Mensajes] = [
          
        Mensajes(id: 0, name: "Catherine", message: "Hi Kavsoft", profile: "p1", offset: 0),
        Mensajes(id: 1, name: "Emma", message: "New Video !!!", profile: "p2", offset: 0),
        Mensajes(id: 2, name: "Julie", message: "How Are You ???", profile: "p3", offset: 0),
        Mensajes(id: 3, name: "Emily", message: "Hey iJustine", profile: "p4", offset: 0),
        Mensajes(id: 4, name: "Kaviya", message: "hello ????", profile: "p5", offset: 0),
        Mensajes(id: 5, name: "Jenna", message: "Bye :)))", profile: "p6", offset: 0),
        Mensajes(id: 6, name: "Juliana", message: "Nothing Much......", profile: "p7", offset: 0),
        
      ]
      
      @State var pinnedViews : [Mensajes] = []
      
      var columns = Array(repeating: GridItem(.flexible(), spacing: 15), count: 3)
      
      @Namespace var name
      @State var showingDetail = false
      
      var body: some View{
          
          ScrollView(.vertical, showsIndicators: false, content: {
              
              // Pinned View...
              
              if !pinnedViews.isEmpty{
                  
                  LazyVGrid(columns: columns,spacing: 20){
                      
                      ForEach(pinnedViews){pinned in
                            Image(pinned.profile)
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                //padding 30 + spacing 20 = 70
                                .frame(width: (UIScreen.main.bounds.width - 70) / 3, height: (UIScreen.main.bounds.width - 70) / 3)
                                .clipShape(Circle())
                            // context menu for restoring...
                                // context menushape...
                                .contentShape(Circle())
                                .contextMenu{
                                    
                                    Button(action: {self.showingDetail.toggle()} ) {
                                        Text("Enviar mensaje")
                                    }
                                    
                                    Button(action: {
                                        
                                        // removing View...
                                        
                                        withAnimation(.default){
                                            
                                            var index = 0
                                            
                                            for i in 0..<pinnedViews.count{
                                                
                                                if pinned.profile == pinnedViews[i].profile{
                                                    
                                                    index = i
                                                }
                                            }
                                            
                                            // removing pin view...
                                            
                                            pinnedViews.remove(at: index)
                                            
                                            // adding view to main View....
                                            
                                            messages.append(pinned)
                                        }
                                        
                                    }) {
                                        
                                        Text("Eliminar de fijos")
                                    }
                                }
                        
                        //.matchedGeometryEffect(id: pinned.profile, in: name)
                      }
                  }
                  .padding()
              }
                LazyVStack(alignment: .leading, spacing: 0, content: {
                    
                    ForEach(messages) { msg in
                        
                        Button(action: {self.showingDetail.toggle()}, label: {
                            ZStack {
                                
                                // adding Buttons...
                                
                                HStack{
                                    
                                    Color.yellow
                                        .frame(width: 90)
                                    // hiding when left swipe...
                                        .opacity(msg.offset > 0 ? 1 : 0)
                                    
                                    Spacer()
                                    
                                    Color.red
                                        .frame(width: 90)
                                        .opacity(msg.offset < 0 ? 1 : 0)
                                }
                                
                                HStack{
                                    
                                    Button(action: {
                                        
                                        // appending View....
                                        withAnimation(.default){
                                            
                                            
                                            let index = getIndex(profile: msg.profile)
                                            
                                            var pinnedView = messages[index]
                                            
                                            // setting offset to 0
                                            
                                            pinnedView.offset = 0
                                            
                                            pinnedViews.append(pinnedView)
                                            
                                            // removing from main View...
                                            
                                            messages.removeAll { (msg1) -> Bool in
                                                
                                                if msg.profile == msg1.profile{return true}
                                                else{return false}
                                            }
                                        }
                                        
                                    }, label: {
                                        
                                        Image(systemName: "pin.fill")
                                            .font(.title)
                                            .foregroundColor(Color("back"))
                                        
                                    })
                                    .frame(width: 90)
                                    
                                    // on ended not working...
                                    
                                    Spacer()
                                    
                                    Button(action: {
                                        
                                        // removing from main View...
                                        
                                        withAnimation(.default){
                                            
                                            messages.removeAll { (msg1) -> Bool in
                                                
                                                if msg.profile == msg1.profile{return true}
                                                else{return false}
                                            }
                                        }
                                        
                                    }, label: {
                                        
                                        Image(systemName: "trash.fill")
                                            .font(.title)
                                            .foregroundColor(Color("back"))
                                    })
                                    .frame(width: 90)
                                }
                                
                                        HStack(spacing: 15) {
                                            
                                            Image(msg.profile)
                                                .resizable()
                                                .renderingMode(.original)
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: 60, height: 60)
                                                .clipShape(Circle())
                                                .matchedGeometryEffect(id: msg.profile, in: name)
                                      
                                              
                                              VStack(alignment: .leading,spacing: 10){
                                                  
                                                  HStack{
                                                      Text(msg.name)
                                                      Spacer()
                                                      Text("12/02/2020")
                                                  }
                                                  
                                                  HStack{
                                                      Text(msg.message)
                                                          .foregroundColor(.gray)
                                                          .lineLimit(2)
                                                      Spacer()
                                                      ZStack{
                                                          Circle()
                                                              .fill(Color.red)
                                                              .frame(width: 20, height: 20)
                                                          Text("1")
                                                              .foregroundColor(.white)
                                                              .lineLimit(1)
                                                              .font(.system(size: 12))
                                                      }
                                                  }
                                                  
                                                  Divider()
                                           }
                                        }
                                .padding(.all)
                                .background(Color("back"))
                                .contentShape(Rectangle())
                                // adding gesture...
                                .offset(x: msg.offset)
                                .gesture(DragGesture().onChanged({ (value) in
                                    
                                    withAnimation(.default){
                                        
                                        messages[getIndex(profile: msg.profile)].offset = value.translation.width
                                    }
                                    
                                })
                                .onEnded({ (value) in
                                    
                                    withAnimation(.default){
                                        
                                        if value.translation.width > 80{
                                            
                                            messages[getIndex(profile: msg.profile)].offset = 90
                                        }
                                        else if value.translation.width < -80{
                                            
                                            messages[getIndex(profile: msg.profile)].offset = -90
                                        }
                                        else{
                                            
                                            messages[getIndex(profile: msg.profile)].offset = 0
                                        }
                                    }
                                }))
                                
                            }
                        }).sheet(isPresented: $showingDetail) {
                            ChatView()
                        }
                    }
                })
                .padding(.vertical)
          })
      }
      
      func getIndex(profile: String)->Int{
          
          var index = 0
          
          for i in 0..<messages.count{
              
              if profile == messages[i].profile{
                  
                  index = i
              }
          }
          
          return index
      }
  }
  
  // Sample Data...
  
  struct Mensajes : Identifiable {
      
      var id : Int
      var name : String
      var message : String
      var profile : String
      var offset: CGFloat
  }
