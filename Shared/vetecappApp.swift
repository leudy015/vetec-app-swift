//
//  vetecappApp.swift
//  Shared
//
//  Created by Leudy on 29/06/2020.
//

import SwiftUI
import GoogleSignIn
import Firebase
import Apollo
import Alamofire
import SocketIO
import OneSignal

class AppDelegate: NSObject, UIApplicationDelegate, GIDSignInDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        FirebaseApp.configure()

        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        let manager = SocketManager(socketURL: URL(string: "https://server.vetec.es")!, config: [.log(true), .compress])
        let socket = manager.defaultSocket

        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
        }

        socket.connect()
        
        
         //START OneSignal initialization code
         let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
         
         // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
         OneSignal.initWithLaunchOptions(launchOptions,
           appId: "1c66942a-ead0-4e6b-a811-81a79760b5bb",
           handleNotificationAction: nil,
           settings: onesignalInitSettings)

         OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

         // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 6)
         OneSignal.promptForPushNotifications(userResponse: { accepted in
           print("User accepted notifications: \(accepted)")
         })
         //END OneSignal initializataion code
        
        return true
    }

    @available(iOS 9.0, *)
        func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
          -> Bool {    return GIDSignIn.sharedInstance().handle(url)
        }
        
        func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
            if let error = error {
                print(error)
                return
            }
            guard let authentication = user.authentication else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                              accessToken: authentication.accessToken)
            
            Auth.auth().signIn(with: credential) { (result, error) in
                if error != nil {
                    print((error?.localizedDescription)!)
                }
                
                let nombre = result?.user.displayName
                let email = result?.user.email
                let password = result?.user.refreshToken
                
                let datos = [nombre, email, password]
                
                print(datos)
                
                AF.request(URL.init(string: "https://server.vetec.es/api/v1/auth/social/mobile")!, method: .post, encoding: JSONEncoding.default).responseString { (response) in
                
                    print(response)
            
                        switch response.result {
                        case .success(_):
                            if let json = response.value {
                                
                                print(json)
                                
                                Network.shared.apollo.perform(mutation: AutenticarUsuarioMutation(email: "leudy@vetec.es", password: "Leudy")) { result in
                                 switch result {
                                 case .success(let graphQLResult):
                                    if let token = graphQLResult.data?.autenticarUsuario?.data?.token,
                                       let id = graphQLResult.data?.autenticarUsuario?.data?.id {
                                    
                                        let defaults = UserDefaults.standard
                                            defaults.set(token, forKey: "token")
                                            defaults.set(id, forKey: "id")
                                            defaults.synchronize()
                                            }

                                   if let errors = graphQLResult.errors {
                                     print("Errors from server: \(errors)")
                                   }
                                 case .failure(let error):
                                   print("Error: \(error)")
                                    }
                                }
                            }
                            break
                        case .failure(let error):
                            print(error)
                            break
                        }
                    }
                }
            }
        }

@main
struct vetecappApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    let defaults = UserDefaults.standard
    var body: some Scene {
        WindowGroup {
            if let _ = defaults.value(forKey: "token"),
               let _ = defaults.value(forKey: "id") {
                ContentView()
            } else {
                LandingView()
            }

        }
    }
}
