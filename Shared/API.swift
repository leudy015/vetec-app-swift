// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public struct UsuarioInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - nombre
  ///   - apellidos
  ///   - email
  ///   - password
  public init(nombre: String, apellidos: String, email: String, password: String) {
    graphQLMap = ["nombre": nombre, "apellidos": apellidos, "email": email, "password": password]
  }

  public var nombre: String {
    get {
      return graphQLMap["nombre"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "nombre")
    }
  }

  public var apellidos: String {
    get {
      return graphQLMap["apellidos"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "apellidos")
    }
  }

  public var email: String {
    get {
      return graphQLMap["email"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var password: String {
    get {
      return graphQLMap["password"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "password")
    }
  }
}

public struct DateRangeInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - fromDate
  ///   - toDate
  public init(fromDate: Swift.Optional<String?> = nil, toDate: Swift.Optional<String?> = nil) {
    graphQLMap = ["fromDate": fromDate, "toDate": toDate]
  }

  public var fromDate: Swift.Optional<String?> {
    get {
      return graphQLMap["fromDate"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "fromDate")
    }
  }

  public var toDate: Swift.Optional<String?> {
    get {
      return graphQLMap["toDate"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "toDate")
    }
  }
}

public struct ActualizarUsuarioInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - id
  ///   - email
  ///   - nombre
  ///   - apellidos
  ///   - ciudad
  ///   - telefono
  ///   - avatar
  ///   - myVet
  ///   - setVideoConsultas
  ///   - isPlus
  ///   - notificacion
  public init(id: GraphQLID, email: Swift.Optional<String?> = nil, nombre: Swift.Optional<String?> = nil, apellidos: Swift.Optional<String?> = nil, ciudad: Swift.Optional<String?> = nil, telefono: Swift.Optional<String?> = nil, avatar: Swift.Optional<String?> = nil, myVet: Swift.Optional<GraphQLID?> = nil, setVideoConsultas: Swift.Optional<Int?> = nil, isPlus: Swift.Optional<Bool?> = nil, notificacion: Swift.Optional<Bool?> = nil) {
    graphQLMap = ["id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "MyVet": myVet, "setVideoConsultas": setVideoConsultas, "isPlus": isPlus, "notificacion": notificacion]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var email: Swift.Optional<String?> {
    get {
      return graphQLMap["email"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var nombre: Swift.Optional<String?> {
    get {
      return graphQLMap["nombre"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "nombre")
    }
  }

  public var apellidos: Swift.Optional<String?> {
    get {
      return graphQLMap["apellidos"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "apellidos")
    }
  }

  public var ciudad: Swift.Optional<String?> {
    get {
      return graphQLMap["ciudad"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ciudad")
    }
  }

  public var telefono: Swift.Optional<String?> {
    get {
      return graphQLMap["telefono"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "telefono")
    }
  }

  public var avatar: Swift.Optional<String?> {
    get {
      return graphQLMap["avatar"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "avatar")
    }
  }

  public var myVet: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["MyVet"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "MyVet")
    }
  }

  public var setVideoConsultas: Swift.Optional<Int?> {
    get {
      return graphQLMap["setVideoConsultas"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "setVideoConsultas")
    }
  }

  public var isPlus: Swift.Optional<Bool?> {
    get {
      return graphQLMap["isPlus"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "isPlus")
    }
  }

  public var notificacion: Swift.Optional<Bool?> {
    get {
      return graphQLMap["notificacion"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notificacion")
    }
  }
}

public struct Mascotainput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - name
  ///   - age
  ///   - avatar
  ///   - usuario
  ///   - especie
  ///   - raza
  ///   - genero
  ///   - peso
  ///   - microchip
  ///   - vacunas
  ///   - alergias
  ///   - castrado
  public init(name: Swift.Optional<String?> = nil, age: Swift.Optional<String?> = nil, avatar: Swift.Optional<String?> = nil, usuario: Swift.Optional<GraphQLID?> = nil, especie: Swift.Optional<String?> = nil, raza: Swift.Optional<String?> = nil, genero: Swift.Optional<String?> = nil, peso: Swift.Optional<String?> = nil, microchip: Swift.Optional<String?> = nil, vacunas: Swift.Optional<String?> = nil, alergias: Swift.Optional<String?> = nil, castrado: Swift.Optional<Bool?> = nil) {
    graphQLMap = ["name": name, "age": age, "avatar": avatar, "usuario": usuario, "especie": especie, "raza": raza, "genero": genero, "peso": peso, "microchip": microchip, "vacunas": vacunas, "alergias": alergias, "castrado": castrado]
  }

  public var name: Swift.Optional<String?> {
    get {
      return graphQLMap["name"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var age: Swift.Optional<String?> {
    get {
      return graphQLMap["age"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "age")
    }
  }

  public var avatar: Swift.Optional<String?> {
    get {
      return graphQLMap["avatar"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "avatar")
    }
  }

  public var usuario: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["usuario"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "usuario")
    }
  }

  public var especie: Swift.Optional<String?> {
    get {
      return graphQLMap["especie"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "especie")
    }
  }

  public var raza: Swift.Optional<String?> {
    get {
      return graphQLMap["raza"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "raza")
    }
  }

  public var genero: Swift.Optional<String?> {
    get {
      return graphQLMap["genero"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "genero")
    }
  }

  public var peso: Swift.Optional<String?> {
    get {
      return graphQLMap["peso"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "peso")
    }
  }

  public var microchip: Swift.Optional<String?> {
    get {
      return graphQLMap["microchip"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "microchip")
    }
  }

  public var vacunas: Swift.Optional<String?> {
    get {
      return graphQLMap["vacunas"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "vacunas")
    }
  }

  public var alergias: Swift.Optional<String?> {
    get {
      return graphQLMap["alergias"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "alergias")
    }
  }

  public var castrado: Swift.Optional<Bool?> {
    get {
      return graphQLMap["castrado"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "castrado")
    }
  }
}

public struct ConsultaCreationInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - endDate
  ///   - time
  ///   - clave
  ///   - nota
  ///   - id
  ///   - aceptaTerminos
  ///   - cupon
  ///   - profesional
  ///   - mascota
  ///   - cantidad
  ///   - usuario
  public init(endDate: Swift.Optional<String?> = nil, time: Swift.Optional<String?> = nil, clave: Swift.Optional<String?> = nil, nota: Swift.Optional<String?> = nil, id: Swift.Optional<GraphQLID?> = nil, aceptaTerminos: Swift.Optional<Bool?> = nil, cupon: Swift.Optional<GraphQLID?> = nil, profesional: Swift.Optional<GraphQLID?> = nil, mascota: Swift.Optional<GraphQLID?> = nil, cantidad: Swift.Optional<Int?> = nil, usuario: Swift.Optional<GraphQLID?> = nil) {
    graphQLMap = ["endDate": endDate, "time": time, "clave": clave, "nota": nota, "id": id, "aceptaTerminos": aceptaTerminos, "cupon": cupon, "profesional": profesional, "mascota": mascota, "cantidad": cantidad, "usuario": usuario]
  }

  public var endDate: Swift.Optional<String?> {
    get {
      return graphQLMap["endDate"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "endDate")
    }
  }

  public var time: Swift.Optional<String?> {
    get {
      return graphQLMap["time"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "time")
    }
  }

  public var clave: Swift.Optional<String?> {
    get {
      return graphQLMap["clave"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "clave")
    }
  }

  public var nota: Swift.Optional<String?> {
    get {
      return graphQLMap["nota"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "nota")
    }
  }

  public var id: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["id"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var aceptaTerminos: Swift.Optional<Bool?> {
    get {
      return graphQLMap["aceptaTerminos"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "aceptaTerminos")
    }
  }

  public var cupon: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["cupon"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "cupon")
    }
  }

  public var profesional: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["profesional"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "profesional")
    }
  }

  public var mascota: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["mascota"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "mascota")
    }
  }

  public var cantidad: Swift.Optional<Int?> {
    get {
      return graphQLMap["cantidad"] as? Swift.Optional<Int?> ?? Swift.Optional<Int?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "cantidad")
    }
  }

  public var usuario: Swift.Optional<GraphQLID?> {
    get {
      return graphQLMap["usuario"] as? Swift.Optional<GraphQLID?> ?? Swift.Optional<GraphQLID?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "usuario")
    }
  }
}

public struct NotificationInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  /// - Parameters:
  ///   - user
  ///   - usuario
  ///   - profesional
  ///   - consulta
  ///   - read
  ///   - type
  public init(user: Swift.Optional<String?> = nil, usuario: Swift.Optional<String?> = nil, profesional: Swift.Optional<String?> = nil, consulta: Swift.Optional<String?> = nil, read: Swift.Optional<Bool?> = nil, type: Swift.Optional<String?> = nil) {
    graphQLMap = ["user": user, "usuario": usuario, "profesional": profesional, "consulta": consulta, "read": read, "type": type]
  }

  public var user: Swift.Optional<String?> {
    get {
      return graphQLMap["user"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "user")
    }
  }

  public var usuario: Swift.Optional<String?> {
    get {
      return graphQLMap["usuario"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "usuario")
    }
  }

  public var profesional: Swift.Optional<String?> {
    get {
      return graphQLMap["profesional"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "profesional")
    }
  }

  public var consulta: Swift.Optional<String?> {
    get {
      return graphQLMap["consulta"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "consulta")
    }
  }

  public var read: Swift.Optional<Bool?> {
    get {
      return graphQLMap["read"] as? Swift.Optional<Bool?> ?? Swift.Optional<Bool?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "read")
    }
  }

  public var type: Swift.Optional<String?> {
    get {
      return graphQLMap["type"] as? Swift.Optional<String?> ?? Swift.Optional<String?>.none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }
}

public final class GetUsuarioQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getUsuario($id: ID!) {
      getUsuario(id: $id) {
        __typename
        id
        email
        UserID
        nombre
        apellidos
        ciudad
        telefono
        avatar
        StripeID
        MyVet
        connected
        isNotVerified
        lastTime
        verifyPhone
        setVideoConsultas
        isPlus
        notificacion
        created_at
      }
    }
    """

  public let operationName: String = "getUsuario"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getUsuario", arguments: ["id": GraphQLVariable("id")], type: .object(GetUsuario.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getUsuario: GetUsuario? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getUsuario": getUsuario.flatMap { (value: GetUsuario) -> ResultMap in value.resultMap }])
    }

    public var getUsuario: GetUsuario? {
      get {
        return (resultMap["getUsuario"] as? ResultMap).flatMap { GetUsuario(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getUsuario")
      }
    }

    public struct GetUsuario: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Usuario"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("UserID", type: .scalar(String.self)),
          GraphQLField("nombre", type: .scalar(String.self)),
          GraphQLField("apellidos", type: .scalar(String.self)),
          GraphQLField("ciudad", type: .scalar(String.self)),
          GraphQLField("telefono", type: .scalar(String.self)),
          GraphQLField("avatar", type: .scalar(String.self)),
          GraphQLField("StripeID", type: .scalar(String.self)),
          GraphQLField("MyVet", type: .scalar(GraphQLID.self)),
          GraphQLField("connected", type: .scalar(Bool.self)),
          GraphQLField("isNotVerified", type: .scalar(Bool.self)),
          GraphQLField("lastTime", type: .scalar(String.self)),
          GraphQLField("verifyPhone", type: .scalar(Bool.self)),
          GraphQLField("setVideoConsultas", type: .scalar(Int.self)),
          GraphQLField("isPlus", type: .scalar(Bool.self)),
          GraphQLField("notificacion", type: .scalar(Bool.self)),
          GraphQLField("created_at", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, email: String? = nil, userId: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, stripeId: String? = nil, myVet: GraphQLID? = nil, connected: Bool? = nil, isNotVerified: Bool? = nil, lastTime: String? = nil, verifyPhone: Bool? = nil, setVideoConsultas: Int? = nil, isPlus: Bool? = nil, notificacion: Bool? = nil, createdAt: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "UserID": userId, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "StripeID": stripeId, "MyVet": myVet, "connected": connected, "isNotVerified": isNotVerified, "lastTime": lastTime, "verifyPhone": verifyPhone, "setVideoConsultas": setVideoConsultas, "isPlus": isPlus, "notificacion": notificacion, "created_at": createdAt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return resultMap["email"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }

      public var userId: String? {
        get {
          return resultMap["UserID"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "UserID")
        }
      }

      public var nombre: String? {
        get {
          return resultMap["nombre"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nombre")
        }
      }

      public var apellidos: String? {
        get {
          return resultMap["apellidos"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "apellidos")
        }
      }

      public var ciudad: String? {
        get {
          return resultMap["ciudad"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "ciudad")
        }
      }

      public var telefono: String? {
        get {
          return resultMap["telefono"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "telefono")
        }
      }

      public var avatar: String? {
        get {
          return resultMap["avatar"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avatar")
        }
      }

      public var stripeId: String? {
        get {
          return resultMap["StripeID"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "StripeID")
        }
      }

      public var myVet: GraphQLID? {
        get {
          return resultMap["MyVet"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "MyVet")
        }
      }

      public var connected: Bool? {
        get {
          return resultMap["connected"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "connected")
        }
      }

      public var isNotVerified: Bool? {
        get {
          return resultMap["isNotVerified"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isNotVerified")
        }
      }

      public var lastTime: String? {
        get {
          return resultMap["lastTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastTime")
        }
      }

      public var verifyPhone: Bool? {
        get {
          return resultMap["verifyPhone"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "verifyPhone")
        }
      }

      public var setVideoConsultas: Int? {
        get {
          return resultMap["setVideoConsultas"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "setVideoConsultas")
        }
      }

      public var isPlus: Bool? {
        get {
          return resultMap["isPlus"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isPlus")
        }
      }

      public var notificacion: Bool? {
        get {
          return resultMap["notificacion"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificacion")
        }
      }

      public var createdAt: String? {
        get {
          return resultMap["created_at"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "created_at")
        }
      }
    }
  }
}

public final class AutenticarUsuarioMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation autenticarUsuario($email: String!, $password: String!) {
      autenticarUsuario(email: $email, password: $password) {
        __typename
        success
        message
        data {
          __typename
          token
          id
          verifyPhone
        }
      }
    }
    """

  public let operationName: String = "autenticarUsuario"

  public var email: String
  public var password: String

  public init(email: String, password: String) {
    self.email = email
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["email": email, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("autenticarUsuario", arguments: ["email": GraphQLVariable("email"), "password": GraphQLVariable("password")], type: .object(AutenticarUsuario.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(autenticarUsuario: AutenticarUsuario? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "autenticarUsuario": autenticarUsuario.flatMap { (value: AutenticarUsuario) -> ResultMap in value.resultMap }])
    }

    public var autenticarUsuario: AutenticarUsuario? {
      get {
        return (resultMap["autenticarUsuario"] as? ResultMap).flatMap { AutenticarUsuario(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "autenticarUsuario")
      }
    }

    public struct AutenticarUsuario: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["AutenticarUsuarioResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil, data: Datum? = nil) {
        self.init(unsafeResultMap: ["__typename": "AutenticarUsuarioResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["AutenticarUsuario"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("token", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .nonNull(.scalar(String.self))),
            GraphQLField("verifyPhone", type: .nonNull(.scalar(Bool.self))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(token: String, id: String, verifyPhone: Bool) {
          self.init(unsafeResultMap: ["__typename": "AutenticarUsuario", "token": token, "id": id, "verifyPhone": verifyPhone])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var token: String {
          get {
            return resultMap["token"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "token")
          }
        }

        public var id: String {
          get {
            return resultMap["id"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var verifyPhone: Bool {
          get {
            return resultMap["verifyPhone"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "verifyPhone")
          }
        }
      }
    }
  }
}

public final class CrearUsuarioMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation crearUsuario($input: UsuarioInput) {
      crearUsuario(input: $input) {
        __typename
        success
        message
        data {
          __typename
          id
          nombre
          apellidos
          email
        }
      }
    }
    """

  public let operationName: String = "crearUsuario"

  public var input: UsuarioInput?

  public init(input: UsuarioInput? = nil) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("crearUsuario", arguments: ["input": GraphQLVariable("input")], type: .object(CrearUsuario.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(crearUsuario: CrearUsuario? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "crearUsuario": crearUsuario.flatMap { (value: CrearUsuario) -> ResultMap in value.resultMap }])
    }

    public var crearUsuario: CrearUsuario? {
      get {
        return (resultMap["crearUsuario"] as? ResultMap).flatMap { CrearUsuario(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "crearUsuario")
      }
    }

    public struct CrearUsuario: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CrearUsuarioResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil, data: Datum? = nil) {
        self.init(unsafeResultMap: ["__typename": "CrearUsuarioResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Usuario"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("nombre", type: .scalar(String.self)),
            GraphQLField("apellidos", type: .scalar(String.self)),
            GraphQLField("email", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, nombre: String? = nil, apellidos: String? = nil, email: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "nombre": nombre, "apellidos": apellidos, "email": email])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var nombre: String? {
          get {
            return resultMap["nombre"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nombre")
          }
        }

        public var apellidos: String? {
          get {
            return resultMap["apellidos"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "apellidos")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }
      }
    }
  }
}

public final class GetProfesionalallQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getProfesionalall {
      getProfesionalall {
        __typename
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        UserID
        profesion
        experiencia
        descricion
        verifyPhone
        Precio
        visitas
        anadidoFavorito
        notificacion
        isProfesional
        isVerified
        isAvailable
        connected
        lastTime
        professionalRatinglist {
          __typename
          id
          customer {
            __typename
            id
            email
            nombre
            apellidos
            ciudad
            verifyPhone
            telefono
            avatar
            UserID
          }
          rate
          updated_at
          coment
        }
        consultas {
          __typename
          id
        }
      }
    }
    """

  public let operationName: String = "getProfesionalall"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getProfesionalall", type: .list(.object(GetProfesionalall.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getProfesionalall: [GetProfesionalall?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getProfesionalall": getProfesionalall.flatMap { (value: [GetProfesionalall?]) -> [ResultMap?] in value.map { (value: GetProfesionalall?) -> ResultMap? in value.flatMap { (value: GetProfesionalall) -> ResultMap in value.resultMap } } }])
    }

    public var getProfesionalall: [GetProfesionalall?]? {
      get {
        return (resultMap["getProfesionalall"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetProfesionalall?] in value.map { (value: ResultMap?) -> GetProfesionalall? in value.flatMap { (value: ResultMap) -> GetProfesionalall in GetProfesionalall(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetProfesionalall?]) -> [ResultMap?] in value.map { (value: GetProfesionalall?) -> ResultMap? in value.flatMap { (value: GetProfesionalall) -> ResultMap in value.resultMap } } }, forKey: "getProfesionalall")
      }
    }

    public struct GetProfesionalall: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Profesional"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("nombre", type: .scalar(String.self)),
          GraphQLField("apellidos", type: .scalar(String.self)),
          GraphQLField("ciudad", type: .scalar(String.self)),
          GraphQLField("telefono", type: .scalar(String.self)),
          GraphQLField("avatar", type: .scalar(String.self)),
          GraphQLField("UserID", type: .scalar(String.self)),
          GraphQLField("profesion", type: .scalar(String.self)),
          GraphQLField("experiencia", type: .scalar(String.self)),
          GraphQLField("descricion", type: .scalar(String.self)),
          GraphQLField("verifyPhone", type: .scalar(Bool.self)),
          GraphQLField("Precio", type: .scalar(String.self)),
          GraphQLField("visitas", type: .scalar(Int.self)),
          GraphQLField("anadidoFavorito", type: .scalar(Bool.self)),
          GraphQLField("notificacion", type: .scalar(Bool.self)),
          GraphQLField("isProfesional", type: .scalar(Bool.self)),
          GraphQLField("isVerified", type: .scalar(Bool.self)),
          GraphQLField("isAvailable", type: .scalar(Bool.self)),
          GraphQLField("connected", type: .scalar(Bool.self)),
          GraphQLField("lastTime", type: .scalar(String.self)),
          GraphQLField("professionalRatinglist", type: .list(.object(ProfessionalRatinglist.selections))),
          GraphQLField("consultas", type: .list(.object(Consulta.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, profesion: String? = nil, experiencia: String? = nil, descricion: String? = nil, verifyPhone: Bool? = nil, precio: String? = nil, visitas: Int? = nil, anadidoFavorito: Bool? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil, professionalRatinglist: [ProfessionalRatinglist?]? = nil, consultas: [Consulta?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId, "profesion": profesion, "experiencia": experiencia, "descricion": descricion, "verifyPhone": verifyPhone, "Precio": precio, "visitas": visitas, "anadidoFavorito": anadidoFavorito, "notificacion": notificacion, "isProfesional": isProfesional, "isVerified": isVerified, "isAvailable": isAvailable, "connected": connected, "lastTime": lastTime, "professionalRatinglist": professionalRatinglist.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, "consultas": consultas.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return resultMap["email"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }

      public var nombre: String? {
        get {
          return resultMap["nombre"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nombre")
        }
      }

      public var apellidos: String? {
        get {
          return resultMap["apellidos"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "apellidos")
        }
      }

      public var ciudad: String? {
        get {
          return resultMap["ciudad"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "ciudad")
        }
      }

      public var telefono: String? {
        get {
          return resultMap["telefono"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "telefono")
        }
      }

      public var avatar: String? {
        get {
          return resultMap["avatar"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avatar")
        }
      }

      public var userId: String? {
        get {
          return resultMap["UserID"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "UserID")
        }
      }

      public var profesion: String? {
        get {
          return resultMap["profesion"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "profesion")
        }
      }

      public var experiencia: String? {
        get {
          return resultMap["experiencia"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "experiencia")
        }
      }

      public var descricion: String? {
        get {
          return resultMap["descricion"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "descricion")
        }
      }

      public var verifyPhone: Bool? {
        get {
          return resultMap["verifyPhone"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "verifyPhone")
        }
      }

      public var precio: String? {
        get {
          return resultMap["Precio"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "Precio")
        }
      }

      public var visitas: Int? {
        get {
          return resultMap["visitas"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "visitas")
        }
      }

      public var anadidoFavorito: Bool? {
        get {
          return resultMap["anadidoFavorito"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "anadidoFavorito")
        }
      }

      public var notificacion: Bool? {
        get {
          return resultMap["notificacion"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificacion")
        }
      }

      public var isProfesional: Bool? {
        get {
          return resultMap["isProfesional"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isProfesional")
        }
      }

      public var isVerified: Bool? {
        get {
          return resultMap["isVerified"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isVerified")
        }
      }

      public var isAvailable: Bool? {
        get {
          return resultMap["isAvailable"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isAvailable")
        }
      }

      public var connected: Bool? {
        get {
          return resultMap["connected"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "connected")
        }
      }

      public var lastTime: String? {
        get {
          return resultMap["lastTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastTime")
        }
      }

      public var professionalRatinglist: [ProfessionalRatinglist?]? {
        get {
          return (resultMap["professionalRatinglist"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProfessionalRatinglist?] in value.map { (value: ResultMap?) -> ProfessionalRatinglist? in value.flatMap { (value: ResultMap) -> ProfessionalRatinglist in ProfessionalRatinglist(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, forKey: "professionalRatinglist")
        }
      }

      public var consultas: [Consulta?]? {
        get {
          return (resultMap["consultas"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Consulta?] in value.map { (value: ResultMap?) -> Consulta? in value.flatMap { (value: ResultMap) -> Consulta in Consulta(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }, forKey: "consultas")
        }
      }

      public struct ProfessionalRatinglist: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ProfessionalRating"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("customer", type: .object(Customer.selections)),
            GraphQLField("rate", type: .scalar(Int.self)),
            GraphQLField("updated_at", type: .scalar(String.self)),
            GraphQLField("coment", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, customer: Customer? = nil, rate: Int? = nil, updatedAt: String? = nil, coment: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id, "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }, "rate": rate, "updated_at": updatedAt, "coment": coment])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var customer: Customer? {
          get {
            return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "customer")
          }
        }

        public var rate: Int? {
          get {
            return resultMap["rate"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "rate")
          }
        }

        public var updatedAt: String? {
          get {
            return resultMap["updated_at"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "updated_at")
          }
        }

        public var coment: String? {
          get {
            return resultMap["coment"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "coment")
          }
        }

        public struct Customer: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Usuario"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("verifyPhone", type: .scalar(Bool.self)),
              GraphQLField("telefono", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("UserID", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, verifyPhone: Bool? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "verifyPhone": verifyPhone, "telefono": telefono, "avatar": avatar, "UserID": userId])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var verifyPhone: Bool? {
            get {
              return resultMap["verifyPhone"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "verifyPhone")
            }
          }

          public var telefono: String? {
            get {
              return resultMap["telefono"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "telefono")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var userId: String? {
            get {
              return resultMap["UserID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "UserID")
            }
          }
        }
      }

      public struct Consulta: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Consultas"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil) {
          self.init(unsafeResultMap: ["__typename": "Consultas", "id": id])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }
      }
    }
  }
}

public final class GetProfesionaloneQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getProfesionalone($id: ID!, $updateProfVisit: Boolean) {
      getProfesionalone(id: $id, updateProfVisit: $updateProfVisit) {
        __typename
        message
        success
        data {
          __typename
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
          profesion
          experiencia
          descricion
          Precio
          visitas
          notificacion
          isProfesional
          anadidoFavorito
          isVerified
          isAvailable
          professionalRatinglist {
            __typename
            id
            customer {
              __typename
              id
              email
              nombre
              apellidos
              ciudad
              telefono
              avatar
              UserID
            }
            rate
            updated_at
            coment
          }
          consultas {
            __typename
            id
          }
        }
      }
    }
    """

  public let operationName: String = "getProfesionalone"

  public var id: GraphQLID
  public var updateProfVisit: Bool?

  public init(id: GraphQLID, updateProfVisit: Bool? = nil) {
    self.id = id
    self.updateProfVisit = updateProfVisit
  }

  public var variables: GraphQLMap? {
    return ["id": id, "updateProfVisit": updateProfVisit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getProfesionalone", arguments: ["id": GraphQLVariable("id"), "updateProfVisit": GraphQLVariable("updateProfVisit")], type: .object(GetProfesionalone.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getProfesionalone: GetProfesionalone? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getProfesionalone": getProfesionalone.flatMap { (value: GetProfesionalone) -> ResultMap in value.resultMap }])
    }

    public var getProfesionalone: GetProfesionalone? {
      get {
        return (resultMap["getProfesionalone"] as? ResultMap).flatMap { GetProfesionalone(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getProfesionalone")
      }
    }

    public struct GetProfesionalone: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponseProOne"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("success", type: .scalar(Bool.self)),
          GraphQLField("data", type: .object(Datum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(message: String? = nil, success: Bool? = nil, data: Datum? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponseProOne", "message": message, "success": success, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var success: Bool? {
        get {
          return resultMap["success"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Profesional"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("nombre", type: .scalar(String.self)),
            GraphQLField("apellidos", type: .scalar(String.self)),
            GraphQLField("ciudad", type: .scalar(String.self)),
            GraphQLField("telefono", type: .scalar(String.self)),
            GraphQLField("avatar", type: .scalar(String.self)),
            GraphQLField("UserID", type: .scalar(String.self)),
            GraphQLField("profesion", type: .scalar(String.self)),
            GraphQLField("experiencia", type: .scalar(String.self)),
            GraphQLField("descricion", type: .scalar(String.self)),
            GraphQLField("Precio", type: .scalar(String.self)),
            GraphQLField("visitas", type: .scalar(Int.self)),
            GraphQLField("notificacion", type: .scalar(Bool.self)),
            GraphQLField("isProfesional", type: .scalar(Bool.self)),
            GraphQLField("anadidoFavorito", type: .scalar(Bool.self)),
            GraphQLField("isVerified", type: .scalar(Bool.self)),
            GraphQLField("isAvailable", type: .scalar(Bool.self)),
            GraphQLField("professionalRatinglist", type: .list(.object(ProfessionalRatinglist.selections))),
            GraphQLField("consultas", type: .list(.object(Consulta.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, profesion: String? = nil, experiencia: String? = nil, descricion: String? = nil, precio: String? = nil, visitas: Int? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, anadidoFavorito: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, professionalRatinglist: [ProfessionalRatinglist?]? = nil, consultas: [Consulta?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId, "profesion": profesion, "experiencia": experiencia, "descricion": descricion, "Precio": precio, "visitas": visitas, "notificacion": notificacion, "isProfesional": isProfesional, "anadidoFavorito": anadidoFavorito, "isVerified": isVerified, "isAvailable": isAvailable, "professionalRatinglist": professionalRatinglist.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, "consultas": consultas.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var nombre: String? {
          get {
            return resultMap["nombre"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nombre")
          }
        }

        public var apellidos: String? {
          get {
            return resultMap["apellidos"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "apellidos")
          }
        }

        public var ciudad: String? {
          get {
            return resultMap["ciudad"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "ciudad")
          }
        }

        public var telefono: String? {
          get {
            return resultMap["telefono"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "telefono")
          }
        }

        public var avatar: String? {
          get {
            return resultMap["avatar"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatar")
          }
        }

        public var userId: String? {
          get {
            return resultMap["UserID"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "UserID")
          }
        }

        public var profesion: String? {
          get {
            return resultMap["profesion"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profesion")
          }
        }

        public var experiencia: String? {
          get {
            return resultMap["experiencia"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "experiencia")
          }
        }

        public var descricion: String? {
          get {
            return resultMap["descricion"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "descricion")
          }
        }

        public var precio: String? {
          get {
            return resultMap["Precio"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "Precio")
          }
        }

        public var visitas: Int? {
          get {
            return resultMap["visitas"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "visitas")
          }
        }

        public var notificacion: Bool? {
          get {
            return resultMap["notificacion"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "notificacion")
          }
        }

        public var isProfesional: Bool? {
          get {
            return resultMap["isProfesional"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isProfesional")
          }
        }

        public var anadidoFavorito: Bool? {
          get {
            return resultMap["anadidoFavorito"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "anadidoFavorito")
          }
        }

        public var isVerified: Bool? {
          get {
            return resultMap["isVerified"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isVerified")
          }
        }

        public var isAvailable: Bool? {
          get {
            return resultMap["isAvailable"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isAvailable")
          }
        }

        public var professionalRatinglist: [ProfessionalRatinglist?]? {
          get {
            return (resultMap["professionalRatinglist"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProfessionalRatinglist?] in value.map { (value: ResultMap?) -> ProfessionalRatinglist? in value.flatMap { (value: ResultMap) -> ProfessionalRatinglist in ProfessionalRatinglist(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, forKey: "professionalRatinglist")
          }
        }

        public var consultas: [Consulta?]? {
          get {
            return (resultMap["consultas"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Consulta?] in value.map { (value: ResultMap?) -> Consulta? in value.flatMap { (value: ResultMap) -> Consulta in Consulta(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }, forKey: "consultas")
          }
        }

        public struct ProfessionalRatinglist: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["ProfessionalRating"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("customer", type: .object(Customer.selections)),
              GraphQLField("rate", type: .scalar(Int.self)),
              GraphQLField("updated_at", type: .scalar(String.self)),
              GraphQLField("coment", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, customer: Customer? = nil, rate: Int? = nil, updatedAt: String? = nil, coment: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id, "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }, "rate": rate, "updated_at": updatedAt, "coment": coment])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var customer: Customer? {
            get {
              return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "customer")
            }
          }

          public var rate: Int? {
            get {
              return resultMap["rate"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "rate")
            }
          }

          public var updatedAt: String? {
            get {
              return resultMap["updated_at"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "updated_at")
            }
          }

          public var coment: String? {
            get {
              return resultMap["coment"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "coment")
            }
          }

          public struct Customer: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Usuario"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
                GraphQLField("email", type: .scalar(String.self)),
                GraphQLField("nombre", type: .scalar(String.self)),
                GraphQLField("apellidos", type: .scalar(String.self)),
                GraphQLField("ciudad", type: .scalar(String.self)),
                GraphQLField("telefono", type: .scalar(String.self)),
                GraphQLField("avatar", type: .scalar(String.self)),
                GraphQLField("UserID", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var email: String? {
              get {
                return resultMap["email"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "email")
              }
            }

            public var nombre: String? {
              get {
                return resultMap["nombre"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "nombre")
              }
            }

            public var apellidos: String? {
              get {
                return resultMap["apellidos"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "apellidos")
              }
            }

            public var ciudad: String? {
              get {
                return resultMap["ciudad"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "ciudad")
              }
            }

            public var telefono: String? {
              get {
                return resultMap["telefono"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "telefono")
              }
            }

            public var avatar: String? {
              get {
                return resultMap["avatar"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "avatar")
              }
            }

            public var userId: String? {
              get {
                return resultMap["UserID"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "UserID")
              }
            }
          }
        }

        public struct Consulta: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Consultas"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil) {
            self.init(unsafeResultMap: ["__typename": "Consultas", "id": id])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }
        }
      }
    }
  }
}

public final class GetMascotaQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMascota($usuarios: ID!) {
      getMascota(usuarios: $usuarios) {
        __typename
        message
        success
        list {
          __typename
          id
          name
          age
          usuario
          avatar
          especie
          raza
          genero
          peso
          microchip
          vacunas
          alergias
          castrado
        }
      }
    }
    """

  public let operationName: String = "getMascota"

  public var usuarios: GraphQLID

  public init(usuarios: GraphQLID) {
    self.usuarios = usuarios
  }

  public var variables: GraphQLMap? {
    return ["usuarios": usuarios]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getMascota", arguments: ["usuarios": GraphQLVariable("usuarios")], type: .object(GetMascotum.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getMascota: GetMascotum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getMascota": getMascota.flatMap { (value: GetMascotum) -> ResultMap in value.resultMap }])
    }

    public var getMascota: GetMascotum? {
      get {
        return (resultMap["getMascota"] as? ResultMap).flatMap { GetMascotum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getMascota")
      }
    }

    public struct GetMascotum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MascotaListResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("list", type: .list(.object(List.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(message: String? = nil, success: Bool, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "MascotaListResponse", "message": message, "success": success, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Mascota"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("age", type: .scalar(String.self)),
            GraphQLField("usuario", type: .scalar(GraphQLID.self)),
            GraphQLField("avatar", type: .scalar(String.self)),
            GraphQLField("especie", type: .scalar(String.self)),
            GraphQLField("raza", type: .scalar(String.self)),
            GraphQLField("genero", type: .scalar(String.self)),
            GraphQLField("peso", type: .scalar(String.self)),
            GraphQLField("microchip", type: .scalar(String.self)),
            GraphQLField("vacunas", type: .scalar(String.self)),
            GraphQLField("alergias", type: .scalar(String.self)),
            GraphQLField("castrado", type: .scalar(Bool.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, name: String? = nil, age: String? = nil, usuario: GraphQLID? = nil, avatar: String? = nil, especie: String? = nil, raza: String? = nil, genero: String? = nil, peso: String? = nil, microchip: String? = nil, vacunas: String? = nil, alergias: String? = nil, castrado: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "Mascota", "id": id, "name": name, "age": age, "usuario": usuario, "avatar": avatar, "especie": especie, "raza": raza, "genero": genero, "peso": peso, "microchip": microchip, "vacunas": vacunas, "alergias": alergias, "castrado": castrado])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var age: String? {
          get {
            return resultMap["age"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "age")
          }
        }

        public var usuario: GraphQLID? {
          get {
            return resultMap["usuario"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "usuario")
          }
        }

        public var avatar: String? {
          get {
            return resultMap["avatar"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatar")
          }
        }

        public var especie: String? {
          get {
            return resultMap["especie"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "especie")
          }
        }

        public var raza: String? {
          get {
            return resultMap["raza"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "raza")
          }
        }

        public var genero: String? {
          get {
            return resultMap["genero"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "genero")
          }
        }

        public var peso: String? {
          get {
            return resultMap["peso"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "peso")
          }
        }

        public var microchip: String? {
          get {
            return resultMap["microchip"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "microchip")
          }
        }

        public var vacunas: String? {
          get {
            return resultMap["vacunas"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "vacunas")
          }
        }

        public var alergias: String? {
          get {
            return resultMap["alergias"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "alergias")
          }
        }

        public var castrado: Bool? {
          get {
            return resultMap["castrado"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "castrado")
          }
        }
      }
    }
  }
}

public final class GetProfessionalRatingQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getProfessionalRating($id: ID!) {
      getProfessionalRating(id: $id) {
        __typename
        message
        success
        list {
          __typename
          id
          coment
          rate
          updated_at
          customer {
            __typename
            id
            ciudad
            nombre
            apellidos
            avatar
          }
        }
      }
    }
    """

  public let operationName: String = "getProfessionalRating"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getProfessionalRating", arguments: ["id": GraphQLVariable("id")], type: .object(GetProfessionalRating.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getProfessionalRating: GetProfessionalRating? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getProfessionalRating": getProfessionalRating.flatMap { (value: GetProfessionalRating) -> ResultMap in value.resultMap }])
    }

    public var getProfessionalRating: GetProfessionalRating? {
      get {
        return (resultMap["getProfessionalRating"] as? ResultMap).flatMap { GetProfessionalRating(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getProfessionalRating")
      }
    }

    public struct GetProfessionalRating: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ProfessionalListRating"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("list", type: .list(.object(List.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(message: String? = nil, success: Bool, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "ProfessionalListRating", "message": message, "success": success, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ProfessionalRating"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("coment", type: .scalar(String.self)),
            GraphQLField("rate", type: .scalar(Int.self)),
            GraphQLField("updated_at", type: .scalar(String.self)),
            GraphQLField("customer", type: .object(Customer.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, coment: String? = nil, rate: Int? = nil, updatedAt: String? = nil, customer: Customer? = nil) {
          self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id, "coment": coment, "rate": rate, "updated_at": updatedAt, "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var coment: String? {
          get {
            return resultMap["coment"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "coment")
          }
        }

        public var rate: Int? {
          get {
            return resultMap["rate"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "rate")
          }
        }

        public var updatedAt: String? {
          get {
            return resultMap["updated_at"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "updated_at")
          }
        }

        public var customer: Customer? {
          get {
            return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "customer")
          }
        }

        public struct Customer: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Usuario"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, ciudad: String? = nil, nombre: String? = nil, apellidos: String? = nil, avatar: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "ciudad": ciudad, "nombre": nombre, "apellidos": apellidos, "avatar": avatar])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }
        }
      }
    }
  }
}

public final class GetUsuarioFavoritoProQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getUsuarioFavoritoPro($id: ID!) {
      getUsuarioFavoritoPro(id: $id) {
        __typename
        success
        message
        list {
          __typename
          id
          profesionalId
          usuarioId
          profesional {
            __typename
            id
            email
            nombre
            apellidos
            ciudad
            telefono
            avatar
            UserID
            profesion
            experiencia
            descricion
            Precio
            visitas
            notificacion
            isProfesional
            isVerified
            isAvailable
            verifyPhone
            anadidoFavorito
            connected
            lastTime
            professionalRatinglist {
              __typename
              id
              customer {
                __typename
                id
                email
                nombre
                apellidos
                ciudad
                verifyPhone
                telefono
                avatar
                UserID
              }
              rate
              updated_at
              coment
            }
            consultas {
              __typename
              id
            }
          }
        }
      }
    }
    """

  public let operationName: String = "getUsuarioFavoritoPro"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getUsuarioFavoritoPro", arguments: ["id": GraphQLVariable("id")], type: .object(GetUsuarioFavoritoPro.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getUsuarioFavoritoPro: GetUsuarioFavoritoPro? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getUsuarioFavoritoPro": getUsuarioFavoritoPro.flatMap { (value: GetUsuarioFavoritoPro) -> ResultMap in value.resultMap }])
    }

    public var getUsuarioFavoritoPro: GetUsuarioFavoritoPro? {
      get {
        return (resultMap["getUsuarioFavoritoPro"] as? ResultMap).flatMap { GetUsuarioFavoritoPro(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getUsuarioFavoritoPro")
      }
    }

    public struct GetUsuarioFavoritoPro: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["UsuarioFavoritoProResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("list", type: .list(.object(List.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "UsuarioFavoritoProResponse", "success": success, "message": message, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["UsuarioFavoritoPro"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("profesionalId", type: .scalar(GraphQLID.self)),
            GraphQLField("usuarioId", type: .scalar(GraphQLID.self)),
            GraphQLField("profesional", type: .object(Profesional.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, profesionalId: GraphQLID? = nil, usuarioId: GraphQLID? = nil, profesional: Profesional? = nil) {
          self.init(unsafeResultMap: ["__typename": "UsuarioFavoritoPro", "id": id, "profesionalId": profesionalId, "usuarioId": usuarioId, "profesional": profesional.flatMap { (value: Profesional) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var profesionalId: GraphQLID? {
          get {
            return resultMap["profesionalId"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "profesionalId")
          }
        }

        public var usuarioId: GraphQLID? {
          get {
            return resultMap["usuarioId"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "usuarioId")
          }
        }

        public var profesional: Profesional? {
          get {
            return (resultMap["profesional"] as? ResultMap).flatMap { Profesional(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "profesional")
          }
        }

        public struct Profesional: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Profesional"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("telefono", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("UserID", type: .scalar(String.self)),
              GraphQLField("profesion", type: .scalar(String.self)),
              GraphQLField("experiencia", type: .scalar(String.self)),
              GraphQLField("descricion", type: .scalar(String.self)),
              GraphQLField("Precio", type: .scalar(String.self)),
              GraphQLField("visitas", type: .scalar(Int.self)),
              GraphQLField("notificacion", type: .scalar(Bool.self)),
              GraphQLField("isProfesional", type: .scalar(Bool.self)),
              GraphQLField("isVerified", type: .scalar(Bool.self)),
              GraphQLField("isAvailable", type: .scalar(Bool.self)),
              GraphQLField("verifyPhone", type: .scalar(Bool.self)),
              GraphQLField("anadidoFavorito", type: .scalar(Bool.self)),
              GraphQLField("connected", type: .scalar(Bool.self)),
              GraphQLField("lastTime", type: .scalar(String.self)),
              GraphQLField("professionalRatinglist", type: .list(.object(ProfessionalRatinglist.selections))),
              GraphQLField("consultas", type: .list(.object(Consulta.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, profesion: String? = nil, experiencia: String? = nil, descricion: String? = nil, precio: String? = nil, visitas: Int? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, verifyPhone: Bool? = nil, anadidoFavorito: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil, professionalRatinglist: [ProfessionalRatinglist?]? = nil, consultas: [Consulta?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId, "profesion": profesion, "experiencia": experiencia, "descricion": descricion, "Precio": precio, "visitas": visitas, "notificacion": notificacion, "isProfesional": isProfesional, "isVerified": isVerified, "isAvailable": isAvailable, "verifyPhone": verifyPhone, "anadidoFavorito": anadidoFavorito, "connected": connected, "lastTime": lastTime, "professionalRatinglist": professionalRatinglist.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, "consultas": consultas.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var telefono: String? {
            get {
              return resultMap["telefono"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "telefono")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var userId: String? {
            get {
              return resultMap["UserID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "UserID")
            }
          }

          public var profesion: String? {
            get {
              return resultMap["profesion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profesion")
            }
          }

          public var experiencia: String? {
            get {
              return resultMap["experiencia"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "experiencia")
            }
          }

          public var descricion: String? {
            get {
              return resultMap["descricion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "descricion")
            }
          }

          public var precio: String? {
            get {
              return resultMap["Precio"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "Precio")
            }
          }

          public var visitas: Int? {
            get {
              return resultMap["visitas"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "visitas")
            }
          }

          public var notificacion: Bool? {
            get {
              return resultMap["notificacion"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "notificacion")
            }
          }

          public var isProfesional: Bool? {
            get {
              return resultMap["isProfesional"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isProfesional")
            }
          }

          public var isVerified: Bool? {
            get {
              return resultMap["isVerified"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isVerified")
            }
          }

          public var isAvailable: Bool? {
            get {
              return resultMap["isAvailable"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isAvailable")
            }
          }

          public var verifyPhone: Bool? {
            get {
              return resultMap["verifyPhone"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "verifyPhone")
            }
          }

          public var anadidoFavorito: Bool? {
            get {
              return resultMap["anadidoFavorito"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "anadidoFavorito")
            }
          }

          public var connected: Bool? {
            get {
              return resultMap["connected"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "connected")
            }
          }

          public var lastTime: String? {
            get {
              return resultMap["lastTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastTime")
            }
          }

          public var professionalRatinglist: [ProfessionalRatinglist?]? {
            get {
              return (resultMap["professionalRatinglist"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProfessionalRatinglist?] in value.map { (value: ResultMap?) -> ProfessionalRatinglist? in value.flatMap { (value: ResultMap) -> ProfessionalRatinglist in ProfessionalRatinglist(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, forKey: "professionalRatinglist")
            }
          }

          public var consultas: [Consulta?]? {
            get {
              return (resultMap["consultas"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Consulta?] in value.map { (value: ResultMap?) -> Consulta? in value.flatMap { (value: ResultMap) -> Consulta in Consulta(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }, forKey: "consultas")
            }
          }

          public struct ProfessionalRatinglist: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["ProfessionalRating"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
                GraphQLField("customer", type: .object(Customer.selections)),
                GraphQLField("rate", type: .scalar(Int.self)),
                GraphQLField("updated_at", type: .scalar(String.self)),
                GraphQLField("coment", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil, customer: Customer? = nil, rate: Int? = nil, updatedAt: String? = nil, coment: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id, "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }, "rate": rate, "updated_at": updatedAt, "coment": coment])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var customer: Customer? {
              get {
                return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "customer")
              }
            }

            public var rate: Int? {
              get {
                return resultMap["rate"] as? Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "rate")
              }
            }

            public var updatedAt: String? {
              get {
                return resultMap["updated_at"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "updated_at")
              }
            }

            public var coment: String? {
              get {
                return resultMap["coment"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "coment")
              }
            }

            public struct Customer: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Usuario"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(GraphQLID.self)),
                  GraphQLField("email", type: .scalar(String.self)),
                  GraphQLField("nombre", type: .scalar(String.self)),
                  GraphQLField("apellidos", type: .scalar(String.self)),
                  GraphQLField("ciudad", type: .scalar(String.self)),
                  GraphQLField("verifyPhone", type: .scalar(Bool.self)),
                  GraphQLField("telefono", type: .scalar(String.self)),
                  GraphQLField("avatar", type: .scalar(String.self)),
                  GraphQLField("UserID", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, verifyPhone: Bool? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "verifyPhone": verifyPhone, "telefono": telefono, "avatar": avatar, "UserID": userId])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: GraphQLID? {
                get {
                  return resultMap["id"] as? GraphQLID
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var email: String? {
                get {
                  return resultMap["email"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "email")
                }
              }

              public var nombre: String? {
                get {
                  return resultMap["nombre"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "nombre")
                }
              }

              public var apellidos: String? {
                get {
                  return resultMap["apellidos"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "apellidos")
                }
              }

              public var ciudad: String? {
                get {
                  return resultMap["ciudad"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "ciudad")
                }
              }

              public var verifyPhone: Bool? {
                get {
                  return resultMap["verifyPhone"] as? Bool
                }
                set {
                  resultMap.updateValue(newValue, forKey: "verifyPhone")
                }
              }

              public var telefono: String? {
                get {
                  return resultMap["telefono"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "telefono")
                }
              }

              public var avatar: String? {
                get {
                  return resultMap["avatar"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "avatar")
                }
              }

              public var userId: String? {
                get {
                  return resultMap["UserID"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "UserID")
                }
              }
            }
          }

          public struct Consulta: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Consultas"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil) {
              self.init(unsafeResultMap: ["__typename": "Consultas", "id": id])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }
          }
        }
      }
    }
  }
}

public final class GetConversationclientQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getConversationclient($userId: ID!) {
      getConversationclient(userId: $userId) {
        __typename
        message
        success
        conversation {
          __typename
          _id
          user
          prof
          profesional {
            __typename
            id
            email
            nombre
            apellidos
            ciudad
            telefono
            avatar
            UserID
            profesion
            experiencia
            descricion
            Precio
            verifyPhone
            anadidoFavorito
            notificacion
            isProfesional
            isVerified
            isAvailable
            connected
            lastTime
            professionalRatinglist {
              __typename
              id
            }
          }
          usuario {
            __typename
            id
            email
            UserID
            nombre
            apellidos
            ciudad
            telefono
            avatar
            verifyPhone
            isPlus
            notificacion
            StripeID
            connected
            lastTime
          }
          messagechat {
            __typename
            _id
            text
            image
            read
            createdAt
            user {
              __typename
              _id
              name
              avatar
            }
          }
        }
      }
    }
    """

  public let operationName: String = "getConversationclient"

  public var userId: GraphQLID

  public init(userId: GraphQLID) {
    self.userId = userId
  }

  public var variables: GraphQLMap? {
    return ["userId": userId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getConversationclient", arguments: ["userId": GraphQLVariable("userId")], type: .object(GetConversationclient.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getConversationclient: GetConversationclient? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getConversationclient": getConversationclient.flatMap { (value: GetConversationclient) -> ResultMap in value.resultMap }])
    }

    public var getConversationclient: GetConversationclient? {
      get {
        return (resultMap["getConversationclient"] as? ResultMap).flatMap { GetConversationclient(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getConversationclient")
      }
    }

    public struct GetConversationclient: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Conversationgeneralresponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("message", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("conversation", type: .list(.object(Conversation.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(message: String, success: Bool, conversation: [Conversation?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Conversationgeneralresponse", "message": message, "success": success, "conversation": conversation.flatMap { (value: [Conversation?]) -> [ResultMap?] in value.map { (value: Conversation?) -> ResultMap? in value.flatMap { (value: Conversation) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var message: String {
        get {
          return resultMap["message"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var conversation: [Conversation?]? {
        get {
          return (resultMap["conversation"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Conversation?] in value.map { (value: ResultMap?) -> Conversation? in value.flatMap { (value: ResultMap) -> Conversation in Conversation(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Conversation?]) -> [ResultMap?] in value.map { (value: Conversation?) -> ResultMap? in value.flatMap { (value: Conversation) -> ResultMap in value.resultMap } } }, forKey: "conversation")
        }
      }

      public struct Conversation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["ConversationByClient"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("user", type: .scalar(GraphQLID.self)),
            GraphQLField("prof", type: .scalar(GraphQLID.self)),
            GraphQLField("profesional", type: .object(Profesional.selections)),
            GraphQLField("usuario", type: .object(Usuario.selections)),
            GraphQLField("messagechat", type: .list(.object(Messagechat.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(_id: String? = nil, user: GraphQLID? = nil, prof: GraphQLID? = nil, profesional: Profesional? = nil, usuario: Usuario? = nil, messagechat: [Messagechat?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "ConversationByClient", "_id": _id, "user": user, "prof": prof, "profesional": profesional.flatMap { (value: Profesional) -> ResultMap in value.resultMap }, "usuario": usuario.flatMap { (value: Usuario) -> ResultMap in value.resultMap }, "messagechat": messagechat.flatMap { (value: [Messagechat?]) -> [ResultMap?] in value.map { (value: Messagechat?) -> ResultMap? in value.flatMap { (value: Messagechat) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var _id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var user: GraphQLID? {
          get {
            return resultMap["user"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "user")
          }
        }

        public var prof: GraphQLID? {
          get {
            return resultMap["prof"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "prof")
          }
        }

        public var profesional: Profesional? {
          get {
            return (resultMap["profesional"] as? ResultMap).flatMap { Profesional(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "profesional")
          }
        }

        public var usuario: Usuario? {
          get {
            return (resultMap["usuario"] as? ResultMap).flatMap { Usuario(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "usuario")
          }
        }

        public var messagechat: [Messagechat?]? {
          get {
            return (resultMap["messagechat"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Messagechat?] in value.map { (value: ResultMap?) -> Messagechat? in value.flatMap { (value: ResultMap) -> Messagechat in Messagechat(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Messagechat?]) -> [ResultMap?] in value.map { (value: Messagechat?) -> ResultMap? in value.flatMap { (value: Messagechat) -> ResultMap in value.resultMap } } }, forKey: "messagechat")
          }
        }

        public struct Profesional: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Profesional"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("telefono", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("UserID", type: .scalar(String.self)),
              GraphQLField("profesion", type: .scalar(String.self)),
              GraphQLField("experiencia", type: .scalar(String.self)),
              GraphQLField("descricion", type: .scalar(String.self)),
              GraphQLField("Precio", type: .scalar(String.self)),
              GraphQLField("verifyPhone", type: .scalar(Bool.self)),
              GraphQLField("anadidoFavorito", type: .scalar(Bool.self)),
              GraphQLField("notificacion", type: .scalar(Bool.self)),
              GraphQLField("isProfesional", type: .scalar(Bool.self)),
              GraphQLField("isVerified", type: .scalar(Bool.self)),
              GraphQLField("isAvailable", type: .scalar(Bool.self)),
              GraphQLField("connected", type: .scalar(Bool.self)),
              GraphQLField("lastTime", type: .scalar(String.self)),
              GraphQLField("professionalRatinglist", type: .list(.object(ProfessionalRatinglist.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, profesion: String? = nil, experiencia: String? = nil, descricion: String? = nil, precio: String? = nil, verifyPhone: Bool? = nil, anadidoFavorito: Bool? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil, professionalRatinglist: [ProfessionalRatinglist?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId, "profesion": profesion, "experiencia": experiencia, "descricion": descricion, "Precio": precio, "verifyPhone": verifyPhone, "anadidoFavorito": anadidoFavorito, "notificacion": notificacion, "isProfesional": isProfesional, "isVerified": isVerified, "isAvailable": isAvailable, "connected": connected, "lastTime": lastTime, "professionalRatinglist": professionalRatinglist.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var telefono: String? {
            get {
              return resultMap["telefono"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "telefono")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var userId: String? {
            get {
              return resultMap["UserID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "UserID")
            }
          }

          public var profesion: String? {
            get {
              return resultMap["profesion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profesion")
            }
          }

          public var experiencia: String? {
            get {
              return resultMap["experiencia"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "experiencia")
            }
          }

          public var descricion: String? {
            get {
              return resultMap["descricion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "descricion")
            }
          }

          public var precio: String? {
            get {
              return resultMap["Precio"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "Precio")
            }
          }

          public var verifyPhone: Bool? {
            get {
              return resultMap["verifyPhone"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "verifyPhone")
            }
          }

          public var anadidoFavorito: Bool? {
            get {
              return resultMap["anadidoFavorito"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "anadidoFavorito")
            }
          }

          public var notificacion: Bool? {
            get {
              return resultMap["notificacion"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "notificacion")
            }
          }

          public var isProfesional: Bool? {
            get {
              return resultMap["isProfesional"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isProfesional")
            }
          }

          public var isVerified: Bool? {
            get {
              return resultMap["isVerified"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isVerified")
            }
          }

          public var isAvailable: Bool? {
            get {
              return resultMap["isAvailable"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isAvailable")
            }
          }

          public var connected: Bool? {
            get {
              return resultMap["connected"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "connected")
            }
          }

          public var lastTime: String? {
            get {
              return resultMap["lastTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastTime")
            }
          }

          public var professionalRatinglist: [ProfessionalRatinglist?]? {
            get {
              return (resultMap["professionalRatinglist"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProfessionalRatinglist?] in value.map { (value: ResultMap?) -> ProfessionalRatinglist? in value.flatMap { (value: ResultMap) -> ProfessionalRatinglist in ProfessionalRatinglist(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, forKey: "professionalRatinglist")
            }
          }

          public struct ProfessionalRatinglist: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["ProfessionalRating"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil) {
              self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }
          }
        }

        public struct Usuario: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Usuario"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("UserID", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("telefono", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("verifyPhone", type: .scalar(Bool.self)),
              GraphQLField("isPlus", type: .scalar(Bool.self)),
              GraphQLField("notificacion", type: .scalar(Bool.self)),
              GraphQLField("StripeID", type: .scalar(String.self)),
              GraphQLField("connected", type: .scalar(Bool.self)),
              GraphQLField("lastTime", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, email: String? = nil, userId: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, verifyPhone: Bool? = nil, isPlus: Bool? = nil, notificacion: Bool? = nil, stripeId: String? = nil, connected: Bool? = nil, lastTime: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "UserID": userId, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "verifyPhone": verifyPhone, "isPlus": isPlus, "notificacion": notificacion, "StripeID": stripeId, "connected": connected, "lastTime": lastTime])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var userId: String? {
            get {
              return resultMap["UserID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "UserID")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var telefono: String? {
            get {
              return resultMap["telefono"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "telefono")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var verifyPhone: Bool? {
            get {
              return resultMap["verifyPhone"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "verifyPhone")
            }
          }

          public var isPlus: Bool? {
            get {
              return resultMap["isPlus"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isPlus")
            }
          }

          public var notificacion: Bool? {
            get {
              return resultMap["notificacion"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "notificacion")
            }
          }

          public var stripeId: String? {
            get {
              return resultMap["StripeID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "StripeID")
            }
          }

          public var connected: Bool? {
            get {
              return resultMap["connected"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "connected")
            }
          }

          public var lastTime: String? {
            get {
              return resultMap["lastTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastTime")
            }
          }
        }

        public struct Messagechat: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["ConversationList"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("_id", type: .scalar(String.self)),
              GraphQLField("text", type: .scalar(String.self)),
              GraphQLField("image", type: .scalar(String.self)),
              GraphQLField("read", type: .scalar(Bool.self)),
              GraphQLField("createdAt", type: .scalar(String.self)),
              GraphQLField("user", type: .object(User.selections)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(_id: String? = nil, text: String? = nil, image: String? = nil, read: Bool? = nil, createdAt: String? = nil, user: User? = nil) {
            self.init(unsafeResultMap: ["__typename": "ConversationList", "_id": _id, "text": text, "image": image, "read": read, "createdAt": createdAt, "user": user.flatMap { (value: User) -> ResultMap in value.resultMap }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var _id: String? {
            get {
              return resultMap["_id"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "_id")
            }
          }

          public var text: String? {
            get {
              return resultMap["text"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "text")
            }
          }

          public var image: String? {
            get {
              return resultMap["image"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "image")
            }
          }

          public var read: Bool? {
            get {
              return resultMap["read"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "read")
            }
          }

          public var createdAt: String? {
            get {
              return resultMap["createdAt"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "createdAt")
            }
          }

          public var user: User? {
            get {
              return (resultMap["user"] as? ResultMap).flatMap { User(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "user")
            }
          }

          public struct User: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Userchat"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("_id", type: .scalar(String.self)),
                GraphQLField("name", type: .scalar(String.self)),
                GraphQLField("avatar", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(_id: String? = nil, name: String? = nil, avatar: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Userchat", "_id": _id, "name": name, "avatar": avatar])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var _id: String? {
              get {
                return resultMap["_id"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "_id")
              }
            }

            public var name: String? {
              get {
                return resultMap["name"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "name")
              }
            }

            public var avatar: String? {
              get {
                return resultMap["avatar"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "avatar")
              }
            }
          }
        }
      }
    }
  }
}

public final class GetMessageNoReadClientQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getMessageNoReadClient($conversationID: ID!, $profId: ID!) {
      getMessageNoReadClient(conversationID: $conversationID, profId: $profId) {
        __typename
        message
        success
        conversation {
          __typename
          _id
          count
        }
      }
    }
    """

  public let operationName: String = "getMessageNoReadClient"

  public var conversationID: GraphQLID
  public var profId: GraphQLID

  public init(conversationID: GraphQLID, profId: GraphQLID) {
    self.conversationID = conversationID
    self.profId = profId
  }

  public var variables: GraphQLMap? {
    return ["conversationID": conversationID, "profId": profId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getMessageNoReadClient", arguments: ["conversationID": GraphQLVariable("conversationID"), "profId": GraphQLVariable("profId")], type: .object(GetMessageNoReadClient.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getMessageNoReadClient: GetMessageNoReadClient? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getMessageNoReadClient": getMessageNoReadClient.flatMap { (value: GetMessageNoReadClient) -> ResultMap in value.resultMap }])
    }

    public var getMessageNoReadClient: GetMessageNoReadClient? {
      get {
        return (resultMap["getMessageNoReadClient"] as? ResultMap).flatMap { GetMessageNoReadClient(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getMessageNoReadClient")
      }
    }

    public struct GetMessageNoReadClient: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["messagenoReadResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("conversation", type: .list(.object(Conversation.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(message: String? = nil, success: Bool, conversation: [Conversation?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "messagenoReadResponse", "message": message, "success": success, "conversation": conversation.flatMap { (value: [Conversation?]) -> [ResultMap?] in value.map { (value: Conversation?) -> ResultMap? in value.flatMap { (value: Conversation) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var conversation: [Conversation?]? {
        get {
          return (resultMap["conversation"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Conversation?] in value.map { (value: ResultMap?) -> Conversation? in value.flatMap { (value: ResultMap) -> Conversation in Conversation(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Conversation?]) -> [ResultMap?] in value.map { (value: Conversation?) -> ResultMap? in value.flatMap { (value: Conversation) -> ResultMap in value.resultMap } } }, forKey: "conversation")
        }
      }

      public struct Conversation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["MessageNoRead"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(String.self)),
            GraphQLField("count", type: .scalar(Int.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(_id: String? = nil, count: Int? = nil) {
          self.init(unsafeResultMap: ["__typename": "MessageNoRead", "_id": _id, "count": count])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var _id: String? {
          get {
            return resultMap["_id"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var count: Int? {
          get {
            return resultMap["count"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "count")
          }
        }
      }
    }
  }
}

public final class GetConsultaByUsuarioQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getConsultaByUsuario($usuarios: ID!, $dateRange: DateRangeInput) {
      getConsultaByUsuario(usuarios: $usuarios, dateRange: $dateRange) {
        __typename
        success
        message
        list {
          __typename
          id
          time
          estado
          nota
          endDate
          created_at
          mascota {
            __typename
            id
            name
            age
            usuario
            avatar
            especie
            raza
            genero
            peso
            microchip
            vacunas
            alergias
            castrado
          }
          usuario {
            __typename
            id
            email
            UserID
            nombre
            verifyPhone
            apellidos
            ciudad
            telefono
            avatar
            notificacion
            StripeID
            connected
            lastTime
          }
          profesional {
            __typename
            id
            email
            nombre
            apellidos
            UserID
            ciudad
            telefono
            avatar
            profesion
            verifyPhone
            experiencia
            descricion
            Precio
            anadidoFavorito
            notificacion
            isProfesional
            isVerified
            isAvailable
            connected
            lastTime
            professionalRatinglist {
              __typename
              id
              customer {
                __typename
                id
                email
                nombre
                apellidos
                ciudad
                telefono
                avatar
                UserID
              }
              rate
              updated_at
              coment
            }
          }
        }
      }
    }
    """

  public let operationName: String = "getConsultaByUsuario"

  public var usuarios: GraphQLID
  public var dateRange: DateRangeInput?

  public init(usuarios: GraphQLID, dateRange: DateRangeInput? = nil) {
    self.usuarios = usuarios
    self.dateRange = dateRange
  }

  public var variables: GraphQLMap? {
    return ["usuarios": usuarios, "dateRange": dateRange]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getConsultaByUsuario", arguments: ["usuarios": GraphQLVariable("usuarios"), "dateRange": GraphQLVariable("dateRange")], type: .object(GetConsultaByUsuario.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getConsultaByUsuario: GetConsultaByUsuario? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getConsultaByUsuario": getConsultaByUsuario.flatMap { (value: GetConsultaByUsuario) -> ResultMap in value.resultMap }])
    }

    public var getConsultaByUsuario: GetConsultaByUsuario? {
      get {
        return (resultMap["getConsultaByUsuario"] as? ResultMap).flatMap { GetConsultaByUsuario(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getConsultaByUsuario")
      }
    }

    public struct GetConsultaByUsuario: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["ConsultaListResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("list", type: .list(.object(List.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "ConsultaListResponse", "success": success, "message": message, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Consultas"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("time", type: .scalar(String.self)),
            GraphQLField("estado", type: .scalar(String.self)),
            GraphQLField("nota", type: .scalar(String.self)),
            GraphQLField("endDate", type: .scalar(String.self)),
            GraphQLField("created_at", type: .scalar(String.self)),
            GraphQLField("mascota", type: .object(Mascotum.selections)),
            GraphQLField("usuario", type: .object(Usuario.selections)),
            GraphQLField("profesional", type: .object(Profesional.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, time: String? = nil, estado: String? = nil, nota: String? = nil, endDate: String? = nil, createdAt: String? = nil, mascota: Mascotum? = nil, usuario: Usuario? = nil, profesional: Profesional? = nil) {
          self.init(unsafeResultMap: ["__typename": "Consultas", "id": id, "time": time, "estado": estado, "nota": nota, "endDate": endDate, "created_at": createdAt, "mascota": mascota.flatMap { (value: Mascotum) -> ResultMap in value.resultMap }, "usuario": usuario.flatMap { (value: Usuario) -> ResultMap in value.resultMap }, "profesional": profesional.flatMap { (value: Profesional) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var time: String? {
          get {
            return resultMap["time"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "time")
          }
        }

        public var estado: String? {
          get {
            return resultMap["estado"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "estado")
          }
        }

        public var nota: String? {
          get {
            return resultMap["nota"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nota")
          }
        }

        public var endDate: String? {
          get {
            return resultMap["endDate"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "endDate")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["created_at"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }

        public var mascota: Mascotum? {
          get {
            return (resultMap["mascota"] as? ResultMap).flatMap { Mascotum(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "mascota")
          }
        }

        public var usuario: Usuario? {
          get {
            return (resultMap["usuario"] as? ResultMap).flatMap { Usuario(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "usuario")
          }
        }

        public var profesional: Profesional? {
          get {
            return (resultMap["profesional"] as? ResultMap).flatMap { Profesional(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "profesional")
          }
        }

        public struct Mascotum: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Mascota"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("name", type: .scalar(String.self)),
              GraphQLField("age", type: .scalar(String.self)),
              GraphQLField("usuario", type: .scalar(GraphQLID.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("especie", type: .scalar(String.self)),
              GraphQLField("raza", type: .scalar(String.self)),
              GraphQLField("genero", type: .scalar(String.self)),
              GraphQLField("peso", type: .scalar(String.self)),
              GraphQLField("microchip", type: .scalar(String.self)),
              GraphQLField("vacunas", type: .scalar(String.self)),
              GraphQLField("alergias", type: .scalar(String.self)),
              GraphQLField("castrado", type: .scalar(Bool.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, name: String? = nil, age: String? = nil, usuario: GraphQLID? = nil, avatar: String? = nil, especie: String? = nil, raza: String? = nil, genero: String? = nil, peso: String? = nil, microchip: String? = nil, vacunas: String? = nil, alergias: String? = nil, castrado: Bool? = nil) {
            self.init(unsafeResultMap: ["__typename": "Mascota", "id": id, "name": name, "age": age, "usuario": usuario, "avatar": avatar, "especie": especie, "raza": raza, "genero": genero, "peso": peso, "microchip": microchip, "vacunas": vacunas, "alergias": alergias, "castrado": castrado])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var name: String? {
            get {
              return resultMap["name"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }

          public var age: String? {
            get {
              return resultMap["age"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "age")
            }
          }

          public var usuario: GraphQLID? {
            get {
              return resultMap["usuario"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "usuario")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var especie: String? {
            get {
              return resultMap["especie"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "especie")
            }
          }

          public var raza: String? {
            get {
              return resultMap["raza"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "raza")
            }
          }

          public var genero: String? {
            get {
              return resultMap["genero"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "genero")
            }
          }

          public var peso: String? {
            get {
              return resultMap["peso"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "peso")
            }
          }

          public var microchip: String? {
            get {
              return resultMap["microchip"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "microchip")
            }
          }

          public var vacunas: String? {
            get {
              return resultMap["vacunas"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "vacunas")
            }
          }

          public var alergias: String? {
            get {
              return resultMap["alergias"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "alergias")
            }
          }

          public var castrado: Bool? {
            get {
              return resultMap["castrado"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "castrado")
            }
          }
        }

        public struct Usuario: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Usuario"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("UserID", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("verifyPhone", type: .scalar(Bool.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("telefono", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("notificacion", type: .scalar(Bool.self)),
              GraphQLField("StripeID", type: .scalar(String.self)),
              GraphQLField("connected", type: .scalar(Bool.self)),
              GraphQLField("lastTime", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, email: String? = nil, userId: String? = nil, nombre: String? = nil, verifyPhone: Bool? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, notificacion: Bool? = nil, stripeId: String? = nil, connected: Bool? = nil, lastTime: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "UserID": userId, "nombre": nombre, "verifyPhone": verifyPhone, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "notificacion": notificacion, "StripeID": stripeId, "connected": connected, "lastTime": lastTime])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var userId: String? {
            get {
              return resultMap["UserID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "UserID")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var verifyPhone: Bool? {
            get {
              return resultMap["verifyPhone"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "verifyPhone")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var telefono: String? {
            get {
              return resultMap["telefono"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "telefono")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var notificacion: Bool? {
            get {
              return resultMap["notificacion"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "notificacion")
            }
          }

          public var stripeId: String? {
            get {
              return resultMap["StripeID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "StripeID")
            }
          }

          public var connected: Bool? {
            get {
              return resultMap["connected"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "connected")
            }
          }

          public var lastTime: String? {
            get {
              return resultMap["lastTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastTime")
            }
          }
        }

        public struct Profesional: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Profesional"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("UserID", type: .scalar(String.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("telefono", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("profesion", type: .scalar(String.self)),
              GraphQLField("verifyPhone", type: .scalar(Bool.self)),
              GraphQLField("experiencia", type: .scalar(String.self)),
              GraphQLField("descricion", type: .scalar(String.self)),
              GraphQLField("Precio", type: .scalar(String.self)),
              GraphQLField("anadidoFavorito", type: .scalar(Bool.self)),
              GraphQLField("notificacion", type: .scalar(Bool.self)),
              GraphQLField("isProfesional", type: .scalar(Bool.self)),
              GraphQLField("isVerified", type: .scalar(Bool.self)),
              GraphQLField("isAvailable", type: .scalar(Bool.self)),
              GraphQLField("connected", type: .scalar(Bool.self)),
              GraphQLField("lastTime", type: .scalar(String.self)),
              GraphQLField("professionalRatinglist", type: .list(.object(ProfessionalRatinglist.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, userId: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, profesion: String? = nil, verifyPhone: Bool? = nil, experiencia: String? = nil, descricion: String? = nil, precio: String? = nil, anadidoFavorito: Bool? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil, professionalRatinglist: [ProfessionalRatinglist?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "UserID": userId, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "profesion": profesion, "verifyPhone": verifyPhone, "experiencia": experiencia, "descricion": descricion, "Precio": precio, "anadidoFavorito": anadidoFavorito, "notificacion": notificacion, "isProfesional": isProfesional, "isVerified": isVerified, "isAvailable": isAvailable, "connected": connected, "lastTime": lastTime, "professionalRatinglist": professionalRatinglist.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var userId: String? {
            get {
              return resultMap["UserID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "UserID")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var telefono: String? {
            get {
              return resultMap["telefono"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "telefono")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var profesion: String? {
            get {
              return resultMap["profesion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profesion")
            }
          }

          public var verifyPhone: Bool? {
            get {
              return resultMap["verifyPhone"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "verifyPhone")
            }
          }

          public var experiencia: String? {
            get {
              return resultMap["experiencia"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "experiencia")
            }
          }

          public var descricion: String? {
            get {
              return resultMap["descricion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "descricion")
            }
          }

          public var precio: String? {
            get {
              return resultMap["Precio"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "Precio")
            }
          }

          public var anadidoFavorito: Bool? {
            get {
              return resultMap["anadidoFavorito"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "anadidoFavorito")
            }
          }

          public var notificacion: Bool? {
            get {
              return resultMap["notificacion"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "notificacion")
            }
          }

          public var isProfesional: Bool? {
            get {
              return resultMap["isProfesional"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isProfesional")
            }
          }

          public var isVerified: Bool? {
            get {
              return resultMap["isVerified"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isVerified")
            }
          }

          public var isAvailable: Bool? {
            get {
              return resultMap["isAvailable"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isAvailable")
            }
          }

          public var connected: Bool? {
            get {
              return resultMap["connected"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "connected")
            }
          }

          public var lastTime: String? {
            get {
              return resultMap["lastTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastTime")
            }
          }

          public var professionalRatinglist: [ProfessionalRatinglist?]? {
            get {
              return (resultMap["professionalRatinglist"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProfessionalRatinglist?] in value.map { (value: ResultMap?) -> ProfessionalRatinglist? in value.flatMap { (value: ResultMap) -> ProfessionalRatinglist in ProfessionalRatinglist(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, forKey: "professionalRatinglist")
            }
          }

          public struct ProfessionalRatinglist: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["ProfessionalRating"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
                GraphQLField("customer", type: .object(Customer.selections)),
                GraphQLField("rate", type: .scalar(Int.self)),
                GraphQLField("updated_at", type: .scalar(String.self)),
                GraphQLField("coment", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil, customer: Customer? = nil, rate: Int? = nil, updatedAt: String? = nil, coment: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id, "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }, "rate": rate, "updated_at": updatedAt, "coment": coment])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var customer: Customer? {
              get {
                return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "customer")
              }
            }

            public var rate: Int? {
              get {
                return resultMap["rate"] as? Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "rate")
              }
            }

            public var updatedAt: String? {
              get {
                return resultMap["updated_at"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "updated_at")
              }
            }

            public var coment: String? {
              get {
                return resultMap["coment"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "coment")
              }
            }

            public struct Customer: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Usuario"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(GraphQLID.self)),
                  GraphQLField("email", type: .scalar(String.self)),
                  GraphQLField("nombre", type: .scalar(String.self)),
                  GraphQLField("apellidos", type: .scalar(String.self)),
                  GraphQLField("ciudad", type: .scalar(String.self)),
                  GraphQLField("telefono", type: .scalar(String.self)),
                  GraphQLField("avatar", type: .scalar(String.self)),
                  GraphQLField("UserID", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: GraphQLID? {
                get {
                  return resultMap["id"] as? GraphQLID
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var email: String? {
                get {
                  return resultMap["email"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "email")
                }
              }

              public var nombre: String? {
                get {
                  return resultMap["nombre"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "nombre")
                }
              }

              public var apellidos: String? {
                get {
                  return resultMap["apellidos"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "apellidos")
                }
              }

              public var ciudad: String? {
                get {
                  return resultMap["ciudad"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "ciudad")
                }
              }

              public var telefono: String? {
                get {
                  return resultMap["telefono"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "telefono")
                }
              }

              public var avatar: String? {
                get {
                  return resultMap["avatar"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "avatar")
                }
              }

              public var userId: String? {
                get {
                  return resultMap["UserID"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "UserID")
                }
              }
            }
          }
        }
      }
    }
  }
}

public final class GetDiagnosticoQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getDiagnostico($id: ID!) {
      getDiagnostico(id: $id) {
        __typename
        message
        success
        list {
          __typename
          _id
          diagnostico
          mascota
          created_at
          profesional {
            __typename
            nombre
            apellidos
            profesion
            avatar
          }
        }
      }
    }
    """

  public let operationName: String = "getDiagnostico"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getDiagnostico", arguments: ["id": GraphQLVariable("id")], type: .object(GetDiagnostico.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getDiagnostico: GetDiagnostico? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getDiagnostico": getDiagnostico.flatMap { (value: GetDiagnostico) -> ResultMap in value.resultMap }])
    }

    public var getDiagnostico: GetDiagnostico? {
      get {
        return (resultMap["getDiagnostico"] as? ResultMap).flatMap { GetDiagnostico(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getDiagnostico")
      }
    }

    public struct GetDiagnostico: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["DiagnosticoResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("list", type: .list(.object(List.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(message: String? = nil, success: Bool, list: [List?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "DiagnosticoResponse", "message": message, "success": success, "list": list.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var list: [List?]? {
        get {
          return (resultMap["list"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [List?] in value.map { (value: ResultMap?) -> List? in value.flatMap { (value: ResultMap) -> List in List(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [List?]) -> [ResultMap?] in value.map { (value: List?) -> ResultMap? in value.flatMap { (value: List) -> ResultMap in value.resultMap } } }, forKey: "list")
        }
      }

      public struct List: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Diagnostico"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("_id", type: .scalar(GraphQLID.self)),
            GraphQLField("diagnostico", type: .scalar(String.self)),
            GraphQLField("mascota", type: .scalar(String.self)),
            GraphQLField("created_at", type: .scalar(String.self)),
            GraphQLField("profesional", type: .object(Profesional.selections)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(_id: GraphQLID? = nil, diagnostico: String? = nil, mascota: String? = nil, createdAt: String? = nil, profesional: Profesional? = nil) {
          self.init(unsafeResultMap: ["__typename": "Diagnostico", "_id": _id, "diagnostico": diagnostico, "mascota": mascota, "created_at": createdAt, "profesional": profesional.flatMap { (value: Profesional) -> ResultMap in value.resultMap }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var _id: GraphQLID? {
          get {
            return resultMap["_id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "_id")
          }
        }

        public var diagnostico: String? {
          get {
            return resultMap["diagnostico"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "diagnostico")
          }
        }

        public var mascota: String? {
          get {
            return resultMap["mascota"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "mascota")
          }
        }

        public var createdAt: String? {
          get {
            return resultMap["created_at"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "created_at")
          }
        }

        public var profesional: Profesional? {
          get {
            return (resultMap["profesional"] as? ResultMap).flatMap { Profesional(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "profesional")
          }
        }

        public struct Profesional: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Profesionaldiag"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("profesion", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(nombre: String? = nil, apellidos: String? = nil, profesion: String? = nil, avatar: String? = nil) {
            self.init(unsafeResultMap: ["__typename": "Profesionaldiag", "nombre": nombre, "apellidos": apellidos, "profesion": profesion, "avatar": avatar])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var profesion: String? {
            get {
              return resultMap["profesion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profesion")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }
        }
      }
    }
  }
}

public final class ActualizarUsuarioMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation actualizarUsuario($input: ActualizarUsuarioInput) {
      actualizarUsuario(input: $input) {
        __typename
        id
        email
        UserID
        nombre
        apellidos
        ciudad
        telefono
        avatar
        notificacion
        MyVet
        isPlus
        connected
        lastTime
        setVideoConsultas
      }
    }
    """

  public let operationName: String = "actualizarUsuario"

  public var input: ActualizarUsuarioInput?

  public init(input: ActualizarUsuarioInput? = nil) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("actualizarUsuario", arguments: ["input": GraphQLVariable("input")], type: .object(ActualizarUsuario.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(actualizarUsuario: ActualizarUsuario? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "actualizarUsuario": actualizarUsuario.flatMap { (value: ActualizarUsuario) -> ResultMap in value.resultMap }])
    }

    public var actualizarUsuario: ActualizarUsuario? {
      get {
        return (resultMap["actualizarUsuario"] as? ResultMap).flatMap { ActualizarUsuario(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "actualizarUsuario")
      }
    }

    public struct ActualizarUsuario: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Usuario"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("email", type: .scalar(String.self)),
          GraphQLField("UserID", type: .scalar(String.self)),
          GraphQLField("nombre", type: .scalar(String.self)),
          GraphQLField("apellidos", type: .scalar(String.self)),
          GraphQLField("ciudad", type: .scalar(String.self)),
          GraphQLField("telefono", type: .scalar(String.self)),
          GraphQLField("avatar", type: .scalar(String.self)),
          GraphQLField("notificacion", type: .scalar(Bool.self)),
          GraphQLField("MyVet", type: .scalar(GraphQLID.self)),
          GraphQLField("isPlus", type: .scalar(Bool.self)),
          GraphQLField("connected", type: .scalar(Bool.self)),
          GraphQLField("lastTime", type: .scalar(String.self)),
          GraphQLField("setVideoConsultas", type: .scalar(Int.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, email: String? = nil, userId: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, notificacion: Bool? = nil, myVet: GraphQLID? = nil, isPlus: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil, setVideoConsultas: Int? = nil) {
        self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "UserID": userId, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "notificacion": notificacion, "MyVet": myVet, "isPlus": isPlus, "connected": connected, "lastTime": lastTime, "setVideoConsultas": setVideoConsultas])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var email: String? {
        get {
          return resultMap["email"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "email")
        }
      }

      public var userId: String? {
        get {
          return resultMap["UserID"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "UserID")
        }
      }

      public var nombre: String? {
        get {
          return resultMap["nombre"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nombre")
        }
      }

      public var apellidos: String? {
        get {
          return resultMap["apellidos"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "apellidos")
        }
      }

      public var ciudad: String? {
        get {
          return resultMap["ciudad"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "ciudad")
        }
      }

      public var telefono: String? {
        get {
          return resultMap["telefono"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "telefono")
        }
      }

      public var avatar: String? {
        get {
          return resultMap["avatar"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "avatar")
        }
      }

      public var notificacion: Bool? {
        get {
          return resultMap["notificacion"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "notificacion")
        }
      }

      public var myVet: GraphQLID? {
        get {
          return resultMap["MyVet"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "MyVet")
        }
      }

      public var isPlus: Bool? {
        get {
          return resultMap["isPlus"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "isPlus")
        }
      }

      public var connected: Bool? {
        get {
          return resultMap["connected"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "connected")
        }
      }

      public var lastTime: String? {
        get {
          return resultMap["lastTime"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "lastTime")
        }
      }

      public var setVideoConsultas: Int? {
        get {
          return resultMap["setVideoConsultas"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "setVideoConsultas")
        }
      }
    }
  }
}

public final class SingleUploadMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation singleUpload($imgBlob: Upload) {
      singleUpload(file: $imgBlob) {
        __typename
        filename
      }
    }
    """

  public let operationName: String = "singleUpload"

  public var imgBlob: String?

  public init(imgBlob: String? = nil) {
    self.imgBlob = imgBlob
  }

  public var variables: GraphQLMap? {
    return ["imgBlob": imgBlob]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("singleUpload", arguments: ["file": GraphQLVariable("imgBlob")], type: .object(SingleUpload.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(singleUpload: SingleUpload? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "singleUpload": singleUpload.flatMap { (value: SingleUpload) -> ResultMap in value.resultMap }])
    }

    public var singleUpload: SingleUpload? {
      get {
        return (resultMap["singleUpload"] as? ResultMap).flatMap { SingleUpload(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "singleUpload")
      }
    }

    public struct SingleUpload: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["File"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("filename", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(filename: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "File", "filename": filename])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var filename: String? {
        get {
          return resultMap["filename"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "filename")
        }
      }
    }
  }
}

public final class EliminarUsuarioMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation eliminarUsuario($id: ID!) {
      eliminarUsuario(id: $id) {
        __typename
        success
        message
      }
    }
    """

  public let operationName: String = "eliminarUsuario"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("eliminarUsuario", arguments: ["id": GraphQLVariable("id")], type: .object(EliminarUsuario.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(eliminarUsuario: EliminarUsuario? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "eliminarUsuario": eliminarUsuario.flatMap { (value: EliminarUsuario) -> ResultMap in value.resultMap }])
    }

    public var eliminarUsuario: EliminarUsuario? {
      get {
        return (resultMap["eliminarUsuario"] as? ResultMap).flatMap { EliminarUsuario(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "eliminarUsuario")
      }
    }

    public struct EliminarUsuario: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponse", "success": success, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class CrearMascotaMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation crearMascota($input: Mascotainput) {
      crearMascota(input: $input) {
        __typename
        success
        message
        data {
          __typename
          id
          name
          age
          avatar
          especie
          raza
          genero
          peso
          microchip
          vacunas
          alergias
          castrado
        }
      }
    }
    """

  public let operationName: String = "crearMascota"

  public var input: Mascotainput?

  public init(input: Mascotainput? = nil) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("crearMascota", arguments: ["input": GraphQLVariable("input")], type: .object(CrearMascotum.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(crearMascota: CrearMascotum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "crearMascota": crearMascota.flatMap { (value: CrearMascotum) -> ResultMap in value.resultMap }])
    }

    public var crearMascota: CrearMascotum? {
      get {
        return (resultMap["crearMascota"] as? ResultMap).flatMap { CrearMascotum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "crearMascota")
      }
    }

    public struct CrearMascotum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["MascotaResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil, data: Datum? = nil) {
        self.init(unsafeResultMap: ["__typename": "MascotaResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Mascota"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("age", type: .scalar(String.self)),
            GraphQLField("avatar", type: .scalar(String.self)),
            GraphQLField("especie", type: .scalar(String.self)),
            GraphQLField("raza", type: .scalar(String.self)),
            GraphQLField("genero", type: .scalar(String.self)),
            GraphQLField("peso", type: .scalar(String.self)),
            GraphQLField("microchip", type: .scalar(String.self)),
            GraphQLField("vacunas", type: .scalar(String.self)),
            GraphQLField("alergias", type: .scalar(String.self)),
            GraphQLField("castrado", type: .scalar(Bool.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, name: String? = nil, age: String? = nil, avatar: String? = nil, especie: String? = nil, raza: String? = nil, genero: String? = nil, peso: String? = nil, microchip: String? = nil, vacunas: String? = nil, alergias: String? = nil, castrado: Bool? = nil) {
          self.init(unsafeResultMap: ["__typename": "Mascota", "id": id, "name": name, "age": age, "avatar": avatar, "especie": especie, "raza": raza, "genero": genero, "peso": peso, "microchip": microchip, "vacunas": vacunas, "alergias": alergias, "castrado": castrado])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var age: String? {
          get {
            return resultMap["age"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "age")
          }
        }

        public var avatar: String? {
          get {
            return resultMap["avatar"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatar")
          }
        }

        public var especie: String? {
          get {
            return resultMap["especie"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "especie")
          }
        }

        public var raza: String? {
          get {
            return resultMap["raza"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "raza")
          }
        }

        public var genero: String? {
          get {
            return resultMap["genero"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "genero")
          }
        }

        public var peso: String? {
          get {
            return resultMap["peso"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "peso")
          }
        }

        public var microchip: String? {
          get {
            return resultMap["microchip"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "microchip")
          }
        }

        public var vacunas: String? {
          get {
            return resultMap["vacunas"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "vacunas")
          }
        }

        public var alergias: String? {
          get {
            return resultMap["alergias"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "alergias")
          }
        }

        public var castrado: Bool? {
          get {
            return resultMap["castrado"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "castrado")
          }
        }
      }
    }
  }
}

public final class CrearUsuarioFavoritoProMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation crearUsuarioFavoritoPro($profesionalId: ID!, $usuarioId: ID!) {
      crearUsuarioFavoritoPro(profesionalId: $profesionalId, usuarioId: $usuarioId) {
        __typename
        success
        message
      }
    }
    """

  public let operationName: String = "crearUsuarioFavoritoPro"

  public var profesionalId: GraphQLID
  public var usuarioId: GraphQLID

  public init(profesionalId: GraphQLID, usuarioId: GraphQLID) {
    self.profesionalId = profesionalId
    self.usuarioId = usuarioId
  }

  public var variables: GraphQLMap? {
    return ["profesionalId": profesionalId, "usuarioId": usuarioId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("crearUsuarioFavoritoPro", arguments: ["profesionalId": GraphQLVariable("profesionalId"), "usuarioId": GraphQLVariable("usuarioId")], type: .object(CrearUsuarioFavoritoPro.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(crearUsuarioFavoritoPro: CrearUsuarioFavoritoPro? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "crearUsuarioFavoritoPro": crearUsuarioFavoritoPro.flatMap { (value: CrearUsuarioFavoritoPro) -> ResultMap in value.resultMap }])
    }

    public var crearUsuarioFavoritoPro: CrearUsuarioFavoritoPro? {
      get {
        return (resultMap["crearUsuarioFavoritoPro"] as? ResultMap).flatMap { CrearUsuarioFavoritoPro(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "crearUsuarioFavoritoPro")
      }
    }

    public struct CrearUsuarioFavoritoPro: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponse", "success": success, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class EliminarUsuarioFavoritoProMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation eliminarUsuarioFavoritoPro($id: ID!) {
      eliminarUsuarioFavoritoPro(id: $id) {
        __typename
        success
        message
      }
    }
    """

  public let operationName: String = "eliminarUsuarioFavoritoPro"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("eliminarUsuarioFavoritoPro", arguments: ["id": GraphQLVariable("id")], type: .object(EliminarUsuarioFavoritoPro.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(eliminarUsuarioFavoritoPro: EliminarUsuarioFavoritoPro? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "eliminarUsuarioFavoritoPro": eliminarUsuarioFavoritoPro.flatMap { (value: EliminarUsuarioFavoritoPro) -> ResultMap in value.resultMap }])
    }

    public var eliminarUsuarioFavoritoPro: EliminarUsuarioFavoritoPro? {
      get {
        return (resultMap["eliminarUsuarioFavoritoPro"] as? ResultMap).flatMap { EliminarUsuarioFavoritoPro(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "eliminarUsuarioFavoritoPro")
      }
    }

    public struct EliminarUsuarioFavoritoPro: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponse", "success": success, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class EliminarMascotaMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation eliminarMascota($id: ID!) {
      eliminarMascota(id: $id) {
        __typename
        success
        message
      }
    }
    """

  public let operationName: String = "eliminarMascota"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("eliminarMascota", arguments: ["id": GraphQLVariable("id")], type: .object(EliminarMascotum.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(eliminarMascota: EliminarMascotum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "eliminarMascota": eliminarMascota.flatMap { (value: EliminarMascotum) -> ResultMap in value.resultMap }])
    }

    public var eliminarMascota: EliminarMascotum? {
      get {
        return (resultMap["eliminarMascota"] as? ResultMap).flatMap { EliminarMascotum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "eliminarMascota")
      }
    }

    public struct EliminarMascotum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponse", "success": success, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class CrearModificarConsultaMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation crearModificarConsulta($input: ConsultaCreationInput) {
      crearModificarConsulta(input: $input) {
        __typename
        id
        cupon
        nota
        aceptaTerminos
        endDate
        time
        cantidad
        descuento {
          __typename
          clave
          descuento
          tipo
        }
        usuario {
          __typename
          id
          StripeID
          nombre
          apellidos
          email
          connected
          lastTime
        }
        profesional {
          __typename
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          profesion
          experiencia
          descricion
          Precio
          notificacion
          isProfesional
          isVerified
          isAvailable
          connected
          lastTime
        }
        estado
        status
        progreso
        created_at
        mascota {
          __typename
          id
          name
          age
          usuario
          avatar
        }
      }
    }
    """

  public let operationName: String = "crearModificarConsulta"

  public var input: ConsultaCreationInput?

  public init(input: ConsultaCreationInput? = nil) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("crearModificarConsulta", arguments: ["input": GraphQLVariable("input")], type: .object(CrearModificarConsultum.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(crearModificarConsulta: CrearModificarConsultum? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "crearModificarConsulta": crearModificarConsulta.flatMap { (value: CrearModificarConsultum) -> ResultMap in value.resultMap }])
    }

    public var crearModificarConsulta: CrearModificarConsultum? {
      get {
        return (resultMap["crearModificarConsulta"] as? ResultMap).flatMap { CrearModificarConsultum(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "crearModificarConsulta")
      }
    }

    public struct CrearModificarConsultum: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Consultas"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("cupon", type: .scalar(GraphQLID.self)),
          GraphQLField("nota", type: .scalar(String.self)),
          GraphQLField("aceptaTerminos", type: .scalar(Bool.self)),
          GraphQLField("endDate", type: .scalar(String.self)),
          GraphQLField("time", type: .scalar(String.self)),
          GraphQLField("cantidad", type: .scalar(Int.self)),
          GraphQLField("descuento", type: .object(Descuento.selections)),
          GraphQLField("usuario", type: .object(Usuario.selections)),
          GraphQLField("profesional", type: .object(Profesional.selections)),
          GraphQLField("estado", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(String.self)),
          GraphQLField("progreso", type: .scalar(String.self)),
          GraphQLField("created_at", type: .scalar(String.self)),
          GraphQLField("mascota", type: .object(Mascotum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, cupon: GraphQLID? = nil, nota: String? = nil, aceptaTerminos: Bool? = nil, endDate: String? = nil, time: String? = nil, cantidad: Int? = nil, descuento: Descuento? = nil, usuario: Usuario? = nil, profesional: Profesional? = nil, estado: String? = nil, status: String? = nil, progreso: String? = nil, createdAt: String? = nil, mascota: Mascotum? = nil) {
        self.init(unsafeResultMap: ["__typename": "Consultas", "id": id, "cupon": cupon, "nota": nota, "aceptaTerminos": aceptaTerminos, "endDate": endDate, "time": time, "cantidad": cantidad, "descuento": descuento.flatMap { (value: Descuento) -> ResultMap in value.resultMap }, "usuario": usuario.flatMap { (value: Usuario) -> ResultMap in value.resultMap }, "profesional": profesional.flatMap { (value: Profesional) -> ResultMap in value.resultMap }, "estado": estado, "status": status, "progreso": progreso, "created_at": createdAt, "mascota": mascota.flatMap { (value: Mascotum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var cupon: GraphQLID? {
        get {
          return resultMap["cupon"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "cupon")
        }
      }

      public var nota: String? {
        get {
          return resultMap["nota"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "nota")
        }
      }

      public var aceptaTerminos: Bool? {
        get {
          return resultMap["aceptaTerminos"] as? Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "aceptaTerminos")
        }
      }

      public var endDate: String? {
        get {
          return resultMap["endDate"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "endDate")
        }
      }

      public var time: String? {
        get {
          return resultMap["time"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "time")
        }
      }

      public var cantidad: Int? {
        get {
          return resultMap["cantidad"] as? Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "cantidad")
        }
      }

      public var descuento: Descuento? {
        get {
          return (resultMap["descuento"] as? ResultMap).flatMap { Descuento(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "descuento")
        }
      }

      public var usuario: Usuario? {
        get {
          return (resultMap["usuario"] as? ResultMap).flatMap { Usuario(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "usuario")
        }
      }

      public var profesional: Profesional? {
        get {
          return (resultMap["profesional"] as? ResultMap).flatMap { Profesional(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "profesional")
        }
      }

      public var estado: String? {
        get {
          return resultMap["estado"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "estado")
        }
      }

      public var status: String? {
        get {
          return resultMap["status"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var progreso: String? {
        get {
          return resultMap["progreso"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "progreso")
        }
      }

      public var createdAt: String? {
        get {
          return resultMap["created_at"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "created_at")
        }
      }

      public var mascota: Mascotum? {
        get {
          return (resultMap["mascota"] as? ResultMap).flatMap { Mascotum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "mascota")
        }
      }

      public struct Descuento: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Cupon"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("clave", type: .scalar(String.self)),
            GraphQLField("descuento", type: .scalar(Double.self)),
            GraphQLField("tipo", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(clave: String? = nil, descuento: Double? = nil, tipo: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Cupon", "clave": clave, "descuento": descuento, "tipo": tipo])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var clave: String? {
          get {
            return resultMap["clave"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "clave")
          }
        }

        public var descuento: Double? {
          get {
            return resultMap["descuento"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "descuento")
          }
        }

        public var tipo: String? {
          get {
            return resultMap["tipo"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "tipo")
          }
        }
      }

      public struct Usuario: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Usuario"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("StripeID", type: .scalar(String.self)),
            GraphQLField("nombre", type: .scalar(String.self)),
            GraphQLField("apellidos", type: .scalar(String.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("connected", type: .scalar(Bool.self)),
            GraphQLField("lastTime", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, stripeId: String? = nil, nombre: String? = nil, apellidos: String? = nil, email: String? = nil, connected: Bool? = nil, lastTime: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "StripeID": stripeId, "nombre": nombre, "apellidos": apellidos, "email": email, "connected": connected, "lastTime": lastTime])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var stripeId: String? {
          get {
            return resultMap["StripeID"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "StripeID")
          }
        }

        public var nombre: String? {
          get {
            return resultMap["nombre"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nombre")
          }
        }

        public var apellidos: String? {
          get {
            return resultMap["apellidos"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "apellidos")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var connected: Bool? {
          get {
            return resultMap["connected"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "connected")
          }
        }

        public var lastTime: String? {
          get {
            return resultMap["lastTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastTime")
          }
        }
      }

      public struct Profesional: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Profesional"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("nombre", type: .scalar(String.self)),
            GraphQLField("apellidos", type: .scalar(String.self)),
            GraphQLField("ciudad", type: .scalar(String.self)),
            GraphQLField("telefono", type: .scalar(String.self)),
            GraphQLField("avatar", type: .scalar(String.self)),
            GraphQLField("profesion", type: .scalar(String.self)),
            GraphQLField("experiencia", type: .scalar(String.self)),
            GraphQLField("descricion", type: .scalar(String.self)),
            GraphQLField("Precio", type: .scalar(String.self)),
            GraphQLField("notificacion", type: .scalar(Bool.self)),
            GraphQLField("isProfesional", type: .scalar(Bool.self)),
            GraphQLField("isVerified", type: .scalar(Bool.self)),
            GraphQLField("isAvailable", type: .scalar(Bool.self)),
            GraphQLField("connected", type: .scalar(Bool.self)),
            GraphQLField("lastTime", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, profesion: String? = nil, experiencia: String? = nil, descricion: String? = nil, precio: String? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "profesion": profesion, "experiencia": experiencia, "descricion": descricion, "Precio": precio, "notificacion": notificacion, "isProfesional": isProfesional, "isVerified": isVerified, "isAvailable": isAvailable, "connected": connected, "lastTime": lastTime])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var nombre: String? {
          get {
            return resultMap["nombre"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nombre")
          }
        }

        public var apellidos: String? {
          get {
            return resultMap["apellidos"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "apellidos")
          }
        }

        public var ciudad: String? {
          get {
            return resultMap["ciudad"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "ciudad")
          }
        }

        public var telefono: String? {
          get {
            return resultMap["telefono"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "telefono")
          }
        }

        public var avatar: String? {
          get {
            return resultMap["avatar"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatar")
          }
        }

        public var profesion: String? {
          get {
            return resultMap["profesion"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profesion")
          }
        }

        public var experiencia: String? {
          get {
            return resultMap["experiencia"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "experiencia")
          }
        }

        public var descricion: String? {
          get {
            return resultMap["descricion"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "descricion")
          }
        }

        public var precio: String? {
          get {
            return resultMap["Precio"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "Precio")
          }
        }

        public var notificacion: Bool? {
          get {
            return resultMap["notificacion"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "notificacion")
          }
        }

        public var isProfesional: Bool? {
          get {
            return resultMap["isProfesional"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isProfesional")
          }
        }

        public var isVerified: Bool? {
          get {
            return resultMap["isVerified"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isVerified")
          }
        }

        public var isAvailable: Bool? {
          get {
            return resultMap["isAvailable"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isAvailable")
          }
        }

        public var connected: Bool? {
          get {
            return resultMap["connected"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "connected")
          }
        }

        public var lastTime: String? {
          get {
            return resultMap["lastTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastTime")
          }
        }
      }

      public struct Mascotum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Mascota"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("name", type: .scalar(String.self)),
            GraphQLField("age", type: .scalar(String.self)),
            GraphQLField("usuario", type: .scalar(GraphQLID.self)),
            GraphQLField("avatar", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, name: String? = nil, age: String? = nil, usuario: GraphQLID? = nil, avatar: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Mascota", "id": id, "name": name, "age": age, "usuario": usuario, "avatar": avatar])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String? {
          get {
            return resultMap["name"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var age: String? {
          get {
            return resultMap["age"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "age")
          }
        }

        public var usuario: GraphQLID? {
          get {
            return resultMap["usuario"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "usuario")
          }
        }

        public var avatar: String? {
          get {
            return resultMap["avatar"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatar")
          }
        }
      }
    }
  }
}

public final class ConsultaProceedMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation consultaProceed($consultaID: ID!, $estado: String!, $progreso: String!, $status: String!, $nota: String, $coment: String, $rate: Int, $descripcionproblem: String) {
      consultaProceed(consultaID: $consultaID, estado: $estado, progreso: $progreso, status: $status, nota: $nota, coment: $coment, rate: $rate, descripcionproblem: $descripcionproblem) {
        __typename
        success
        message
      }
    }
    """

  public let operationName: String = "consultaProceed"

  public var consultaID: GraphQLID
  public var estado: String
  public var progreso: String
  public var status: String
  public var nota: String?
  public var coment: String?
  public var rate: Int?
  public var descripcionproblem: String?

  public init(consultaID: GraphQLID, estado: String, progreso: String, status: String, nota: String? = nil, coment: String? = nil, rate: Int? = nil, descripcionproblem: String? = nil) {
    self.consultaID = consultaID
    self.estado = estado
    self.progreso = progreso
    self.status = status
    self.nota = nota
    self.coment = coment
    self.rate = rate
    self.descripcionproblem = descripcionproblem
  }

  public var variables: GraphQLMap? {
    return ["consultaID": consultaID, "estado": estado, "progreso": progreso, "status": status, "nota": nota, "coment": coment, "rate": rate, "descripcionproblem": descripcionproblem]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("consultaProceed", arguments: ["consultaID": GraphQLVariable("consultaID"), "estado": GraphQLVariable("estado"), "progreso": GraphQLVariable("progreso"), "status": GraphQLVariable("status"), "nota": GraphQLVariable("nota"), "coment": GraphQLVariable("coment"), "rate": GraphQLVariable("rate"), "descripcionproblem": GraphQLVariable("descripcionproblem")], type: .object(ConsultaProceed.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(consultaProceed: ConsultaProceed? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "consultaProceed": consultaProceed.flatMap { (value: ConsultaProceed) -> ResultMap in value.resultMap }])
    }

    public var consultaProceed: ConsultaProceed? {
      get {
        return (resultMap["consultaProceed"] as? ResultMap).flatMap { ConsultaProceed(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "consultaProceed")
      }
    }

    public struct ConsultaProceed: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponse", "success": success, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class ReadNotificationMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation readNotification($notificationId: ID!) {
      readNotification(notificationId: $notificationId) {
        __typename
        success
        message
      }
    }
    """

  public let operationName: String = "readNotification"

  public var notificationId: GraphQLID

  public init(notificationId: GraphQLID) {
    self.notificationId = notificationId
  }

  public var variables: GraphQLMap? {
    return ["notificationId": notificationId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("readNotification", arguments: ["notificationId": GraphQLVariable("notificationId")], type: .object(ReadNotification.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(readNotification: ReadNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "readNotification": readNotification.flatMap { (value: ReadNotification) -> ResultMap in value.resultMap }])
    }

    public var readNotification: ReadNotification? {
      get {
        return (resultMap["readNotification"] as? ResultMap).flatMap { ReadNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "readNotification")
      }
    }

    public struct ReadNotification: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponse", "success": success, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class CreateNotificationMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation createNotification($input: NotificationInput) {
      createNotification(input: $input) {
        __typename
        success
        message
      }
    }
    """

  public let operationName: String = "createNotification"

  public var input: NotificationInput?

  public init(input: NotificationInput? = nil) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("createNotification", arguments: ["input": GraphQLVariable("input")], type: .object(CreateNotification.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createNotification: CreateNotification? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createNotification": createNotification.flatMap { (value: CreateNotification) -> ResultMap in value.resultMap }])
    }

    public var createNotification: CreateNotification? {
      get {
        return (resultMap["createNotification"] as? ResultMap).flatMap { CreateNotification(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createNotification")
      }
    }

    public struct CreateNotification: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["GeneralResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "GeneralResponse", "success": success, "message": message])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }
    }
  }
}

public final class GetCategoriesQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getCategories {
      getCategories {
        __typename
        id
        title
        image
        font
        description
        profesionales {
          __typename
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
          profesion
          experiencia
          descricion
          Precio
          visitas
          verifyPhone
          anadidoFavorito
          notificacion
          isProfesional
          isVerified
          isAvailable
          connected
          lastTime
          professionalRatinglist {
            __typename
            id
            rate
            updated_at
            coment
            customer {
              __typename
              id
              email
              nombre
              apellidos
              ciudad
              verifyPhone
              telefono
              avatar
              UserID
              connected
              lastTime
            }
          }
          consultas {
            __typename
            id
          }
        }
      }
    }
    """

  public let operationName: String = "getCategories"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getCategories", type: .list(.object(GetCategory.selections))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getCategories: [GetCategory?]? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getCategories": getCategories.flatMap { (value: [GetCategory?]) -> [ResultMap?] in value.map { (value: GetCategory?) -> ResultMap? in value.flatMap { (value: GetCategory) -> ResultMap in value.resultMap } } }])
    }

    public var getCategories: [GetCategory?]? {
      get {
        return (resultMap["getCategories"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [GetCategory?] in value.map { (value: ResultMap?) -> GetCategory? in value.flatMap { (value: ResultMap) -> GetCategory in GetCategory(unsafeResultMap: value) } } }
      }
      set {
        resultMap.updateValue(newValue.flatMap { (value: [GetCategory?]) -> [ResultMap?] in value.map { (value: GetCategory?) -> ResultMap? in value.flatMap { (value: GetCategory) -> ResultMap in value.resultMap } } }, forKey: "getCategories")
      }
    }

    public struct GetCategory: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Category"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .scalar(GraphQLID.self)),
          GraphQLField("title", type: .scalar(String.self)),
          GraphQLField("image", type: .scalar(String.self)),
          GraphQLField("font", type: .scalar(String.self)),
          GraphQLField("description", type: .scalar(String.self)),
          GraphQLField("profesionales", type: .list(.object(Profesionale.selections))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID? = nil, title: String? = nil, image: String? = nil, font: String? = nil, description: String? = nil, profesionales: [Profesionale?]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Category", "id": id, "title": title, "image": image, "font": font, "description": description, "profesionales": profesionales.flatMap { (value: [Profesionale?]) -> [ResultMap?] in value.map { (value: Profesionale?) -> ResultMap? in value.flatMap { (value: Profesionale) -> ResultMap in value.resultMap } } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID? {
        get {
          return resultMap["id"] as? GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String? {
        get {
          return resultMap["title"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var image: String? {
        get {
          return resultMap["image"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "image")
        }
      }

      public var font: String? {
        get {
          return resultMap["font"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "font")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }

      public var profesionales: [Profesionale?]? {
        get {
          return (resultMap["profesionales"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Profesionale?] in value.map { (value: ResultMap?) -> Profesionale? in value.flatMap { (value: ResultMap) -> Profesionale in Profesionale(unsafeResultMap: value) } } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Profesionale?]) -> [ResultMap?] in value.map { (value: Profesionale?) -> ResultMap? in value.flatMap { (value: Profesionale) -> ResultMap in value.resultMap } } }, forKey: "profesionales")
        }
      }

      public struct Profesionale: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Profesional"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("email", type: .scalar(String.self)),
            GraphQLField("nombre", type: .scalar(String.self)),
            GraphQLField("apellidos", type: .scalar(String.self)),
            GraphQLField("ciudad", type: .scalar(String.self)),
            GraphQLField("telefono", type: .scalar(String.self)),
            GraphQLField("avatar", type: .scalar(String.self)),
            GraphQLField("UserID", type: .scalar(String.self)),
            GraphQLField("profesion", type: .scalar(String.self)),
            GraphQLField("experiencia", type: .scalar(String.self)),
            GraphQLField("descricion", type: .scalar(String.self)),
            GraphQLField("Precio", type: .scalar(String.self)),
            GraphQLField("visitas", type: .scalar(Int.self)),
            GraphQLField("verifyPhone", type: .scalar(Bool.self)),
            GraphQLField("anadidoFavorito", type: .scalar(Bool.self)),
            GraphQLField("notificacion", type: .scalar(Bool.self)),
            GraphQLField("isProfesional", type: .scalar(Bool.self)),
            GraphQLField("isVerified", type: .scalar(Bool.self)),
            GraphQLField("isAvailable", type: .scalar(Bool.self)),
            GraphQLField("connected", type: .scalar(Bool.self)),
            GraphQLField("lastTime", type: .scalar(String.self)),
            GraphQLField("professionalRatinglist", type: .list(.object(ProfessionalRatinglist.selections))),
            GraphQLField("consultas", type: .list(.object(Consulta.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, profesion: String? = nil, experiencia: String? = nil, descricion: String? = nil, precio: String? = nil, visitas: Int? = nil, verifyPhone: Bool? = nil, anadidoFavorito: Bool? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil, professionalRatinglist: [ProfessionalRatinglist?]? = nil, consultas: [Consulta?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId, "profesion": profesion, "experiencia": experiencia, "descricion": descricion, "Precio": precio, "visitas": visitas, "verifyPhone": verifyPhone, "anadidoFavorito": anadidoFavorito, "notificacion": notificacion, "isProfesional": isProfesional, "isVerified": isVerified, "isAvailable": isAvailable, "connected": connected, "lastTime": lastTime, "professionalRatinglist": professionalRatinglist.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, "consultas": consultas.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var email: String? {
          get {
            return resultMap["email"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "email")
          }
        }

        public var nombre: String? {
          get {
            return resultMap["nombre"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "nombre")
          }
        }

        public var apellidos: String? {
          get {
            return resultMap["apellidos"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "apellidos")
          }
        }

        public var ciudad: String? {
          get {
            return resultMap["ciudad"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "ciudad")
          }
        }

        public var telefono: String? {
          get {
            return resultMap["telefono"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "telefono")
          }
        }

        public var avatar: String? {
          get {
            return resultMap["avatar"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "avatar")
          }
        }

        public var userId: String? {
          get {
            return resultMap["UserID"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "UserID")
          }
        }

        public var profesion: String? {
          get {
            return resultMap["profesion"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "profesion")
          }
        }

        public var experiencia: String? {
          get {
            return resultMap["experiencia"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "experiencia")
          }
        }

        public var descricion: String? {
          get {
            return resultMap["descricion"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "descricion")
          }
        }

        public var precio: String? {
          get {
            return resultMap["Precio"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "Precio")
          }
        }

        public var visitas: Int? {
          get {
            return resultMap["visitas"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "visitas")
          }
        }

        public var verifyPhone: Bool? {
          get {
            return resultMap["verifyPhone"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "verifyPhone")
          }
        }

        public var anadidoFavorito: Bool? {
          get {
            return resultMap["anadidoFavorito"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "anadidoFavorito")
          }
        }

        public var notificacion: Bool? {
          get {
            return resultMap["notificacion"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "notificacion")
          }
        }

        public var isProfesional: Bool? {
          get {
            return resultMap["isProfesional"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isProfesional")
          }
        }

        public var isVerified: Bool? {
          get {
            return resultMap["isVerified"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isVerified")
          }
        }

        public var isAvailable: Bool? {
          get {
            return resultMap["isAvailable"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "isAvailable")
          }
        }

        public var connected: Bool? {
          get {
            return resultMap["connected"] as? Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "connected")
          }
        }

        public var lastTime: String? {
          get {
            return resultMap["lastTime"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "lastTime")
          }
        }

        public var professionalRatinglist: [ProfessionalRatinglist?]? {
          get {
            return (resultMap["professionalRatinglist"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProfessionalRatinglist?] in value.map { (value: ResultMap?) -> ProfessionalRatinglist? in value.flatMap { (value: ResultMap) -> ProfessionalRatinglist in ProfessionalRatinglist(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, forKey: "professionalRatinglist")
          }
        }

        public var consultas: [Consulta?]? {
          get {
            return (resultMap["consultas"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Consulta?] in value.map { (value: ResultMap?) -> Consulta? in value.flatMap { (value: ResultMap) -> Consulta in Consulta(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }, forKey: "consultas")
          }
        }

        public struct ProfessionalRatinglist: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["ProfessionalRating"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("rate", type: .scalar(Int.self)),
              GraphQLField("updated_at", type: .scalar(String.self)),
              GraphQLField("coment", type: .scalar(String.self)),
              GraphQLField("customer", type: .object(Customer.selections)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, rate: Int? = nil, updatedAt: String? = nil, coment: String? = nil, customer: Customer? = nil) {
            self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id, "rate": rate, "updated_at": updatedAt, "coment": coment, "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var rate: Int? {
            get {
              return resultMap["rate"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "rate")
            }
          }

          public var updatedAt: String? {
            get {
              return resultMap["updated_at"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "updated_at")
            }
          }

          public var coment: String? {
            get {
              return resultMap["coment"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "coment")
            }
          }

          public var customer: Customer? {
            get {
              return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
            }
            set {
              resultMap.updateValue(newValue?.resultMap, forKey: "customer")
            }
          }

          public struct Customer: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Usuario"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
                GraphQLField("email", type: .scalar(String.self)),
                GraphQLField("nombre", type: .scalar(String.self)),
                GraphQLField("apellidos", type: .scalar(String.self)),
                GraphQLField("ciudad", type: .scalar(String.self)),
                GraphQLField("verifyPhone", type: .scalar(Bool.self)),
                GraphQLField("telefono", type: .scalar(String.self)),
                GraphQLField("avatar", type: .scalar(String.self)),
                GraphQLField("UserID", type: .scalar(String.self)),
                GraphQLField("connected", type: .scalar(Bool.self)),
                GraphQLField("lastTime", type: .scalar(String.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, verifyPhone: Bool? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, connected: Bool? = nil, lastTime: String? = nil) {
              self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "verifyPhone": verifyPhone, "telefono": telefono, "avatar": avatar, "UserID": userId, "connected": connected, "lastTime": lastTime])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var email: String? {
              get {
                return resultMap["email"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "email")
              }
            }

            public var nombre: String? {
              get {
                return resultMap["nombre"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "nombre")
              }
            }

            public var apellidos: String? {
              get {
                return resultMap["apellidos"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "apellidos")
              }
            }

            public var ciudad: String? {
              get {
                return resultMap["ciudad"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "ciudad")
              }
            }

            public var verifyPhone: Bool? {
              get {
                return resultMap["verifyPhone"] as? Bool
              }
              set {
                resultMap.updateValue(newValue, forKey: "verifyPhone")
              }
            }

            public var telefono: String? {
              get {
                return resultMap["telefono"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "telefono")
              }
            }

            public var avatar: String? {
              get {
                return resultMap["avatar"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "avatar")
              }
            }

            public var userId: String? {
              get {
                return resultMap["UserID"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "UserID")
              }
            }

            public var connected: Bool? {
              get {
                return resultMap["connected"] as? Bool
              }
              set {
                resultMap.updateValue(newValue, forKey: "connected")
              }
            }

            public var lastTime: String? {
              get {
                return resultMap["lastTime"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "lastTime")
              }
            }
          }
        }

        public struct Consulta: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Consultas"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil) {
            self.init(unsafeResultMap: ["__typename": "Consultas", "id": id])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }
        }
      }
    }
  }
}

public final class GetCategoryQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query getCategory($id: ID!, $price: Int, $city: String) {
      getCategory(id: $id, price: $price, city: $city) {
        __typename
        success
        message
        data {
          __typename
          id
          title
          profesionales {
            __typename
            id
            email
            nombre
            apellidos
            ciudad
            telefono
            avatar
            UserID
            profesion
            experiencia
            descricion
            Precio
            visitas
            verifyPhone
            anadidoFavorito
            notificacion
            isProfesional
            isVerified
            isAvailable
            connected
            lastTime
            professionalRatinglist {
              __typename
              id
              rate
              updated_at
              coment
              customer {
                __typename
                id
                email
                nombre
                apellidos
                ciudad
                verifyPhone
                telefono
                avatar
                UserID
                connected
                lastTime
              }
            }
            consultas {
              __typename
              id
            }
          }
        }
      }
    }
    """

  public let operationName: String = "getCategory"

  public var id: GraphQLID
  public var price: Int?
  public var city: String?

  public init(id: GraphQLID, price: Int? = nil, city: String? = nil) {
    self.id = id
    self.price = price
    self.city = city
  }

  public var variables: GraphQLMap? {
    return ["id": id, "price": price, "city": city]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("getCategory", arguments: ["id": GraphQLVariable("id"), "price": GraphQLVariable("price"), "city": GraphQLVariable("city")], type: .object(GetCategory.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(getCategory: GetCategory? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "getCategory": getCategory.flatMap { (value: GetCategory) -> ResultMap in value.resultMap }])
    }

    public var getCategory: GetCategory? {
      get {
        return (resultMap["getCategory"] as? ResultMap).flatMap { GetCategory(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "getCategory")
      }
    }

    public struct GetCategory: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["CategoryResponse"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("success", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .scalar(String.self)),
          GraphQLField("data", type: .object(Datum.selections)),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(success: Bool, message: String? = nil, data: Datum? = nil) {
        self.init(unsafeResultMap: ["__typename": "CategoryResponse", "success": success, "message": message, "data": data.flatMap { (value: Datum) -> ResultMap in value.resultMap }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var success: Bool {
        get {
          return resultMap["success"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "success")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var data: Datum? {
        get {
          return (resultMap["data"] as? ResultMap).flatMap { Datum(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "data")
        }
      }

      public struct Datum: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Category"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("id", type: .scalar(GraphQLID.self)),
            GraphQLField("title", type: .scalar(String.self)),
            GraphQLField("profesionales", type: .list(.object(Profesionale.selections))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID? = nil, title: String? = nil, profesionales: [Profesionale?]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Category", "id": id, "title": title, "profesionales": profesionales.flatMap { (value: [Profesionale?]) -> [ResultMap?] in value.map { (value: Profesionale?) -> ResultMap? in value.flatMap { (value: Profesionale) -> ResultMap in value.resultMap } } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID? {
          get {
            return resultMap["id"] as? GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var title: String? {
          get {
            return resultMap["title"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        public var profesionales: [Profesionale?]? {
          get {
            return (resultMap["profesionales"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Profesionale?] in value.map { (value: ResultMap?) -> Profesionale? in value.flatMap { (value: ResultMap) -> Profesionale in Profesionale(unsafeResultMap: value) } } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Profesionale?]) -> [ResultMap?] in value.map { (value: Profesionale?) -> ResultMap? in value.flatMap { (value: Profesionale) -> ResultMap in value.resultMap } } }, forKey: "profesionales")
          }
        }

        public struct Profesionale: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Profesional"]

          public static var selections: [GraphQLSelection] {
            return [
              GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
              GraphQLField("id", type: .scalar(GraphQLID.self)),
              GraphQLField("email", type: .scalar(String.self)),
              GraphQLField("nombre", type: .scalar(String.self)),
              GraphQLField("apellidos", type: .scalar(String.self)),
              GraphQLField("ciudad", type: .scalar(String.self)),
              GraphQLField("telefono", type: .scalar(String.self)),
              GraphQLField("avatar", type: .scalar(String.self)),
              GraphQLField("UserID", type: .scalar(String.self)),
              GraphQLField("profesion", type: .scalar(String.self)),
              GraphQLField("experiencia", type: .scalar(String.self)),
              GraphQLField("descricion", type: .scalar(String.self)),
              GraphQLField("Precio", type: .scalar(String.self)),
              GraphQLField("visitas", type: .scalar(Int.self)),
              GraphQLField("verifyPhone", type: .scalar(Bool.self)),
              GraphQLField("anadidoFavorito", type: .scalar(Bool.self)),
              GraphQLField("notificacion", type: .scalar(Bool.self)),
              GraphQLField("isProfesional", type: .scalar(Bool.self)),
              GraphQLField("isVerified", type: .scalar(Bool.self)),
              GraphQLField("isAvailable", type: .scalar(Bool.self)),
              GraphQLField("connected", type: .scalar(Bool.self)),
              GraphQLField("lastTime", type: .scalar(String.self)),
              GraphQLField("professionalRatinglist", type: .list(.object(ProfessionalRatinglist.selections))),
              GraphQLField("consultas", type: .list(.object(Consulta.selections))),
            ]
          }

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, profesion: String? = nil, experiencia: String? = nil, descricion: String? = nil, precio: String? = nil, visitas: Int? = nil, verifyPhone: Bool? = nil, anadidoFavorito: Bool? = nil, notificacion: Bool? = nil, isProfesional: Bool? = nil, isVerified: Bool? = nil, isAvailable: Bool? = nil, connected: Bool? = nil, lastTime: String? = nil, professionalRatinglist: [ProfessionalRatinglist?]? = nil, consultas: [Consulta?]? = nil) {
            self.init(unsafeResultMap: ["__typename": "Profesional", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "telefono": telefono, "avatar": avatar, "UserID": userId, "profesion": profesion, "experiencia": experiencia, "descricion": descricion, "Precio": precio, "visitas": visitas, "verifyPhone": verifyPhone, "anadidoFavorito": anadidoFavorito, "notificacion": notificacion, "isProfesional": isProfesional, "isVerified": isVerified, "isAvailable": isAvailable, "connected": connected, "lastTime": lastTime, "professionalRatinglist": professionalRatinglist.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, "consultas": consultas.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var id: GraphQLID? {
            get {
              return resultMap["id"] as? GraphQLID
            }
            set {
              resultMap.updateValue(newValue, forKey: "id")
            }
          }

          public var email: String? {
            get {
              return resultMap["email"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "email")
            }
          }

          public var nombre: String? {
            get {
              return resultMap["nombre"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "nombre")
            }
          }

          public var apellidos: String? {
            get {
              return resultMap["apellidos"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "apellidos")
            }
          }

          public var ciudad: String? {
            get {
              return resultMap["ciudad"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "ciudad")
            }
          }

          public var telefono: String? {
            get {
              return resultMap["telefono"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "telefono")
            }
          }

          public var avatar: String? {
            get {
              return resultMap["avatar"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "avatar")
            }
          }

          public var userId: String? {
            get {
              return resultMap["UserID"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "UserID")
            }
          }

          public var profesion: String? {
            get {
              return resultMap["profesion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "profesion")
            }
          }

          public var experiencia: String? {
            get {
              return resultMap["experiencia"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "experiencia")
            }
          }

          public var descricion: String? {
            get {
              return resultMap["descricion"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "descricion")
            }
          }

          public var precio: String? {
            get {
              return resultMap["Precio"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "Precio")
            }
          }

          public var visitas: Int? {
            get {
              return resultMap["visitas"] as? Int
            }
            set {
              resultMap.updateValue(newValue, forKey: "visitas")
            }
          }

          public var verifyPhone: Bool? {
            get {
              return resultMap["verifyPhone"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "verifyPhone")
            }
          }

          public var anadidoFavorito: Bool? {
            get {
              return resultMap["anadidoFavorito"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "anadidoFavorito")
            }
          }

          public var notificacion: Bool? {
            get {
              return resultMap["notificacion"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "notificacion")
            }
          }

          public var isProfesional: Bool? {
            get {
              return resultMap["isProfesional"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isProfesional")
            }
          }

          public var isVerified: Bool? {
            get {
              return resultMap["isVerified"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isVerified")
            }
          }

          public var isAvailable: Bool? {
            get {
              return resultMap["isAvailable"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "isAvailable")
            }
          }

          public var connected: Bool? {
            get {
              return resultMap["connected"] as? Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "connected")
            }
          }

          public var lastTime: String? {
            get {
              return resultMap["lastTime"] as? String
            }
            set {
              resultMap.updateValue(newValue, forKey: "lastTime")
            }
          }

          public var professionalRatinglist: [ProfessionalRatinglist?]? {
            get {
              return (resultMap["professionalRatinglist"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [ProfessionalRatinglist?] in value.map { (value: ResultMap?) -> ProfessionalRatinglist? in value.flatMap { (value: ResultMap) -> ProfessionalRatinglist in ProfessionalRatinglist(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [ProfessionalRatinglist?]) -> [ResultMap?] in value.map { (value: ProfessionalRatinglist?) -> ResultMap? in value.flatMap { (value: ProfessionalRatinglist) -> ResultMap in value.resultMap } } }, forKey: "professionalRatinglist")
            }
          }

          public var consultas: [Consulta?]? {
            get {
              return (resultMap["consultas"] as? [ResultMap?]).flatMap { (value: [ResultMap?]) -> [Consulta?] in value.map { (value: ResultMap?) -> Consulta? in value.flatMap { (value: ResultMap) -> Consulta in Consulta(unsafeResultMap: value) } } }
            }
            set {
              resultMap.updateValue(newValue.flatMap { (value: [Consulta?]) -> [ResultMap?] in value.map { (value: Consulta?) -> ResultMap? in value.flatMap { (value: Consulta) -> ResultMap in value.resultMap } } }, forKey: "consultas")
            }
          }

          public struct ProfessionalRatinglist: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["ProfessionalRating"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
                GraphQLField("rate", type: .scalar(Int.self)),
                GraphQLField("updated_at", type: .scalar(String.self)),
                GraphQLField("coment", type: .scalar(String.self)),
                GraphQLField("customer", type: .object(Customer.selections)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil, rate: Int? = nil, updatedAt: String? = nil, coment: String? = nil, customer: Customer? = nil) {
              self.init(unsafeResultMap: ["__typename": "ProfessionalRating", "id": id, "rate": rate, "updated_at": updatedAt, "coment": coment, "customer": customer.flatMap { (value: Customer) -> ResultMap in value.resultMap }])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }

            public var rate: Int? {
              get {
                return resultMap["rate"] as? Int
              }
              set {
                resultMap.updateValue(newValue, forKey: "rate")
              }
            }

            public var updatedAt: String? {
              get {
                return resultMap["updated_at"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "updated_at")
              }
            }

            public var coment: String? {
              get {
                return resultMap["coment"] as? String
              }
              set {
                resultMap.updateValue(newValue, forKey: "coment")
              }
            }

            public var customer: Customer? {
              get {
                return (resultMap["customer"] as? ResultMap).flatMap { Customer(unsafeResultMap: $0) }
              }
              set {
                resultMap.updateValue(newValue?.resultMap, forKey: "customer")
              }
            }

            public struct Customer: GraphQLSelectionSet {
              public static let possibleTypes: [String] = ["Usuario"]

              public static var selections: [GraphQLSelection] {
                return [
                  GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                  GraphQLField("id", type: .scalar(GraphQLID.self)),
                  GraphQLField("email", type: .scalar(String.self)),
                  GraphQLField("nombre", type: .scalar(String.self)),
                  GraphQLField("apellidos", type: .scalar(String.self)),
                  GraphQLField("ciudad", type: .scalar(String.self)),
                  GraphQLField("verifyPhone", type: .scalar(Bool.self)),
                  GraphQLField("telefono", type: .scalar(String.self)),
                  GraphQLField("avatar", type: .scalar(String.self)),
                  GraphQLField("UserID", type: .scalar(String.self)),
                  GraphQLField("connected", type: .scalar(Bool.self)),
                  GraphQLField("lastTime", type: .scalar(String.self)),
                ]
              }

              public private(set) var resultMap: ResultMap

              public init(unsafeResultMap: ResultMap) {
                self.resultMap = unsafeResultMap
              }

              public init(id: GraphQLID? = nil, email: String? = nil, nombre: String? = nil, apellidos: String? = nil, ciudad: String? = nil, verifyPhone: Bool? = nil, telefono: String? = nil, avatar: String? = nil, userId: String? = nil, connected: Bool? = nil, lastTime: String? = nil) {
                self.init(unsafeResultMap: ["__typename": "Usuario", "id": id, "email": email, "nombre": nombre, "apellidos": apellidos, "ciudad": ciudad, "verifyPhone": verifyPhone, "telefono": telefono, "avatar": avatar, "UserID": userId, "connected": connected, "lastTime": lastTime])
              }

              public var __typename: String {
                get {
                  return resultMap["__typename"]! as! String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "__typename")
                }
              }

              public var id: GraphQLID? {
                get {
                  return resultMap["id"] as? GraphQLID
                }
                set {
                  resultMap.updateValue(newValue, forKey: "id")
                }
              }

              public var email: String? {
                get {
                  return resultMap["email"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "email")
                }
              }

              public var nombre: String? {
                get {
                  return resultMap["nombre"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "nombre")
                }
              }

              public var apellidos: String? {
                get {
                  return resultMap["apellidos"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "apellidos")
                }
              }

              public var ciudad: String? {
                get {
                  return resultMap["ciudad"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "ciudad")
                }
              }

              public var verifyPhone: Bool? {
                get {
                  return resultMap["verifyPhone"] as? Bool
                }
                set {
                  resultMap.updateValue(newValue, forKey: "verifyPhone")
                }
              }

              public var telefono: String? {
                get {
                  return resultMap["telefono"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "telefono")
                }
              }

              public var avatar: String? {
                get {
                  return resultMap["avatar"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "avatar")
                }
              }

              public var userId: String? {
                get {
                  return resultMap["UserID"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "UserID")
                }
              }

              public var connected: Bool? {
                get {
                  return resultMap["connected"] as? Bool
                }
                set {
                  resultMap.updateValue(newValue, forKey: "connected")
                }
              }

              public var lastTime: String? {
                get {
                  return resultMap["lastTime"] as? String
                }
                set {
                  resultMap.updateValue(newValue, forKey: "lastTime")
                }
              }
            }
          }

          public struct Consulta: GraphQLSelectionSet {
            public static let possibleTypes: [String] = ["Consultas"]

            public static var selections: [GraphQLSelection] {
              return [
                GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
                GraphQLField("id", type: .scalar(GraphQLID.self)),
              ]
            }

            public private(set) var resultMap: ResultMap

            public init(unsafeResultMap: ResultMap) {
              self.resultMap = unsafeResultMap
            }

            public init(id: GraphQLID? = nil) {
              self.init(unsafeResultMap: ["__typename": "Consultas", "id": id])
            }

            public var __typename: String {
              get {
                return resultMap["__typename"]! as! String
              }
              set {
                resultMap.updateValue(newValue, forKey: "__typename")
              }
            }

            public var id: GraphQLID? {
              get {
                return resultMap["id"] as? GraphQLID
              }
              set {
                resultMap.updateValue(newValue, forKey: "id")
              }
            }
          }
        }
      }
    }
  }
}
