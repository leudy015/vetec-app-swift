//
//  Network.swift
//  vetecapp
//
//  Created by Leudy on 01/07/2020.
//

import Foundation
import Apollo

class Network: HTTPNetworkTransportDelegate {
  static let shared = Network()
  
  // Configure the network transport to use the singleton as the delegate.
  private lazy var networkTransport: HTTPNetworkTransport = {
    let transport = HTTPNetworkTransport(url: URL(string: "https://server.vetec.es/graphql")!)
    transport.delegate = self
    return transport
  }()
    
  // Use the configured network transport in your Apollo client.
  private(set) lazy var apollo = ApolloClient(networkTransport: self.networkTransport)
}

extension Network: HTTPNetworkTransportPreflightDelegate {
    
    func networkTransport(_ networkTransport: HTTPNetworkTransport, shouldSend request: URLRequest) -> Bool {
        return true
    }
    

  func networkTransport(_ networkTransport: HTTPNetworkTransport,
                        willSend request: inout URLRequest) {
                        
    // Get the existing headers, or create new ones if they're nil
    var headers = request.allHTTPHeaderFields ?? [String: String]()

    // Add any new headers you need
    
    let defaults = UserDefaults.standard
    let token = defaults.value(forKey: "token")
    
    headers["authorization"] = "\(token ?? "")"
  
    // Re-assign the updated headers to the request.
    request.allHTTPHeaderFields = headers
    
    print("Outgoing request: \(request)")
  }
}
